$(document).ready(function () {
	dataTableInit();
});

function dataTableInit(){
	$('.dt').DataTable({
		dom: "Bfrtlpi",
		buttons: [
			{
				extend: "copy",
				text: '<span><i class="fa fa-copy"></i></span> COPIAR',
				className: "btn btn-primary margin-dt",
				init: function(api, node) {
					$(node).removeClass("dt-button");
				}
			},
			{
				extend: "csv",
				text: '<span><i class="fa fa-file-o"></i></span> CSV',
				className: "btn btn-primary margin-dt",
				init: function(api, node) {
					$(node).removeClass("dt-button");
				}
			},
			{
				extend: "excel",
				text: '<span><i class="fa fa-file-excel-o"></i></span> EXCEL',
				className: "btn btn-primary margin-dt",
				init: function(api, node) {
					$(node).removeClass("dt-button");
				}
			},
			{
				extend: "pdf",
				text: '<span><i class="fa fa-file-pdf-o"></i></span> PDF',
				className: "btn btn-primary margin-dt",
				init: function(api, node) {
					$(node).removeClass("dt-button");
				}
			},
			{
				extend: "print",
				text: '<span><i class="fa fa-print"></i></span> Imprimir',
				className: "btn btn-primary margin-dt",
				init: function(api, node) {
					$(node).removeClass("dt-button");
				}
			}
		],
		"lengthMenu": [[10, 25, 50, 100,-1], [10, 25,50, 100, "Todos"]],
		paging: true,
		pageLength: 10,
		searching: true,
		ordering: true,
		info: true,
		language: {
			"url": base_url+"scripts/datatables/Spanish.json"
		}
	});
}