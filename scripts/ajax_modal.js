$(document).ready(function(){
    //################################## INFO DE INSUMO ############################################
    //Cambio en Tipo 
    $('.factura').click(function(){
        var factura = $(this).data('value');
        console.log('factura = '+factura);
        // AJAX request
         $.ajax({
            url:base_url+'consultas_ajax/insumos_factura',
            method: 'post',
            data: {this_factura: factura},
            dataType: 'json',
            success: function(response){
                console.log(response);
                $('#factura-label').text('INSUMOS DE FACTURA #'+factura);
				$('#insumos_factura tbody').html('');
                $.each(response,function(index,data){
					$('#insumos_factura').append('<tr>'+
													'<td>'+data['id_catalogo']+'</td>'+
													'<td>'+data['tipo_insumo']+'</td>'+
													'<td>'+data['subtipo_insumo']+'</td>'+
													'<td>'+data['name']+'</td>'+
													'<td>'+data['presentacion']+'</td>'+
													'<td>'+data['cantidad']+'</td>'+
													'<td>'+data['peso']+'</td>'+
												'</tr>'
												);
                });
            }
        });
	});
    $('.asignacion').click(function(){
        var asignacion = $(this).data('value');
        console.log('asignacion = '+asignacion);
        // AJAX request
         $.ajax({
            url:base_url+'consultas_ajax/insumos_asignacion',
            method: 'post',
            data: {this_asignacion: asignacion},
            dataType: 'json',
            success: function(response){
                console.log(response);
                $('#asignacion-label').text('INSUMOS DE ASIGNACION #'+asignacion);
				$('#insumos_asignacion tbody').html('');
                $.each(response,function(index,data){
					$('#insumos_asignacion').append('<tr>'+
													'<td>'+data['id_catalogo']+'</td>'+
													'<td>'+data['tipo_insumo']+'</td>'+
													'<td>'+data['subtipo_insumo']+'</td>'+
													'<td>'+data['name']+'</td>'+
													'<td>'+data['presentacion']+'</td>'+
													'<td>'+data['cantidad']+'</td>'+
													'<td>'+data['peso']+'</td>'+
												'</tr>'
												);
                });
            }
        });
	});
});
