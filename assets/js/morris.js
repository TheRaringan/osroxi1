const morris = document.querySelector('#morris');

async function get(url){
  const response = await fetch(url);
  const resData = await response.json();
  return resData;
}


if(morris){

  get(base_url+'index.php/upsas_ajax/get_upsa_agric_edo')
    .then((dataRes) => {
      const labels = [];
      const labels2 = [];
      const barChar = [];
      const line = [];
      for(let i in dataRes){
        labels2.push(
          { label: 'Distrito Capital', value: parseInt(dataRes[0]['p_agric_distcapital']) },
          { label: 'Amazonas', value: parseInt(dataRes[0]['p_agric_amazonas']) },
          { label: 'Anzoategui', value: parseInt(dataRes[0]['p_agric_anzoategui']) },
          { label: 'Apure', value: parseInt(dataRes[0]['p_agric_apure']) },
        )

        barChar.push(
          {device: 'Distrito Capital', geekbench: parseInt(dataRes[0]['p_agric_distcapital'])},
          {device: 'Amazonas', geekbench: parseInt(dataRes[0]['p_agric_amazonas']) },
          {device: 'Anzoategui', geekbench: parseInt(dataRes[0]['p_agric_anzoategui'])},
          {device: 'Apure', geekbench: parseInt(dataRes[0]['p_agric_apure'])},
        );

        line.push(
          {year: '2010', value: parseInt(dataRes[0]['p_agric_distcapital'])},
          {year: '2011', value: parseInt(dataRes[0]['p_agric_amazonas']) },
          {year: '2012', value: parseInt(dataRes[0]['p_agric_anzoategui'])},
          {year: '2013', value: parseInt(dataRes[0]['p_agric_apure'])},
        );

        for(let a in dataRes[i]){
          labels.push(
            {label: 'someval', value: parseInt(dataRes[i][a])}
          );
        }
      }

      $(document).ready(function() {
        Morris.Bar({
          element: 'graph_bar',
          data: barChar,
          xkey: 'device',
          ykeys: ['geekbench'],
          labels: ['Geekbench'],
          barRatio: 0.4,
          barColors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
          xLabelAngle: 35,
          hideHover: 'auto',
          resize: true
        });

        Morris.Donut({

          element: 'graph_donut',
          data: labels2,
          colors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
          formatter: function (y) {
            return y + "%";
          },
          resize: true
        });

        Morris.Line({
          element: 'graph_line',
          xkey: 'year',
          ykeys: ['value'],
          labels: ['Value'],
          hideHover: 'auto',
          lineColors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
          data: [
            {year: '2012', value: 20},
            {year: '2013', value: 10},
            {year: '2014', value: 5},
            {year: '2015', value: 5},
            {year: '2016', value: 20}
          ],
          resize: true
        });

        $MENU_TOGGLE.on('click', function() {
          $(window).resize();
        });
      });
    })
    .catch((error) => console.log(error));
}