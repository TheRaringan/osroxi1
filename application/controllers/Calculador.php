<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Trabajadores
 *
 * @author Nelly Moreno
 */
class Calculador extends CI_Controller{

    Public $pnoti;
    
   public function __construct()
    {
        parent::__construct();
        $this->load->model('calculador_model', '', TRUE);
        //$this->load->model('auditoria_model', '', TRUE);
        $this->load->helper(array('url', 'form'));
            $this->pnoti=0;
    }
   
     public function index()
    {
        $data = array('titulo' => 'Calculador');
          $a=$this->pnoti;
        if ($a ==1){
            $data = array('titulin' => 'Calculador', 'pnoti' => 'Se registró exitosamente la información!', 'info' => 'info');
        }else  if ($a ==2){
            $data = array('titulin' => 'Calculador', 'pnoti' => 'Ocurrió un Error al tratar de registrar los datos!', 'info' => 'error');
        }else if ($a== 3) {
                $data = array('titulin' => 'Calculador', 'pnoti' => 'Se actualizaron correctamente los datos!', 'info' => 'success');
        }else if ($a== 4) {
                $data = array('titulin' => 'Calculador', 'pnoti' => 'El ingreso que intenta realizar ya se encuentra registrado', 'info' => 'warnnign');
        }else if ($a== 5) {
                $data = array('titulin' => 'Calculador', 'pnoti' => 'Debe llenar todos los para realizar el Registro', 'info' => 'warnnign');
        }
        $this->load->view('templates/header', $data);
       
       
        $rubros = $this->calculador_model->cargar_rubros();
        //$indicador = $this->trabajadores_model->cargar_indicadores();
        //$cargo = $this->trabajadores_model->cargar_cargos();
        //$aprobadores = $this->trabajadores_model->cargar_aprobadores();
        //$listado = $this->trabajadores_model->listar_trabajadores();      
		
		 $data = array('seccion_n' => 'Registro Trabajadores', 
								'sub_titulo_registrar' => 'Registrar nuevos' , 
								'sub_titulo_actualizar' => 'Modificar existentes', 
								'titulo_seg_ventana'=> 'Lista Modificable de Trabajadores',
								'inicio' => 'INICIAR SESI&Oacute;N',
								'rubros' => $rubros);
		
         $this->load->view('templates/navegator', $data);
        $this->load->view('calculador/index', $data);
        $this->load->view('templates/footer');
        //$usuario=$_SESSION['usuario'];
       //$auditoria=new auditoria_model();
        //$rs=$auditoria->registrar_auditoria("Inicio","Ingreso a Ventana de Trabajadores (usuario:".$usuario.")");
        
    }
     public function cargar_rubros(){
		$cedula = $this->input->post('cedula');
		$datos = $this->trabajadores_model->consultar_persona_method($cedula);
		echo json_encode($datos);
		
		}
	
	public function cargar_insumos(){
		$rubro = $this->input->post('id_rubro');
		$datos = $this->calculador_model->consultar_insumos_method($rubro);
		echo json_encode($datos);
		
	}

    public function cargar_rubros_todos(){
        $rubro = $this->input->post('id_rubro');
        $datos = $this->calculador_model->consultar_rubros_todos($rubro);
        echo json_encode($datos);
    }
  
}
