<?php
class Alianza extends CI_Controller{

    Public $pnoti;

   public function __construct()
    {
        parent::__construct();
        $this->load->model('Alianza_model', '', TRUE);
        $this->load->helper(array('url', 'form'));
        //$this->load->model('auditoria_model', '', TRUE);
            $this->pnoti=0;
    }

     public function index(){
        if(!$this->session->userdata('logged_in')){
            redirect('');
        }
        $data['seccion_n'] = 'Reporte UPSAS';
        $a=$this->pnoti;

        $listado = $this->Alianza_model->alianza();
        $upsas = $this->Alianza_model->get_upsas();
        $estados = $this->Alianza_model->get_estado();

        $data = array('inicio' => 'INICIAR SESI&Oacute;N',
            'titulo' => 'Datos Basicos de la UPSA',
            'subtitulo' => 'Coordinador',
            'estados' => $estados,
            'upsas' => $upsas,
            'Listado' => $listado
        );
        $this->load->view('templates/header');
        $this->load->view('templates/navegator', $data);
        $this->load->view('alianza/index', $data);
        $this->load->view('templates/footer');
        //$usuario=$_SESSION['usuario'];
       //$auditoria=new auditoria_model();
        //$rs=$auditoria->registrar_auditoria("Inicio","Ingrso a ventana de personas (usuario:".$usuario.")");
    }


	public function listar(){
            $postData = $this->input->post();
            $data = $this->Alianza_model->alianza_2($postData);
            echo json_encode($data);
    }

    public function modificar_alianza(){

        $id_alianza = $this->input->post('id_alianza');
        $id_upsa = $this->input->post('id_upsa');
        $nombre_alianza = $this->input->post('nombre_alianza');
        $responsable = $this->input->post('responsable');
        $rif = $this->input->post('rif');
        $telefono = $this->input->post('telefono');
        $direccion = $this->input->post('direccion');
        $correo = $this->input->post('correo');
        $id_estado = $this->input->post('id_estado');
        $id_municipio = $this->input->post('id_municipio');
        $id_parroquia = $this->input->post('id_parroquia');
        $latitud = $this->input->post('latitud');
        $longitud = $this->input->post('longitud');
        $objeto = $this->input->post('objeto');
        $aportes = $this->input->post('aportes');
        $fecha_suscripcion = $this->input->post('fecha_suscripcion');
        $duracion = $this->input->post('duracion');
        $utilidad = $this->input->post('utilidad');
        $produccion = $this->input->post('produccion');
        $ba_array = $this->input->post('bienes_asignados');
        $proyecto = $this->input->post('proyecto');
        $fecha_siembra = $this->input->post('fecha_siembra');
        $tiempo_cosecha = $this->input->post('tiempo_cosecha');
        $has_sembradas = $this->input->post('has_sembradas');
        $has_cosechadas = $this->input->post('has_cosechadas');
        $fecha_cosecha = $this->input->post('fecha_cosecha');
        $peso_liquidar = $this->input->post('peso_liquidar');
        $silo = $this->input->post('silo');
        $nombre_contacto = $this->input->post('nombre_contacto');
        $cedula_contacto = $this->input->post('cedula_contacto');
        $telefono_contacto = $this->input->post('telefono_contacto');
        $fecha_contacto = $this->input->post('fecha_contacto');
        $informacion = $this->input->post('informacion');

        for ($i = 0 ; $i < count ($ba_array) ; $i++)    
        {     
            $bienes_asignados .= $ba_array[$i];
            if($i < count ($ba_array)-1)
                $bienes_asignados .= ", ";

        } 

//        var_dump($id_upsa, $id_alianza);die;

        $resultado = $this->Alianza_model->modificar_alianza($id_alianza ,$id_upsa, $nombre_alianza, $responsable, $rif, $telefono, $direccion, $correo, $id_estado, $id_municipio, $id_parroquia, $latitud, $longitud, $objeto, $aportes, $fecha_suscripcion, $duracion, $utilidad, $produccion, $bienes_asignados, $proyecto, $fecha_siembra, $tiempo_cosecha, $has_sembradas, $has_cosechadas, $fecha_cosecha, $peso_liquidar, $silo, $nombre_contacto, $cedula_contacto, $telefono_contacto, $fecha_contacto, $informacion);


        if ($resultado)
        {
            $this->session->set_flashdata('pnotify','alianza_mod');
            redirect('alianza/', 'refresh');
            
        }else{
            $this->session->set_flashdata('pnotify','fail');
            redirect('alianza/', 'refresh');
        }

    }

    public function estatus()
    {
        $id = $this->input->get('id');
        $estatus = $this->input->get('estatus');
        $resultado = $this->Alianza_model->modificar_estatus($id, $estatus);

        if ($resultado)
        {
            if($estatus == 1)
                $this->session->set_flashdata('pnotify','activo');
            else
                $this->session->set_flashdata('pnotify','inactivo');

            redirect('alianza/', 'refresh');
            
        }
        else {
            $this->session->set_flashdata('pnotify','fail');
            redirect('alianza/', 'refresh');
        }
    }

}
