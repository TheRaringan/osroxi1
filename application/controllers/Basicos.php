<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Datos Básicos de UPSA con Datos de Coordinador
 *
 * @author Nelly Moreno
 */
class Basicos extends CI_Controller{

    Public $pnoti;
    
   public function __construct()
    {
        parent::__construct();
        $this->load->model('basicos_model', '', TRUE);
        $this->load->helper(array('url', 'form'));
        //$this->load->model('auditoria_model', '', TRUE);
            $this->pnoti=0;
    }
   
     public function index()
    {
        if(!$this->session->userdata('logged_in')){
            redirect('');
        }

        //var_dump($data['registros']);die;

        $data['seccion_n'] = 'Ficha Básica de UPSA';
          $a=$this->pnoti;
        // if ($a ==1){
        //     $data = array('titulin' => 'Personas', 'pnoti' => 'Se registró exitosamente la información!', 'info' => 'info');
        // }else  if ($a ==2){
        //     $data = array('titulin' => 'Personas', 'pnoti' => 'Ocurrió un Error al tratar de registrar los datos!', 'info' => 'error');
        // }else if ($a== 3) {
        //         $data = array('titulin' => 'Personas', 'pnoti' => 'Se actualizaron correctamente los datos!', 'info' => 'success');
        // }else if ($a== 4) {
        //         $data = array('titulin' => 'Personas', 'pnoti' => 'El ingreso que intenta realizar ya se encuentra registrado', 'info' => 'warnnign');
        // }else if ($a== 5) {
        //         $data = array('titulin' => 'Personas', 'pnoti' => 'Debe llenar todos los para realizar el Registro', 'info' => 'warnnign');
        // }
        
        // $data = array('seccion_n' => 'Ficha Básica de UPSA', 'sub_titulo' => 'Registrar nueva UPSA' , 'sub_titulo2' => 'Coordinador' );
        
        //$listado = $this->personas_model->listar_personas();      
        
        $agropatria = $this->basicos_model->consultar_agropatria();
        $tenencia_tierra = $this->basicos_model->consultar_tenencia();
        $relacion_corpo = $this->basicos_model->consultar_relacion_corpo();
        $data = array('inicio' => 'INICIAR SESI&Oacute;N',
            'agropatria' => $agropatria,
            'tenencia_tierra' => $tenencia_tierra,
            'relacion_corpo' => $relacion_corpo,
            'titulo' => 'Datos Básicos de la UPSA',
            'subtitulo' => 'Coordinador'
        );
        $this->load->view('templates/header');
        $this->load->view('templates/navegator', $data);
        $this->load->view('basicos/index', $data);
        $this->load->view('templates/footer');
        //$usuario=$_SESSION['usuario'];
       //$auditoria=new auditoria_model();
        //$rs=$auditoria->registrar_auditoria("Inicio","Ingrso a ventana de personas (usuario:".$usuario.")");

        
    }


    public function procesardatos()
    
    {
		
		$boton = $this->input->post('boton');

        /*if($boton=='Actualizar'){

            $datos = $this->personas_model->actualizar_method(
            $this->input->post('cedula'), 
            $this->input->post('primer_nombre'),
            $this->input->post('segundo_nombre'),
            $this->input->post('primer_apellido'),
            $this->input->post('segundo_apellido'),
            $this->input->post('nacionalidad')); 
            if($datos){
                     $this->pnoti=3;
                }
                $usuario=$_SESSION['usuario'];
       $auditoria=new auditoria_model();
        $rs=$auditoria->registrar_auditoria("Inicio","Actualización de una Persona (usuario:".$usuario.")");
                 $this->index();  

        }*/
		
        if($boton=='Guardar'){
			
			//echo $boton;
			
			$this->basicos_model->db->trans_start();
            $datos = $this->basicos_model->insertar_method(
            $this->input->post('nombre_upsa'), 
            $this->input->post('cod_sunagro'),
            $this->input->post('cod_insai'),
            $this->input->post('id_agropatria'),
            $this->input->post('id_tenencia_tierra'),
            $this->input->post('id_relacion_corpo'),
            $this->input->post('cedula'),
            $this->input->post('nombre'),
            $this->input->post('apellido'),
            $this->input->post('tlf_local'),
            $this->input->post('tlf_movil'),
            $this->input->post('email'));

            
            
            //$usuario=$_SESSION['usuario'];
       //$auditoria=new auditoria_model();
       // $rs=$auditoria->registrar_auditoria("Inicio","Registro de Nueva Persona (usuario:".$usuario.")");
                
                if($datos == 1001){
                    $this->session->set_flashdata('pnotify','insert');
                }else if($datos == 1000)    {
                     $this->pnoti=2;
                }else if($datos == 1004){
                    $this->session->set_flashdata('pnotify','exists');
                }else if($datos == 1005){
                     $this->pnoti=5;
                }
                 $this->index(); 
                 $this->basicos_model->db->trans_complete();
        } 
        redirect('basicos','refresh');
       
     }

     public function insertar_basicos(){
        $nombre_upsa = $this->input->post('nombre_upsa');
        $cod_sunagro = $this->input->post('cod_sunagro');
        $cod_insai = $this->input->post('cod_insai');
        $id_agropatria = $this->input->post('id_agropatria');
        $id_tenencia_tierra = $this->input->post('id_tenencia_tierra');
        $id_relacion_corpo = $this->input->post('id_relacion_corpo');
        $cedula = $this->input->post('cedula');
        $nombre_coordinador = $this->input->post('nombre');
        $apellido_coordinador = $this->input->post('apellido');
        $tlf_local = $this->input->post('tlf_local');
        if($tlf_local == ''){
            $tlf_local = 'N/A';
        }
        $tlf_movil = $this->input->post('tlf_movil');
        $email = $this->input->post('email');

        $insertar = $this->basicos_model->insertar_basicos( $nombre_upsa,
                                                            $cod_sunagro,
                                                            $cod_insai,
                                                            $id_agropatria,
                                                            $id_tenencia_tierra,
                                                            $id_relacion_corpo,
                                                            $cedula,
                                                            $nombre_coordinador,
                                                            $apellido_coordinador,
                                                            $tlf_local,
                                                            $tlf_movil,
                                                            $email);
        if($insertar === FALSE){
            $this->session->set_flashdata('pnotify','exists');
            redirect('principal','refresh');
        }
     }
}
