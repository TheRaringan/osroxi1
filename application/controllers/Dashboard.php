<?php
    class Dashboard extends CI_Controller{
        //Vistas Estaticas
        /*public function index($page="dashboard"){
            //Chequea si la Vista Existe
            if(!file_exists(APPPATH.'views/dashboard/'.$page.'.php')){
                //Si la vista no existe muestra error
                show_404();
            }else{
                //Sino, Carga la Vista
                $data['title'] = ucfirst($page);

                $this->load->view('templates/header');
                $this->load->view('templates/navegator');
                $this->load->view('dashboard/'.$page,$data);
                $this->load->view('templates/footer');
            }
        }*/

        public function report(){
            //Chequea si la Vista Existe
            //Sino, Carga la Vista
            if(!$this->session->userdata('logged_in')){
                redirect('');
            }
            $data['title'] = 'Panel de Control';
            
            $data['test'] = $this->upsas_localizacion_model->get_upsa_agric_edo();

            $this->load->view('templates/header');
            $this->load->view('templates/navegator');
            $this->load->view('dashboard/dashboard',$data);
            $this->load->view('templates/footer_morris');
        }

        public function index(){
            if(!$this->session->userdata('logged_in')){
                redirect('');
            }

            if($_SERVER['REQUEST_METHOD'] === 'POST'){

                if($this->upsas_localizacion_model->checkUpsa()){
                    $this->upsas_localizacion_model->updateCoords();
                    redirect('dashboard');
                }else{
                    $this->upsas_localizacion_model->insertUpsaCoords();
                    redirect('dashboard');
                }
            }else{
                $data['title'] = ucfirst('google map');
                $data['upsas'] = $this->upsas_localizacion_model->getUpsas();
                $this->load->view('templates/header');
                $this->load->view('templates/navegator');
                $this->load->view('dashboard/upsas',$data);
                $this->load->view('dashboard/footer');
            }
        }
    }
?>
