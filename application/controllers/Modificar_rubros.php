<?php
class Modificar_rubros extends CI_Controller{

    Public $pnoti;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('modificar_rubros_model', '', TRUE);
        //$this->load->model('auditoria_model', '', TRUE);
        $this->load->helper(array('url', 'form'));
            $this->pnoti=0;
    } 

     public function index()
    {
        if(!$this->session->userdata('logged_in')){
            redirect('');
        }
        $data = array('titulo' => 'Inicio');
        $data['rubros'] = $this->modificar_rubros_model->consultar_rubros();
        $this->load->view('templates/header', $data);
        $data = array('seccion_n' => 'ECOPRO-UPSA', 'sub_titulo' => 'Estadisticas');
        $this->load->view('templates/navegator', $data);
        $usuario =$this->session->userdata('usuario');
        //$auditoria=new auditoria_model();
		//$rs=$auditoria->registrar_auditoria("Inicio","Inicio de Sesion (usuario:".$usuario.")");

		 //$data['grafica'] = $this->principal_models->cargar_grafica();


        $this->load->view('modificar_rubros/index', $data);
        $this->load->view('templates/footer');

    }

    public function modificar()
    {
                $id = $this->input->get('id');
                $data['rubro'] = $this->modificar_rubros_model->consultar_rubro($id);
                $this->load->view('templates/header', $data);
                $this->load->view('templates/navegator', $data);
                $this->load->view('modificar_rubros/modificar_rubro.php', $data);
                $this->load->view('templates/footer.php');
                $usuario =$this->session->userdata('usuario');
    }

    public function modificar_rubro()
        {
            $id = $this->input->post('id');
            $nombre = $this->input->post('nombre');
            $rendimiento = $this->input->post('rendimiento');
            $densidad = $this->input->post('densidad');
            $ciclo = $this->input->post('ciclo');
            $estatus = $this->input->post('estatus');

            $resultado = $this->modificar_rubros_model->modificar_rubro($id, $nombre, $rendimiento, $densidad, $ciclo, $estatus);
            if ($resultado)
            {
                $this->session->set_flashdata('pnotify','rubro_modificado');
                redirect('modificar_rubros/index', 'refresh');
                
            }
            else {
                redirect('modificar_rubros/index', 'refresh');
            }
        }    

        public function estatus()
        {

            $id = $this->input->get('id');
            $estatus = $this->input->get('estatus');
            $resultado = $this->modificar_rubros_model->modificar_estatus($id, $estatus);
            if ($resultado)
            {
                $this->session->set_flashdata('pnotify','rubro_modificado');
                redirect('modificar_rubros/index', 'refresh');
                
            }
            else {
                redirect('modificar_rubros/index', 'refresh');
            }
        }

        public function agregar_rubro()
        {
                $data = array('titulo' => 'Inicio');
                $data = array('seccion_n' => 'ECOPRO-UPSA', 'sub_titulo' => 'Estadisticas');
                $this->load->view('templates/header', $data);
                $this->load->view('templates/navegator', $data);
                $usuario =$this->session->userdata('usuario');
                $this->load->view('modificar_rubros/agregar_rubro.php', $data);
                $this->load->view('templates/footer.php');
        }

        public function agregar_rubro_s()
        {
            $nombre = $this->input->post('nombre');
            $rendimiento = $this->input->post('rendimiento');
            $densidad = $this->input->post('densidad');
            $ciclo = $this->input->post('ciclo');
            $estatus = $this->input->post('estatus');

            $resultado = $this->modificar_rubros_model->agregar_rubro_s($nombre, $rendimiento, $densidad, $ciclo, $estatus);
            if ($resultado)
            {
                redirect('modificar_rubros/index', 'refresh');
                
            }
            else {
                redirect('modificar_rubros/index', 'refresh');
            }
        }
}
