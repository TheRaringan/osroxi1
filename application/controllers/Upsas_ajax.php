<?php
    class Upsas_ajax extends CI_Controller{

      public function __construct()
       {
           parent::__construct();
           $this->load->model('upsas_localizacion_model', '', TRUE);
           $this->load->helper(array('url', 'form'));
               $this->pnoti=0;
       }

        public function get_upsas_coords(){
            $data = $this->upsas_localizacion_model->get_upsas_coords();
            echo json_encode($data);
        }

        public function get_upsa_agric_edo(){
            $data = $this->upsas_localizacion_model->get_upsa_agric_edo();
            echo json_encode($data);
        }
    }
?>
