<?php
class Ficha_upsa extends CI_Controller{

    Public $pnoti;

   public function __construct()
    {
        parent::__construct();
        $this->load->model('Ficha_upsa_model', '', TRUE);
        $this->load->helper(array('url', 'form'));
        //$this->load->model('auditoria_model', '', TRUE);
            $this->pnoti=0;
    }

     public function index(){
        if(!$this->session->userdata('logged_in')){
            redirect('');
        }
        $data['seccion_n'] = 'Reporte UPSAS';
        $a=$this->pnoti;

        $listado = $this->Ficha_upsa_model->ficha_upsa();

        $data = array('inicio' => 'INICIAR SESI&Oacute;N',
            'titulo' => 'Datos Basicos de la UPSA',
            'subtitulo' => 'Coordinador',
             'Listado' => $listado
        );
        $this->load->view('templates/header');
        $this->load->view('templates/navegator', $data);
        $this->load->view('ficha_upsa/index', $data);
        $this->load->view('templates/footer');
        //$usuario=$_SESSION['usuario'];
       //$auditoria=new auditoria_model();
        //$rs=$auditoria->registrar_auditoria("Inicio","Ingrso a ventana de personas (usuario:".$usuario.")");
    }


	public function listar(){
            $postData = $this->input->post();
            $data = $this->Ficha_upsa_model->ficha_upsa_2($postData);
            echo json_encode($data);
        }
  }
