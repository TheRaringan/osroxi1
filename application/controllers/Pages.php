<?php
    class Pages extends CI_Controller{
        //Vistas Estaticas
        public function view($page="home"){
            //Chequea si la Vista Existe
            if(!file_exists(APPPATH.'views/pages/'.$page.'.php')){
                //Si la vista no existe muestra error
                show_404();
            }else{
                //Sino, Carga la Vista
                $data['title'] = ucfirst($page);
                
                $this->load->view('templates/header');
                $this->load->view('templates/navigator');
                $this->load->view('pages/'.$page,$data);
                $this->load->view('templates/footer');
            }
        }
    }
?>