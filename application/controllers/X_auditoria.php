<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Contacto
 *
 * @author Nelly Moreno
 */
class X_auditoria extends CI_Controller{
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Listar_auditoria_model', '', TRUE);
        
    }
   
    public function index()
    {
        
        $listado = $this->Listar_auditoria_model->listar_auditoria();
        
        $data = array('inicio' => 'INICIAR SESI&Oacute;N',
					  'Listado' => $listado);
       
        $this->load->view('x_auditoria/index', $data);
   
        
    }
    

}
