<?php
class Principal extends CI_Controller{

    Public $pnoti;

   public function __construct()
    {
        parent::__construct();
        $this->load->model('principal_models', '', TRUE);
        //$this->load->model('auditoria_model', '', TRUE);
        $this->load->helper(array('url', 'form'));
            $this->pnoti=0;
    }

     public function index()
    {
        if(!$this->session->userdata('logged_in')){
            redirect('');
        }
        $data = array('titulo' => 'Inicio');

        $this->load->view('templates/header', $data);
        $data = array('seccion_n' => 'ECOPRO-UPSA', 'sub_titulo' => 'Estadisticas');
        $this->load->view('templates/navegator', $data);
        $usuario =$this->session->userdata('usuario');
        //$auditoria=new auditoria_model();
		//$rs=$auditoria->registrar_auditoria("Inicio","Inicio de Sesion (usuario:".$usuario.")");

		 //$data['grafica'] = $this->principal_models->cargar_grafica();


        $this->load->view('principal/index', $data);
        $this->load->view('templates/footer');

    }
}
