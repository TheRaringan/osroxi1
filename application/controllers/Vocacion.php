<?php
  class Vocacion extends CI_Controller{
    public function __construct(){
      parent::__construct();
      $this->load->model('vocacion_model','',TRUE);
    }
    public function insertar_recurso(){
			$id_upsa = $this->session->userdata('id_upsa');
			$id_clase = $this->input->post('clase[]');
			$count = count($id_clase);
			for($i=0;$i<$count;$i++){
				$this->vocacion_model->insertar_vocacion($i,$id_upsa,$id_clase);
			}
			redirect('principal');
		}
  }