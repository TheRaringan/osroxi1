<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Sislogin Pagina principal del Sistema
 *
 * @author Nelly Moreno
 */
class Ecopro_upsa extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->helper(array('url', 'form'));
    }
    public function index()
    {
        if($this->session->userdata('logged_in')){
            redirect('principal');
        }
        $data = array('titulo' => 'ECOPRO-UPSA');
        $this->load->view('templates/header', $data);
        //$this->load->view('notificaion');
        //¿$data = array('inicio' => 'INICIAR SESI&Oacute;N');
        $this->load->view('inicio/index', $data);

    }}
