<?php
class Registro_alianza extends CI_Controller{

    Public $pnoti;

   public function __construct()
    {
        parent::__construct();
        $this->load->model('Registro_alianza_model', '', TRUE);
        $this->load->helper(array('url', 'form'));
        //$this->load->model('auditoria_model', '', TRUE);
            $this->pnoti=0;
    }

     public function index(){
        if(!$this->session->userdata('logged_in')){
            redirect('');
        }
        $a=$this->pnoti;
        $upsas = $this->Registro_alianza_model->get_upsas();
        $estados = $this->Registro_alianza_model->get_estado();


        $data = array(
            'estados' => $estados,
            'upsas' => $upsas
            );
        $this->load->view('templates/header');
        $this->load->view('templates/navegator', $data);
        $this->load->view('registro_alianza/index', $data);
        $this->load->view('templates/footer');
        //$usuario=$_SESSION['usuario'];
       //$auditoria=new auditoria_model();
        //$rs=$auditoria->registrar_auditoria("Inicio","Ingrso a ventana de personas (usuario:".$usuario.")");
    }

    public function get_municipio(){
      $postData = $this->input->post();
      $data = $this->ubicacion_model->get_municipio($postData);
      echo json_encode($data);
    }

    public function get_parroquia(){
      $postData = $this->input->post();
      $data = $this->ubicacion_model->get_parroquia($postData);
      echo json_encode($data);
    }

    public function registro_alianza(){

        $id_upsa = $this->input->post('id_upsa');
        $nombre_alianza = $this->input->post('nombre_alianza');
        $responsable = $this->input->post('responsable');
        $rif = $this->input->post('rif');
        $telefono = $this->input->post('telefono');
        $direccion = $this->input->post('direccion');
        $correo = $this->input->post('correo');
        $id_estado = $this->input->post('id_estado');
        $id_municipio = $this->input->post('id_municipio');
        $id_parroquia = $this->input->post('id_parroquia');
        $latitud = $this->input->post('latitud');
        $longitud = $this->input->post('longitud');
        $objeto = $this->input->post('objeto');
        $aportes = $this->input->post('aportes');
        $fecha_suscripcion = $this->input->post('fecha_suscripcion');
        $duracion = $this->input->post('duracion');
        $utilidad = $this->input->post('utilidad');
        $produccion = $this->input->post('produccion');
        $ba_array = $this->input->post('bienes_asignados');
        $proyecto = $this->input->post('proyecto');
        $fecha_siembra = $this->input->post('fecha_siembra');
        $tiempo_cosecha = $this->input->post('tiempo_cosecha');
        $has_sembradas = $this->input->post('has_sembradas');
        $has_cosechadas = $this->input->post('has_cosechadas');
        $fecha_cosecha = $this->input->post('fecha_cosecha');
        $peso_liquidar = $this->input->post('peso_liquidar');
        $silo = $this->input->post('silo');
        $nombre_contacto = $this->input->post('nombre_contacto');
        $cedula_contacto = $this->input->post('cedula_contacto');
        $telefono_contacto = $this->input->post('telefono_contacto');
        $informacion = $this->input->post('informacion');

        for ($i = 0 ; $i < count ($ba_array) ; $i++)    
        {     
            $bienes_asignados .= $ba_array[$i];
            if($i < count ($ba_array)-1)
                $bienes_asignados .= ", ";

        } 


        $resultado = $this->Registro_alianza_model->registro_alianza($id_upsa, $nombre_alianza, $responsable, $rif, $telefono, $direccion, $correo, $id_estado, $id_municipio, $id_parroquia, $latitud, $longitud, $objeto, $aportes, $fecha_suscripcion, $duracion, $utilidad, $produccion, $bienes_asignados, $proyecto, $fecha_siembra, $tiempo_cosecha, $has_sembradas, $has_cosechadas, $fecha_cosecha, $peso_liquidar, $silo, $nombre_contacto, $cedula_contacto, $telefono_contacto, $informacion);


        if ($resultado)
        {
            $this->session->set_flashdata('pnotify','insert');
            redirect('alianza/', 'refresh');
            
        }else {
            $this->session->set_flashdata('pnotify','fail');
            redirect('registro_alianza/', 'refresh');
        }

    }
  }
