<?php
defined('BASEPATH') OR exit('No direct script access allowed');

  class Consultas_ajax extends CI_Controller{
		public function __construct(){
				parent::__construct();
				$this->load->model('select_recursos_model', '', TRUE);
				$this->load->model('select_actividades_model', '', TRUE);
				$this->load->model('basicos_model','',TRUE);
				$this->load->model('ubicacion_model', '', TRUE);
				$this->load->model('capacidad_model', '', TRUE);
				$this->load->model('vocacion_model', '', TRUE);
				//$this->load->helper(array('url', 'form'));
		}

		########################## RECURSOS HIDRICOS ###############################################
		//DEPENDIENTE RECURSOS HIDRICOS
		public function get_recursos_hidricos(){
			$postData = $this->input->post();
			$data = $this->select_recursos_model->get_recursos_hidricos($postData);
			echo json_encode($data);
		}

		######################### ACTIVIDADES ECONOMICAS ############################################
				//DEPENDIENTE RECURSOS HIDRICOS
		public function get_actividades(){
			$postData = $this->input->post();
			$data = $this->select_actividades_model->get_actividades($postData);
			echo json_encode($data);
		}

		public function get_rubros(){
			$postData = $this->input->post();
			$data = $this->select_actividades_model->get_rubros($postData);
			echo json_encode($data);
		}

		public function get_subrubros(){
			$postData = $this->input->post();
			$data = $this->select_actividades_model->get_subrubros($postData);
			echo json_encode($data);
		}

		public function get_info_upsa(){
			$data = $this->basicos_model->consultar_registro_upsa();
			echo json_encode($data);
		}

		public function get_info_ubicacion(){
			$data = $this->ubicacion_model->consultar_registro_ubicacion();
			echo json_encode($data);
		}

		public function get_info_capacidad(){
			$data = $this->capacidad_model->consultar_registro_capacidad();
			echo json_encode($data);
		}

		public function get_info_vocacion(){
			$data = $this->vocacion_model->consultar_registro_vocacion();
			echo json_encode($data);
		}
	}
