<?php
class Modificar_insumos extends CI_Controller{

    Public $pnoti;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('modificar_insumos_model', '', TRUE);
        //$this->load->model('auditoria_model', '', TRUE);
        $this->load->helper(array('url', 'form'));
            $this->pnoti=0;
    } 

     public function index()
    {
        if(!$this->session->userdata('logged_in')){
            redirect('');
        }
        $data = array('titulo' => 'Inicio');
        $data['insumos'] = $this->modificar_insumos_model->consultar_insumos();
        $this->load->view('templates/header', $data);
        $data = array('seccion_n' => 'ECOPRO-UPSA', 'sub_titulo' => 'Estadisticas');
        $this->load->view('templates/navegator', $data);
        $usuario =$this->session->userdata('usuario');
        //$auditoria=new auditoria_model();
		//$rs=$auditoria->registrar_auditoria("Inicio","Inicio de Sesion (usuario:".$usuario.")");

		 //$data['grafica'] = $this->principal_models->cargar_grafica();


        $this->load->view('modificar_insumos/index', $data);
        $this->load->view('templates/footer');

    }

    public function general()
    {
        if(!$this->session->userdata('logged_in')){
            redirect('');
        }
        $data = array('titulo' => 'Inicio');
        $data['insumos'] = $this->modificar_insumos_model->consultar_insumos_general();
        $this->load->view('templates/header', $data);
        $data = array('seccion_n' => 'ECOPRO-UPSA', 'sub_titulo' => 'Estadisticas');
        $this->load->view('templates/navegator', $data);
        $usuario =$this->session->userdata('usuario');
        //$auditoria=new auditoria_model();
		//$rs=$auditoria->registrar_auditoria("Inicio","Inicio de Sesion (usuario:".$usuario.")");

		 //$data['grafica'] = $this->principal_models->cargar_grafica();


        $this->load->view('modificar_insumos/general', $data);
        $this->load->view('templates/footer');

    }

     public function modificar()
    {
                $id = $this->input->get('id');
                $data['rubros'] = $this->modificar_insumos_model->consultar_rubros();
                $data['insumo'] = $this->modificar_insumos_model->consultar_insumo($id);
                $this->load->view('templates/header', $data);
                $this->load->view('templates/navegator', $data);
                $this->load->view('modificar_insumos/modificar_insumo.php', $data);
                $this->load->view('templates/footer.php');
                $usuario =$this->session->userdata('usuario');
    } 

        public function agregar_insumos()
        {
                $data['rubros'] = $this->modificar_insumos_model->consultar_rubros();
                $this->load->view('templates/header', $data);
                $this->load->view('templates/navegator', $data);
                $this->load->view('modificar_insumos/agregar_insumos.php', $data);
                $this->load->view('templates/footer.php');
        }

        public function agregar_insumo_s()
        {
            $rubro = $this->input->post('rubro');
            $tipo = $this->input->post('tipo');
            $insumo = $this->input->post('insumo');
            $medida = $this->input->post('medida');
            $cantidad = $this->input->post('cantidad');
            $bolivares = $this->input->post('bolivares');
            /* $dolares = $this->input->post('dolares'); */

            $resultado = $this->modificar_insumos_model->agregar_insumo_s($rubro, $tipo, $insumo, $medida, $cantidad, $bolivares);
            if ($resultado)
            {
                redirect('modificar_rubros/index', 'refresh');
                
            }
            else {
                redirect('modificar_rubros/index', 'refresh');
            }
        }

        public function modificar_insumos()
        {
            $id_insumo = $this->input->post('id_insumo');
            $rubro = $this->input->post('rubro');
            $tipo = $this->input->post('tipo');
            $insumo = $this->input->post('insumo');
            $medida = $this->input->post('medida');
            $cantidad = $this->input->post('cantidad');
            $bolivares = $this->input->post('bolivares');
            /* $dolares = $this->input->post('dolares'); */

            $resultado = $this->modificar_insumos_model->modificar_insumo($id_insumo, $rubro, $tipo, $insumo, $medida, $cantidad, $bolivares);
            if ($resultado)
            {
                redirect('modificar_insumos/index', 'refresh');
                
            }
            else {
                redirect('modificar_insumos/index', 'refresh');
            }
        } 
}
