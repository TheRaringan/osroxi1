<div class="right_col" role="main">
	<div class="clearfix"></div>
	<div class="row">
		<div class="col-md-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>
						<?=$titulo?>
					</h2>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					<?= form_open('ubicacion/insertar_ubicacion', array('name' => 'formulario', 'id' => 'formulario_ubicacion'))?>
						<br />
						<form class="form-horizontal form-label-left">
							<div class="row">
								<div class="col-md-12 col-sm-12 col-xs-12 form-group">
									<label>UPSA</label>
									<input name="id_upsa" type="text" id="id_upsa" class="form-control" value="<?=$this->session->userdata('nombre_upsa')?>"
									  disabled>
								</div>
							</div>
							<div id="ubicacion_selects" class="row">
								<div class="col-md-3 col-sm-12 col-xs-12 form-group">
									<label>REGIÓN</label>
									<select name="id_region" id="id_region" class="select2_single form-control">
										<option value="0">SELECCIONE</option>
										<?php foreach($regiones as $region): ?>
										<option value="<?=$region['id_region']?>">
											<?=$region['descripcion']?>
										</option>
										<?php endforeach; ?>
									</select>
								</div>

								<div class="col-md-3 col-sm-12 col-xs-12 form-group">
									<label>ESTADO</label>
									<select class="select2_single form-control" id="id_estado" name="id_estado">
										<option value="0">SELECCIONE</option>
									</select>
								</div>

								<div class="col-md-3 col-sm-12 col-xs-12 form-group">
									<label>MINICIPIO</label>
									<select class="select2_single form-control" id="id_municipio" name="id_municipio">
										<option value="0">SELECCIONE</option>
									</select>
								</div>

								<div class="col-md-3 col-sm-12 col-xs-12 form-group">
									<label>PARROQUIA</label>
									<select class="select2_single form-control" id="id_parroquia" name="id_parroquia">
										<option value="0">SELECCIONE</option>
									</select>
								</div>
							</div>
							<div class="row">
								<div id="ubicacion_registrada" class="col-md-3 col-sm-12 col-xs-12 form-group">
								</div>
							</div>

							<div class="row">
								<div id="cdo_upsa" class="col-md-3 col-sm-12 col-xs-12 form-group">
									<label>COD. UPSA</label>
									<input name="cod_upsa" type="text" id="cod_upsa" placeholder="Cod. UPSA" class="form-control" onKeyUp="this.value=this.value.toUpperCase();" onKeyPress="return valida(event,this,2,100)"
									  onBlur="valida2(this,2,100)">
								</div>
								<div class="col-md-9 col-sm-12 col-xs-12 form-group">
									<label>DIRECCIÓN UPSA</label>
									<input type="text" name="direccion" id="direccion" placeholder="DIRECCIÓN COMPLETA" class="form-control"  onKeyUp="this.value=this.value.toUpperCase();" onKeyPress="return valida(event,this,2,100)"
									  onBlur="valida2(this,2,100)">
								</div>
							</div>

							<div class="row">
								<div id="lindero_norte" class="col-md-3 col-sm-12 col-xs-12 form-group">
									<label>LINDERO NORTE</label>
									<input name="lindero_norte" type="text" id="lindero_norte" placeholder="LINDERO NORTE" class="form-control" onKeyUp="this.value=this.value.toUpperCase();" onKeyPress="return valida(event,this,2,100)"
									  onBlur="valida2(this,2,100)">
								</div>
								<div id="lindero_sur" class="col-md-3 col-sm-12 col-xs-12 form-group">
									<label>LINDERO SUR</label>
									<input name="lindero_sur" type="text" id="lindero_sur" placeholder="LINDERO SUR" class="form-control"  onKeyUp="this.value=this.value.toUpperCase();" onKeyPress="return valida(event,this,2,100)"
									  onBlur="valida2(this,2,100)">
								</div>
								<div id="lindero_este" class="col-md-3 col-sm-12 col-xs-12 form-group">
									<label>LINDERO ESTE</label>
									<input name="lindero_este" type="text" id="lindero_este" placeholder="LINDERO ESTE" class="form-control" onKeyUp="this.value=this.value.toUpperCase();" onKeyPress="return valida(event,this,2,100)"
									  onBlur="valida2(this,2,100)">
								</div>
								<div name="lindero_oeste" id="lindero_oeste" class="col-md-3 col-sm-12 col-xs-12 form-group">
									<label>LINDERO OESTE</label>
									<input name="lindero_oeste" type="text" id="lindero_oeste" placeholder="LINDERO OESTE" class="form-control" onKeyUp="this.value=this.value.toUpperCase();" onKeyPress="return valida(event,this,2,100)"
									  onBlur="valida2(this,2,100)">
								</div>
							</div>
							<div class="clearfix"></div>
							<div class="col-md-12 col-sm-12 col-xs-12">
								<br>
								<div class="text-center">
									<button type="submit" class="btn btn-dark" id="guardar_ubicacion" name="guardar_ubicacion" value="Guardar"> GUARDAR</button>
								</div>
							</div>
							<?= form_close()?>
				</div>
			</div>
		</div>
	</div>
</div>
