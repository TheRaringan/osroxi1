<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>ECOPRO | UPSA</title>

    <!-- Bootstrap -->
    <link href="<?=base_url()?>plantilla/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?=base_url()?>plantilla/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?= base_url() ?>plantilla/css/custom.css" rel="stylesheet">

    <link href="<?=base_url()?>plantilla/vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    <link href="<?=base_url()?>plantilla/vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    <link href="<?=base_url()?>plantilla/vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/ccs/styles.css" rel="stylesheet">
  </head>

    <body style="background:#F7F7F7;" onload="Captcha();" oncopy="return false" onpaste="return false">
        <div class="">
          <a class="hiddenanchor" id="toregister"></a>
          <a class="hiddenanchor" id="tologin"></a>

            <div id="wrapper">
                <div id="login" class="form">
                    <section class="login_content">
                        <div class="login-logo">
                            <a href="#"><img src="<?php echo base_url(); ?>assets/images/corpo.png" style="height: 150px; margin-top:-20px" alt=""></a>
                        </div>
                        <form name="frmLogin" id="frmLogin" method="post" action ="usuarios/login" autocomplete="off">

                            <h1>ECOPRO - UPSA</h1>
                            <div>
                                <input type="text" class="form-control" name="usuario" id="usuario"  placeholder="Usuario"  onKeyUp="this.value=this.value.toUpperCase();" required="" />
                            </div>
                            <div>
                                <input type="password" class="form-control" name="clave" id="clave" placeholder="Clave" required="" />
                            </div>
                            <!--CATCHA-->
                            <!--<div id="captcha-div">
                                <div class="g-recaptcha" data-sitekey="6LfVrnMUAAAAAP3bgz9h68cSZ5sgT3qVbUMQNs3I"></div>
                            </div>-->

                            <div>
                                <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                    <h5 >Si no comprende la imagen, recargue aqui!</h5>
                                    <img src="<?php echo base_url(); ?>assets/images/recarga.svg" style="height: 40px;" id="refrescar" onclick="Captcha();">
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <h5>Captcha</h5>
                                    <input type="text" id="captcha" name="captcha" class="captcha" size="4" readonly onmousedown="return false;">
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                    <h5>Respuesta</h5>
                                    <input type="text" name="respuesta" id="respuesta" onKeyUp="this.value=this.value.toUpperCase();" class="captcha2">
                                </div>

                            </div>

                            <div>
                                <br><br><br><br><br><br><br>
                                <!-- //////////////////// -->
                                <div class="col-xs-offset-3 col-md-3 col-sm-3 col-xs-3">
                				    <input type="submit" value="Entrar" class="btn btn-default submit"/>
                                </div>
                                <div class="clearfix"></div>
                                <div class="separator">
                                    <div class="clearfix"></div>
                                    <br />
                                    <div>
                                        <h1><i class="fa fa-share" style="font-size: 26px;"></i>Acceso</h1>
                                        <p>©2016 Desarrollado por Oficina de Tenología de la Información DELAGRO</p>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </section>
                </div>
            </div>
        </div>
    <!-- jQuery -->
    <script src="<?=base_url()?>plantilla/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?=base_url()?>plantilla/vendors/bootstrap/dist/js/bootstrap.min.js"></script>


<script src='https://www.google.com/recaptcha/api.js'></script>
    <!-- PNotify -->

    <script src="<?=base_url()?>plantilla/vendors/pnotify/dist/pnotify.js"></script>
    <script src="<?=base_url()?>plantilla/vendors/pnotify/dist/pnotify.buttons.js"></script>
    <script src="<?=base_url()?>plantilla/vendors/pnotify/dist/pnotify.nonblock.js"></script>
    <script src="<?=base_url()?>plantilla/vendors/pnotify/dist/pnotify.js"></script>
    <script src="<?=base_url()?>plantilla/vendors/pnotify/dist/pnotify.buttons.js"></script>
    <script src="<?=base_url()?>plantilla/vendors/pnotify/dist/pnotify.nonblock.js"></script>

    <script src="<?=base_url()?>js/alertas.js"></script>

    <script type="text/javascript">
        function Captcha(){
             var alpha = new Array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
         	    	'0','1','2','3','4','5','6','7','8','9');
             var i;
             for (i=0;i<6;i++){
                 var a = alpha[Math.floor(Math.random() * alpha.length)];
                 var b = alpha[Math.floor(Math.random() * alpha.length)];
                 var c = alpha[Math.floor(Math.random() * alpha.length)];
                 var d = alpha[Math.floor(Math.random() * alpha.length)];
                 var e = alpha[Math.floor(Math.random() * alpha.length)];
                 var f = alpha[Math.floor(Math.random() * alpha.length)];
                 var g = alpha[Math.floor(Math.random() * alpha.length)];
                              }
                 var code = a + '' + b + '' + '' + c + '' + d + '' + e + ''+ f + '' + g;
                 document.getElementById("captcha").innerHTML = code
        		 document.getElementById("captcha").value = code
               }
        function removeSpaces(string){
             return string.split(' ').join('');
        }
    </script>
  </body>
</html>
