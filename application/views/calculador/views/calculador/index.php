<script language="JavaScript" type="text/javascript">




</script>

<?php //var_dump($data); ?>
<script type="text/javascript" src="http://localhost/sislogin/js/cargar_insumos.js"></script>

<?= form_open('trabajadores/procesardatos', array('name' => 'formulario', 'id' => 'formulario'))?>
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
        <div class="title_left">
            <h3><i class="fa fa-envelope-o"></i> Calculador </h3> 
        </div>
        <div class="title_right">
        </div>
    </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="x_panel">
                <div class="x_title">
                  <h2>Calculador</h2>
                 
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
      					    <div class="col-md-4 col-sm-12 col-xs-12 form-group">
          						  <select name ="id_rubro" id ="id_rubro" class="select2_single form-control" tabindex="-1">
              							<option value='0'> Rubros </option>
              							<?php foreach ($rubros as $row) : ?>
              								   <option value='<?=$row[0]?>'> <?=$row[1]?> </option>
              						   <?php endforeach; ?>
          						  </select>
        						</div>
                    <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                        <input type="text" name ="extension" id ="extension" placeholder="Hectáreas" class="form-control"  onKeyPress="return valida(event,this,10,4)" onBlur="valida2(this,10,4)" >
                    </div>
      					     <button type="button" class="btn btn-round btn-info" onclick="buscar_insumo(this)" >Generar Proyecto</button>
      					   
                </div>
                <div>
                    <label>Rendimiento:</label> <span id="rendimiento" > </span>
                    <label>| Densidad:</label> <span id="densidad"> </span>
                    <label>| Ciclo:</label> <span id="ciclo" > </span>
                </div>
                <div id="contenedor_tabla">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Tipo</th>
                                <th>Insumo</th>
                                <th>Unidad de medida</th>
                                <th>Cantidad</th>
                                <th>Costo</th>
                                <th>Cantidad * Costo </th>
                            </tr>
                        </thead>
                        <tbody id="tbody_tablas">
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
  </div>
 <?= form_close()?>