//alert("Hello! I am an alert box!!");
function buscar_insumo(){ 
	var id_rubro = $("#id_rubro").val();
	var data = {
		'id_rubro':id_rubro
		};
		
		$.ajax({
			url:"http://localhost/sislogin/index.php/calculador/cargar_insumos",
			type:"POST",
			data: data,
			cache: false,
			error: function(resp){alert(resp);},
			success: function(resp){
							var recordset=$.parseJSON(resp);
							$("#tbody_tablas").html("");
							crear_tabla(recordset);
						}
			});
}

function buscar_rubros_todos(){ 
	var id_rubro = $("#id_rubro").val();
	var data = {
		'id_rubro':id_rubro
		};
		
		$.ajax({
			url:"http://localhost/sislogin/index.php/calculador/cargar_rubros_todos",
			type:"POST",
			data: data,
			cache: false,
			error: function(resp){alert(resp);},
			success: function(resp){
					var recordset=$.parseJSON(resp);
					$("#rendimiento").text(recordset[0][0]);
					$("#densidad").text(recordset[0][1]);
					$("#ciclo").text(recordset[0][2]);
			}
		});
}

function crear_tabla(recordset){
	var tabla = "";
	var cont = 1;
	var acum = 0;
	$.each( recordset, function( clave, valor ){
	  	//alert( "Index #" + clave + ": " + valor["tipo"] );
	  	
	  	extension = $("#extension").val();
	  	
	  	extension_por_cantidad = extension * valor["cantidad"];

	    cantidad_por_costo = extension_por_cantidad*valor["costo"];

   	  	acum = acum+ cantidad_por_costo;

	  	tabla="<tr>\
	  					<th>"+cont+"</th>\
	  					<th>"+valor["tipo"]+"</th>\
	  					<th>"+valor["insumo"]+"</th>\
	  					<th>"+valor["unidad_medida"]+"</th>\
	  					<th>"+extension_por_cantidad+"</th>\
	  					<th>"+valor["costo"]+"</th>\
	  					<th>"+cantidad_por_costo+"</th>\
	  	</tr>";
	  	$("#tbody_tablas").append(tabla);
	  	tabla = "";
	  	cont++;
	});
	//Total
	tabla2="<tr>\
	  					<th></th>\
	  					<th></th>\
	  					<th></th>\
	  					<th></th>\
	  					<th></th>\
	  					<th>Total </th>\
	  					<th>"+acum+"</th>\
	  	</tr>";
	  	
	$("#tbody_tablas").append(tabla2);

	buscar_rubros_todos();
}
