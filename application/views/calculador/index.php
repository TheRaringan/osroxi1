<?php //var_dump($data); ?>

<!-- <script type="text/javascript" src="http://ecopro.corpodelagro.gob.ve/js/cargar_insumos.js"></script> -->

<script type="text/javascript">
     function anular(e) {
          tecla = (document.all) ? e.keyCode : e.which;
          return (tecla != 13);
     }
</script>


<script type="text/javascript" src="http://localhost/ecopro_upsa/js/cargar_insumos.js"></script>


<?= form_open('trabajadores/procesardatos', array('name' => 'formulario', 'id' => 'formulario', 'onkeypress' => 'return anular(event)'))?>
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3><i class="fa fa-calculator"></i> CALCULADOR </h3>
            </div>
            <div class="title_right"></div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="x_panel">
                <div class="x_title">
                    <h2>CALCULADOR DE PROYECTOS AGRÍCOLAS</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
  					<div class="col-md-3 col-sm-12 col-xs-12 form-group">
      				  <select name ="id_rubro" id ="id_rubro" class="select2_single form-control" tabindex="-1">
          					<option value='0'> RUBROS </option>
          						<?php foreach ($rubros as $row) : ?>
          						   <option value='<?=$row[0]?>'> <?=$row[1]?> </option>
          					   <?php endforeach; ?>
      					</select>
    				</div>
            <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                <input type="text" name ="extension" id ="extension" placeholder="HECTÁREAS" class="form-control"  onKeyPress="return valida(event,this,10,4)" onBlur="valida2(this,10,4)" >
            </div>
            
            <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                <input type="text" name ="dolar" id ="dolar" placeholder="PRECIO DOLAR" class="form-control"  onKeyPress="return valida(event,this,10,4)" onBlur="valida2(this,10,4)" >
            </div>
  					<button type="button" class="btn btn-round btn-info" onclick="buscar_insumo(this)">GENERAR PROYECTO</button>
                </div>
                <div>
                    <label>RENDIMIENTO:</label> <span id="rendimiento" > </span>
                    <label>| DENSIDAD:</label> <span id="densidad"> </span>
                    <label>| CICLO:</label> <span id="ciclo" > </span>
                </div>
                <div id="contenedor_tabla">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>TIPO</th>
                                <th>INSUMO</th>
                                <th>UNIDAD DE MEDIDA</th>
                                <th>CANTIDAD</th>
                                <th>PRECIO DOLARES</th>
                                <th>PRECIO Bs.S</th>
                                <th>TOTAL DOLARES</th>
                                <th>TOTAL Bs.S</th>
                            </tr>
                        </thead>
                        <tbody id="tbody_tablas">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
 <?= form_close()?>
