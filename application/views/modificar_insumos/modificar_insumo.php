<div class="main_container">
    <div class="right_col" role="main">
        <form action="modificar_insumos" method="POST" id="demo-form2" autocomplete="off">
            <div class="x_panel">
                <div class="x_title">
                    <div>
                        <h3><i class="fa fa-edit"></i> MODIFICAR INSUMO - <?=$insumo['nombre']?> </h3>
                    </div>
                </div>
                <div class="x_content">
                    <div class="form-group">
                        <h4>INFORMACIÓN DEL INSUMO</h4>
                        <div class="row">

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>RUBRO</label>
                                    <select name="rubro" id="rubro" class="form-control custom-select">
                                                <option value="" disabled="true" selected> <?=$insumo['nombre']?> </option>
                                            <?php foreach($rubros as $rubros): ?>
                                                <option value="<?=$rubros['id_rubro']?>">
                                                    <?=$rubros['nombre_rubro']?>
                                                </option>
                                            <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                <label>TIPO</label>
                                <input type="text" name="tipo" id="tipo" value="<?=$insumo['tipo']?>" class="form-control" onKeyUp="this.value=this.value.toUpperCase();" onKeyPress="return valida(event,this,0,17)"
                                onBlur="valida2(this,0,17)" required >
                            </div>

                            <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                <label>INSUMO</label>
                                <input type="text" name="insumo" id="insumo" value="<?=$insumo['insumo']?>" class="form-control" onKeyUp="this.value=this.value.toUpperCase();" onKeyPress="return valida(event,this,0,17)"
                                onBlur="valida2(this,0,17)" required >
                            </div>

                        </div>
                        <br>
                        <div class="row">

                            <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                <label>MEDIDA</label>
                                <input type="text" name="medida" id="medida" value="<?=$insumo['medida']?>" class="form-control" onKeyUp="this.value=this.value.toUpperCase();" onKeyPress="return valida(event,this,11)"
                                onBlur="valida2(this,11)" required >
                            </div>

                            <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                <label>CANTIDAD</label>
                                <input type="text" name="cantidad" id="cantidad" value="<?=$insumo['cantidad']?>" class="form-control" onKeyPress="return valida(event,this,10,8)"
                                onBlur="valida2(this,10,8)" required>
                            </div>

                            <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                <label>COSTO DOLARES</label>
                                <input type="text" name="bolivares" id="bolivares" value="<?=$insumo['bolivares']?>" class="form-control" onKeyPress="return valida(event,this,10,8)"
                                onBlur="valida2(this,10,8)" required>
                            </div>
                
                        </div>
                        <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                <input type="hidden" name="id_insumo" id="id_insumo" value="<?=$insumo['id_insumo']?>" class="form-control" onKeyPress="return valida(event,this,10,8)"
                                onBlur="valida2(this,10,8)" readonly>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <div class="ln_solid"></div>
            <div class="form-group">
                <div class="col-md-offset-5 col-md-6">
                    <button type="button" class="btn btn-primary" onclick="location.href = '../modificar_insumos/index'";>Cancelar</button>
                    <button type="submit" id="" class="btn btn-success">Registrar</button>
                </div>
            </div>
        </form>
    </div>
</div>