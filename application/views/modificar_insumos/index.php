<?php 
	$id_estado = $_SESSION['id_estado'];
	$consulta_estado = $this->db->query("SELECT descripcion FROM public.estado WHERE id_estado = $id_estado ")->row_array(); 
?> 
<div class="main_container">
	<div class="right_col" role="main">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
				<div class="col-12">
				<a class="btn btn-dark pull-right my-2 mx-2" href="agregar_insumos"><i class="ft-list"></i> AGREGAR INSUMO</a>
			</div>
					<div class="x_title">
						<h2>CONSULTA GENERAL DE LOS INSUMOS - <?php echo $consulta_estado['descripcion']; ?></h2>
						<div class="clearfix">

						</div>
					</div>
					<div class="x_content">
						<table id="datatable-buttons" data-page-length='10' class="table table-striped table-bordered" style="width: 100%; text-align: center;">
							<thead>
								<tr>
									<th>RUBRO</th>
                                    <th>TIPO</th>
                                    <th>INSUMO</th>
                                    <th>MEDIDA</th>
                                    <th>CANTIDAD</th>
                                    <th>COSTO DOLARES</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
							<?php foreach($insumos as $insumos): ?>
								<tr>
									<td><?=$insumos['nombre']?></td>
                                    <td><?=$insumos['tipo']?></td>
                                    <td><?=$insumos['insumo']?></td>
                                    <td><?=$insumos['medida']?></td>
                                    <td><?=$insumos['cantidad']?></td>
                                    <td><?=$insumos['bolivares']?></td>
									<td><a class="btn btn-dark btn-round" href="<?php echo base_url();?>index.php/modificar_insumos/modificar?id=<?php echo $insumos['id_insumo'];?>"><i class="fa fa-pencil"></i></a></td>
								</tr>
							<?php endforeach; ?> 
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>