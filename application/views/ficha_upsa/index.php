<div class="right_col" role="main">
	<div class="">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>Listado de UPSAS Registradas</h2>
						<div class="clearfix"></div>
					</div>
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
                			<div class="x_panel">
                  				<div class="x_content">
				                  	<table id="datatable-keytable" class="table table-striped table-bordered">
										<thead>
											<tr>
												<th>UBICACIÓN</th>
												<th>Nombre UPSA</th>
												<th>ACCIÓN</th>
											</tr>
										</thead>
										<tbody>
											<?php if($Listado != FALSE):?>
											<?php foreach ($Listado as $row) : ?>
											<tr>
												<td><?php
				  									if ($row[2]=='' ){
				  										echo "<font color=\"red\">SIN CARGAR</font>";;
				  									}else
				  										echo $row[2]?></td>
												<td><?=$row[1]?></td>
												<td>
													<button type="button" data-value="<?=$row[0]?>" class="btn btn-info consulta" data-toggle="modal" data-target="#myModal">Ver</button>
												</td>
											</tr>
											<?php endforeach; ?>
											<?php endif; ?>
										</tbody>
									</table>
                  				</div>
                			</div>
              			</div>

						<div class="modal fade" id="myModal" role="dialog">
						    <div class="modal-dialog modal-lg">
						      	<div class="modal-content">
						        	<div class="modal-header">
						          		<button type="button" class="close" data-dismiss="modal">&times;</button>
						          		<h4 class="modal-title">Información de la Ipsa</h4>
						        	</div>
						        	<div class="modal-body">
										<div role="main">
							          		<div class="">
							            		<div class="clearfix"></div>
						            			<div class="row">
						              				<div class="col-md-6 col-xs-12">
						                				<div class="x_panel">
						                  					<div class="x_title">
						                    					<h2>Ubicacion</h2>
						                    					<div class="clearfix"></div>
						                  					</div>
						                  					<div class="x_content">
											                    <br />
																<form class="form-horizontal form-label-left">
																	<div class="form-group">
																	   <label class="control-label col-md-3 col-sm-3 col-xs-12">Región:</label>
																	   <div class="col-md-19 col-sm-9 col-xs-12">
																		 <input type="text" name="region" id="region" class="form-control" disabled>
																	   </div>
																	</div>
																	<div class="form-group">
																	   <label class="control-label col-md-3 col-sm-3 col-xs-12">Estado:</label>
																	   <div class="col-md-19 col-sm-9 col-xs-12">
																		 <input type="text" name="estado" id="estado" class="form-control" disabled>
																	   </div>
																	</div>
																 	<div class="form-group">
 																	   <label class="control-label col-md-3 col-sm-3 col-xs-12">Municipio:</label>
 																	   <div class="col-md-19 col-sm-9 col-xs-12">
 																		 <input type="text" name="municipio" id="municipio" class="form-control" disabled>
 																	   </div>
 																	</div>
																	<div class="form-group">
 																	   <label class="control-label col-md-3 col-sm-3 col-xs-12">Parroquia:</label>
 																	   <div class="col-md-9 col-sm-9 col-xs-12">
 																		 <input type="text" name="parroquia" id="parroquia" class="form-control" disabled>
 																	   </div>
 																	</div>
																</form>
						                  					</div>
						                				</div>
						              				</div>

									              	<div class="col-md-6 col-xs-12">
									                	<div class="x_panel">
										                  	<div class="x_title">
										                    	<h2>Predio</h2>
										                    	<div class="clearfix"></div>
										                  	</div>
										                  	<div class="x_content">
										                    	<br />
																<form class="form-horizontal form-label-left">
																	<div class="form-group">
																	   <label class="control-label col-md-2 col-sm-3 col-xs-12">Nombre:</label>
																	   <div class="col-md-10 col-sm-10 col-xs-12">
																		 <input type="text" name="nombre" id="nombre" class="form-control" disabled>
																	   </div>
																	 </div>
																	 <div class="form-group">
 																	   <label class="control-label col-md-2 col-sm-3 col-xs-12">Empresa:</label>
 																	   <div class="col-md-10 col-sm-10 col-xs-12">
 																		 <input type="text" name="relacion" id="relacion" class="form-control" disabled>
 																	   </div>
 																	 </div>
																	 <div class="form-group">
 																	   <label class="control-label col-md-3 col-sm-3 col-xs-12">Responsable:</label>
 																	   <div class="col-md-9 col-sm-10 col-xs-12">
 																		 <input type="text" name="responsable" id="responsable" class="form-control" disabled>
 																	   </div>
 																	 </div>
																	 <div class="form-group">
 																	   <label class="control-label col-md-2 col-sm-3 col-xs-12">Cédula:</label>
 																	   <div class="col-md-10 col-sm-10 col-xs-12">
 																		 <input type="text" name="cedula" id="cedula" class="form-control" disabled>
 																	   </div>
 																	 </div>
																	 <div class="form-group">
 																	   <label class="control-label col-md-2 col-sm-3 col-xs-12">Telefóno:</label>
 																	   <div class="col-md-10 col-sm-10 col-xs-12">
 																		 <input type="text" name="telefono" id="telefono" class="form-control" disabled>
 																	   </div>
 																	 </div>
																</form>
										                  	</div>
									                	</div>
													</div>
						            			</div>
												<div class="row">
						              				<div class="col-md-12 col-xs-12">
						                				<div class="x_panel">
						                  					<div class="x_title">
						                    					<h2>Descripción</h2>
						                    					<div class="clearfix"></div>
						                  					</div>
						                  					<div class="x_content">
											                    <br />
																<form class="form-horizontal form-label-left">
																	<div class="col-md-4 col-sm-6 col-xs-12 form-group has-feedback">
																		<label>Superficie Total (Has):</label>
											                        	<input type="text" name="superficie_total" id="superficie_total" class="form-control" disabled>
											                      	</div>

																	<div class="col-md-4 col-sm-6 col-xs-12 form-group has-feedback">
																		<label>Superficie Utilizable (Has):</label>
											                        	<input type="text" name="superficie_utilizable" id="superficie_utilizable" class="form-control" disabled>
											                      	</div>

																	<div class="col-md-4 col-sm-6 col-xs-12 form-group has-feedback">
																		<label>Superficie en uso (Has):</label>
											                        	 <input type="text" name="totall" id="totall" class="form-control" disabled>
											                      	</div>

																	<div class="col-md-12 col-sm-6 col-xs-12 form-group has-feedback">
																		<label>Actividades:</label>
											                        	 <input type="text" name="actividades" id="actividades" class="form-control" disabled>
											                      	</div>

																	<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
																		<label>Tipo de Fuentes de Agua:</label>
											                        	 <input type="text" name="tipo_rec" id="tipo_rec" class="form-control" disabled>
											                      	</div>

																	<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
																		<label>Fuentes de Agua:</label>
											                        	 <input type="text" name="recurso" id="recurso" class="form-control" disabled>
											                      	</div>
																	
																	 <input type="hidden" name="opcion" id="operativo_agricola" value="si">
																	 <input type="hidden" name="opcion" id="operativo_pecuario" value="si">
																</form>
						                  					</div>
						                				</div>
						              				</div>
						            			</div>
							          		</div>
						        		</div>
						        	</div>
						        	<div class="modal-footer">
						          		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						        	</div>
						      	</div>
						    </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
