<!-- page content -->
<div class="right_col" role="main">
  <form action="registrar_usuario" method="POST" id="demo-form2">
    <div class="x_panel">
      <div class="x_title">
        <div>
          <h3>
            <?=$title?>
          </h3>
        </div>
      </div>
      <div class="x_content">
        <div class="form-group">
            <div class="row">
                <div class="col-md-offset-3 col-md-6 col-sm-12 col-xs12">
                <label>TIPO DE USUARIO</label>
                    <!--<input type="text" name="n_usuario" class="form-control" placeholder="INGRESAR USUARIO" onKeyUp="this.value=this.value.toUpperCase();">
                  -->
                  <select name="perfil_id" id="perfil_id" class="select2_single form-control">
                      <option value="0">SELECCIONE</option>
                      <?php foreach($tipo_usuarios as $tipo_usuario): ?>
                      <option value="<?=$tipo_usuario['perfil_id']?>">
                          <?=$tipo_usuario['descripcion']?>
                      </option>
                      <?php endforeach; ?>
                  </select>
                </div>
            </div>
          <div class="row">
            <div class="col-md-offset-3 col-md-6 col-sm-12 col-xs12">
              <label>USUARIO</label>
              <input type="text" name="n_usuario" class="form-control" placeholder="INGRESAR USUARIO" onKeyUp="this.value=this.value.toUpperCase();">
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-offset-3 col-md-3 col-sm-12 col-xs12">
              <label>CONTRASEÑA</label>
              <input type="text" name="n_clave" class="form-control" placeholder="REPITA CONTRASEÑA">
            </div>
            <div class="col-md-3 col-sm-12 col-xs12">
              <label>CONTRASEÑA</label>
              <input type="text" name="repetir_clave" class="form-control" placeholder="REPITA CONTRASEÑA">
            </div>
          </div>
          <br>
          <div class="row">
              <div class="col-md-offset-3 col-md-6 col-sm-12 col-xs12">
                  <label>CÉDULA</label>
                  <input type="text" name="n_cedula" id="n_cedula" placeholder="CÉDULA" class="form-control" onKeyPress="return valida(event,this,10,8)"
                   onBlur="valida2(this,10,8)">
              </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-offset-3 col-md-6 col-sm-12 col-xs12">
              <label>NOMBRE</label>
              <input type="text" name="nombre" id="nombre" placeholder="INGRESE NOMBRE" class="form-control" onKeyUp="this.value=this.value.toUpperCase();" onKeyPress="return valida(event,this,0,100)"
               onBlur="valida2(this,0,100)">
            </div>
          </div>
          <br>
          <div class="row">
              <div class="col-md-offset-3 col-md-6 col-sm-12 col-xs12">
                  <label>APELLIDO</label>
                  <input type="text" name="apellido" id="apellido" placeholder="INGRESE APELLIDO" class="form-control" onKeyUp="this.value=this.value.toUpperCase();" onKeyPress="return valida(event,this,0,100)"
                   onBlur="valida2(this,0,100)">
              </div>
          </div>
          <br>
          <div class="row">
              <div class="col-md-offset-3 col-md-6 col-sm-12 col-xs12">
                  <label>TELÉFONO</label>
                  <input type="text" name="n_celular" id="n_celular" placeholder="TELÉFONO" data-inputmask="'mask' : '09999999999'" class="form-control" onKeyPress="return valida(event,this,10,100)"
                   onBlur="valida2(this,10,100)">
              </div>
          </div>
          <br>
          <div class="row">
              <div class="col-md-offset-3 col-md-6 col-sm-12 col-xs12">
                  <label>CORREO</label>
                  <input type="email" name="n_correo" id="n_correo" placeholder="CORREO ELÉCTRONICO" class="form-control" onKeyPress="return valida(event,this,7,100)" onKeyUp="this.value=this.value.toUpperCase();"
                   onBlur="valida2(this,7,100)">
              </div>
          </div>
          <br>
        </div>
        <br>
        <div class="ln_solid"></div>
        <div class="form-group">
          <div class="col-md-offset-5 col-md-6">
            <button type="button" class="btn btn-primary">Cancelar</button>
            <button type="submit" id="" class="btn btn-success">Registrar</button>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
<!-- /page content -->
