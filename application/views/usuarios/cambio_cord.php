<div class="right_col" role="main">
	<form action="cambiar_cordinador" method="post" id="demo-form2">
		<div class="x_panel">
			<div class="x_title">
				<div>
					<h3><i class="fa fa-edit"></i><?=$title?></h3>
				</div>
			</div>
			<div class="x_content">
				<div class="form-group">
					<div class="row">
						<div class="col-md-6 col-sm-12 col-xs-12 form-group">
							<label>NOMBRE DE LA UPSA</label>
							<select class="form-control select2_single" name="">
								<option value="0">SELECCIONE</option>
								<?php foreach($nom_upsas as $tipo_actividad):?>
								<option value="<?=$tipo_actividad['0']?>">
									<?=$tipo_actividad['1']?>
								</option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
					<h4>RESPONSABLE ACTUAL</h4>
					<div class="row">
						<div class="col-md-4 col-sm-12 col-xs-12 form-group">
							<label>NOMBRE Y APELLIDO</label>
							<input type="text" name="cedula" id="cedula" placeholder="NOMBRE" class="form-control" onKeyPress="return valida(event,this,10,8)"
							 onBlur="valida2(this,10,8)" disabled>
						</div>
						<div class="col-md-4 col-sm-12 col-xs-12 form-group">
							<label>CÉDULA</label>
							<input type="text" name="cedula" id="cedula" placeholder="CEDULA" class="form-control" onKeyPress="return valida(event,this,10,8)"
							 onBlur="valida2(this,10,8)" disabled>
						</div>
						<div class="col-md-4 col-sm-12 col-xs-12 form-group">
							<label>CORREO</label>
							<input type="text" name="cedula" id="cedula" placeholder="CORREO" class="form-control" onKeyPress="return valida(event,this,10,8)"
							 onBlur="valida2(this,10,8)" disabled>
						</div>
					</div>
					<br>
					<h4>NUEVO RESPONSABLE</h4>
					<div class="row">
						<div class="col-md-4 col-sm-12 col-xs-12 form-group">
							<label>CÉDULA</label>
							<select class="form-control select2_single" name="">
								<option value="0">SELECCIONE</option>
								<?php foreach($cedula as $tipo_actividad):?>
								<option value="<?=$tipo_actividad['0']?>">
									<?=$tipo_actividad['1']?>
								</option>
								<?php endforeach; ?>
							</select>
						</div>
						<div class="col-md-4 col-sm-12 col-xs-12 form-group">
							<label>NOMBRE Y APELLIDO</label>
							<input type="text" name="cedula" id="cedula" placeholder="NOMBRE" class="form-control" onKeyPress="return valida(event,this,10,8)"
							 onBlur="valida2(this,10,8)" disabled>
						</div>
						<div class="col-md-4 col-sm-12 col-xs-12 form-group">
							<label>CORREO</label>
							<input type="text" name="cedula" id="cedula" placeholder="CORREO" class="form-control" onKeyPress="return valida(event,this,10,8)"
							 onBlur="valida2(this,10,8)" disabled>
						</div>
					</div>
					<div class="row">

						<div class="col-md-4 col-sm-12 col-xs-12 form-group">
							<label>TELEFÓNO</label>
							<input type="text" name="cedula" id="cedula" placeholder="CORREO" class="form-control" onKeyPress="return valida(event,this,10,8)"
							 onBlur="valida2(this,10,8)" disabled>
						</div>
						<div class="col-md-4 col-sm-12 col-xs-12 form-group">
							<label>CORREO</label>
							<input type="text" name="cedula" id="cedula" placeholder="CORREO" class="form-control" onKeyPress="return valida(event,this,10,8)"
							 onBlur="valida2(this,10,8)" disabled>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
