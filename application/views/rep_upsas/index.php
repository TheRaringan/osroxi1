<div class="right_col" role="main">
	<div class="">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>UPSAS REGISTRADAS Y SU RESPECTIVO COODINADOR</h2>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					<table id="datatable-buttons" class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>ESTADO</th>
								<th>NOMBRE UPSA</th>
								<th>CÉDULA</th>
								<th>NOMBRE Y APELLIDO</th>
								<th>TELEFONO</th>
								<th>CORREO</th>
							</tr>
						</thead>
						<tbody>
							<?php if($Listado != FALSE):?>
							<?php foreach ($Listado as $row) : ?>
					       		<tr>
									<td><?php
										if ($row[5]=='' ){
											echo "No ha cargado información";
										}else
											echo $row[5]?></td>
						            <td><?=$row[0]?></td>
						            <td><?=$row[1]?></td>
						            <td><?=$row[2]?></td>
						            <td><?=$row[3]?></td>
						            <td><?=$row[4]?></td>
				        		</tr>
				    		<?php endforeach; ?>
							<?php endif; ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
