<!-- page content -->
<div class="right_col" role="main">
  <form action="recursos_hidricos/insertar_recurso" method="POST" id="demo-form2">
    <div class="x_panel">
      <div class="x_title">
        <div>
          <h2>
            <?=$title?>
        </h2>
        </div>
        <div class="row text-right">
            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal">RECURSOS REGISTRADOS</button>
        </div>

      </div>
      <div class="x_content">
        <div class="form-group">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 form-group">
              <label>UPSA</label>
              <input name="id_upsa" type="text" id="id_upsa" class="form-control" value="<?=$this->session->userdata('nombre_upsa')?>" disabled>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-6">
              <label>TIPO RECURSO HíDRICO</label>
              <select id="s1" name="id_tipo_recurso_hidr" class="form-control select2_single">
                <option value="0">--SELECCIONE UNA OPCION</option>
                <?php foreach($recursos as $recurso): ?>
                <option value="<?=$recurso['id_tipo_recurso_hidr']?>">
                  <?=$recurso['nombre']?>
                </option>
                <?php endforeach;?>
              </select>
            </div>
            <div class="col-sm-6">
              <label>RECURSO HÍDRICO</label>
              <select id="s2" name="id_recurso_hidr" class="form-control select2_single">
                <option value="0">--SELECCIONE UNA OPCION--</option>
              </select>
            </div>
          </div>
        </div>
        <br>
        <div class="row text-center">
          <button type="button" id="myElement" class="btn btn-primary btn-circle waves-effect waves-circle waves-float ">
            <i class="fa fa-lg fa-plus"></i>
          </button>
        </div>
        <div class="ln_solid"></div>
        <table class="table table-hover table-bordered">
          <thead>
            <tr>
              <th>TIPO RECURSO</th>
              <th>RECURSO HÍDRICO</th>
              <th></th>
            </tr>
          </thead>
          <tbody id="myTable">

          </tbody>
        </table>
        <div class="ln_solid"></div>

        <div class="modal fade" id="myModal" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Recursos Hídricos Registrados</h4>
              </div>
              <div class="modal-body">
                <p>A continuación se muestran los Recursos Hídricos que ya han sido cargados por usted.</p>
                <p>
                    <label>UPSA</label>
                    <input name="id_upsa" type="text" id="id_upsa" class="form-control" value="<?=$this->session->userdata('nombre_upsa')?>" disabled>
                </p>
                <div class="table-responsive">
                    <table class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th>TIPO DE RECURSO HÍDRICO</th>
                                <th>RECURSO HÍDRICO</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if($listado_rec != FALSE):?>
                            <?php foreach ($listado_rec as $row) : ?>
                            <tr>
                                <td><?=$row[0]?></td>
                                <td><?=$row[1]?></td>
                            </tr>
                            <?php endforeach; ?>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
              </div>


              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>

          </div>
        </div>

        <div class="form-group">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="text-center">
              <button type="submit" id="mySubmit" class="btn btn-success">GUARDAR</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
<!-- /page content -->
