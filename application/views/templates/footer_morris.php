<footer>
  <div class="pull-right">
    DELAGRO Desarrollado por: Oficina de Tecnología de la Información y Comunicaciones

  </div>
  <div class="clearfix"></div>
</footer>
<!-- /footer content -->
</div>
</div>

<script>
  var base_url = "<?=base_url()?>";
</script>
<!-- jQuery -->
<script src="<?=base_url(); ?>plantilla/vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="<?=base_url(); ?>plantilla/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="<?=base_url(); ?>plantilla/vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="<?=base_url(); ?>plantilla/vendors/nprogress/nprogress.js"></script>
<!-- morris.js -->
<script src="<?=base_url(); ?>plantilla/vendors/raphael/raphael.min.js"></script>
<script src="<?=base_url(); ?>plantilla/vendors/morris.js/morris.min.js"></script>

<!-- Custom Theme Scripts -->
<script src="<?=base_url(); ?>plantilla/js/custom.js"></script>

<script src="<?=base_url(); ?>assets/js/morris.js"></script>
</body>
</html>