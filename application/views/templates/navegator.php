<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                  <a href="<?=base_url()?>principal" class="site_title"><i class="fa fa-share-alt-square"></i> <span>ECOPRO - UPSA </span></a>
                </div>

                <div class="clearfix"></div>
                <!-- /menu prile quick info -->
                <br />
                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <div class="menu_section">
                        <h3>Inicio</h3>
                        <ul class="nav side-menu">
                            <li><a href="<?=base_url()?>principal"><i class="fa fa-home"></i>PÁGINA PRINCIPAL</a></li>
                        </ul>
                    </div>
                    <?php if($this->session->userdata('perfil_id') == 333):?>
                        <div class="menu_section">
                            <h3>General</h3>
                            <ul class="nav side-menu">
                                <li><a><i class="fa fa-edit"></i>REG. DE UPSAS<span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">
                                        <li><a href="<?=base_url()?>basicos">DATOS BÁSICOS</a></li>
                                        <li><a href="<?=base_url()?>ubicacion">UBICACIÓN</a></li>
                                        <li><a href="<?=base_url()?>capacidad_vocacion">CAPACIDAD OPERATIVA</a></li>
                                        <li><a href="<?=base_url()?>Actividad_economica">ACTIVIDAD ECONÓMICA</a></li>
                                        <li><a href="<?=base_url()?>Recursos_hidricos">RECURSOS HÍDRICOS</a></li>
                                        <!--<li><a href="<?=base_url()?>Maquinaria">Registro Maquinaria</a></li>
                                        <li><a href="<?=base_url()?>Infraestructura">Infraestructuta de Apoyo a la Producción</a></li>-->
                                    </ul>
                                </li>
                               <!--
                                <li><a><i class="fa fa-edit"></i>REG. PECUARIO<span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">
                                        <li><a>BOVINOS<span class="fa fa-chevron-down"></span></a>
                                            <ul class="nav child_menu">
                                                <li class="sub_menu"><a href="<?=base_url()?>P_nacimientos">NACIMIENTOS</a></li>
                                                <li><a href="P_salidas">SALIDAS</a></li>
                                                <li><a href="P_pajuelas">COMPRA DE PAJUELAS</a></li>
                                                <li><a href="#level2_2">PRODUCCIÓN GENERAL</a></li>
                                                <li><a href="#level2_2">PRODUCCIÓN INDIVIDUAL</a></li>
                                                <li><a href="#level2_2">CAMBIO DE ESTATUS</a></li>
                                                <li><a href="#level2_2">INVENTARIO</a></li>
                                                <li><a href="#level2_2">OBSERVACIONES DE INSUMOS</a></li>
                                                <li><a href="#level2_2">REPRODUCCIÓN</a></li> 
                                            </ul>
                                        </li>
                                        <li><a href="#level1_2">PORCINOS<span class="fa fa-chevron-down"></span></a>
                                            <ul class="nav child_menu">
                                                <li class="sub_menu"><a href="level2.html">Level Two</a></li>
                                                <li><a href="#level2_1">Level Two</a></li>
                                                <li><a href="#level2_2">Level Two</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li> 
                                -->
                                <li><a><i class="fa fa-wrench"></i>HERRAMIENTAS DE APOYO</a>
                                  <ul class="nav child_menu">
                                      <li><a href="<?=base_url()?>calculador/index"><i class="fa fa-calculator"></i>CALCULADOR</a></li>
                                  </ul>
                                </li>
                                 <li><a href="<?=base_url()?>usuarios/cambio_contrasenia"><i class="fa fa-key"></i>CAMBIAR CONTRASEÑA</a></li>
                            </ul>
                        </div>

                    <?php elseif($this->session->userdata('perfil_id') >= 666):?>
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                        <div class="menu_section">
                            <h3>Administrador</h3>
                            <ul class="nav side-menu">
                              <!-- <li><a href="<?=base_url()?>dashboard/report"><i class="fa fa-gear"></i> Panel de Control </a></li> -->
                                <?php if($this->session->userdata('perfil_id') >= 666):?>
                                <li><a><i class="fa fa-hand-stop-o"></i>ALIANZAS</a>
                                    <ul class="nav child_menu">
                                        <li><a href="<?=base_url()?>registro_alianza"><i class="fa fa-edit"></i>REGISTRAR ALIANZA</a></li>
                                        <li><a href="<?=base_url()?>alianza"><i class="fa fa-list-alt"></i>FICHA DE ALIANZAS</a></li>
                                        <!--
                                        <li><a href="<?=base_url()?>mapa"><i class="fa fa-dot-circle-o"></i>MAPA 2</a></li>
                                        -->
                                    </ul>
                                </li>
                                <?php endif;?>
                                <?php if($this->session->userdata('perfil_id') == 777):?>
                                <li><a><i class="fa fa-map-marker"></i>REPORTES / MAPAS</a>
                                    <ul class="nav child_menu">
                                        <li><a href="<?=base_url()?>Dashboard"><i class="fa fa-dot-circle-o"></i>MAPA</a></li>
                                        <li><a href="<?=base_url()?>mapa"><i class="fa fa-dot-circle-o"></i>MAPA 2</a></li>
                                        <li><a href="<?=base_url()?>graficos"><i class="fa fa-dot-circle-o"></i>GRÁFICOS</a></li>
                                        <!--<li><a href="<?=base_url()?>rep_cuadros_upsas">Reporte de Operatividad</a></li>-->
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-institution"></i>REPORTES MINISTERIO</a>
                                    <ul class="nav child_menu">
                                        <li><a href="<?=base_url()?>rep_cuadros_upsas/sup_total"><i class="fa fa-area-chart"></i>SUPERFICIE TOTAL POR UPSA</a></li>
                                        <li><a href="<?=base_url()?>ficha_upsa"><i class="fa fa-users"></i>FICHA UPSAS</a></li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-bar-chart-o"></i>REPORTES ESPECIFICOS</a>
                                    <ul class="nav child_menu">
                                        <li><a href="<?=base_url()?>rep_upsas"><i class="fa fa-list"></i>UPSAS y CORDINADOR REGISTRADOS</a></li>
                                        <!--<li><a href="<?=base_url()?>rep_cuadros_upsas">Reporte de Operatividad</a></li>-->
                                        <li><a href="<?=base_url()?>rep_cuadros_upsas/rep_act_prod"><i class="fa fa-bar-chart"></i>RPT. DE ACT. ECONÓMICA</a></li>
                                        <li><a href="<?=base_url()?>rep_cuadros_upsas/rep_rec_hib"><i class="fa fa-bar-chart"></i>RPT. DE RECURSOS HÍDRICOS</a></li>
                                        <li><a href="<?=base_url()?>rep_cuadros_upsas/rep_voc_tie"><i class="fa fa-area-chart"></i>RPT. DE VOC. TIERRA</a></li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-line-chart"></i>REPORTES SEGUMIENTO DE CARGA</a>
                                    <ul class="nav child_menu">
                                        <li><a href="<?=base_url()?>rep_informacion_upsas"><i class="fa fa-list-ol"></i>RPT. DE USUARIOS REGISTRADOS</a></li>
                                        <li><a href="<?=base_url()?>rep_informacion_upsas/upsas_sin_inf"><i class="fa fa-outdent"></i>RPT. DE UPSAS SIN INFORMACIÓN</a></li>
                                        <li><a href="<?=base_url()?>rep_cuadros_upsas/upsas_sin_cargar"><i class="fa fa-list-alt"></i>INFORMACIÓN DE VENTANAS SIN CARGAR</a></li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-cogs"></i>HERRAMIENTAS DE APOYO</a>
                                    <ul class="nav child_menu">
                                        <li><a href="<?=base_url()?>calculador/index"><i class="fa fa-calculator"></i>CALCULADOR</a></li>
                                        <?php if($this->session->userdata('perfil_id') >= 777){?>
                                        <li><a href="<?=base_url()?>modificar_rubros/index"><i class="fa fa-pencil"></i>RUBROS</a></li>
                                        <li><a href="<?=base_url()?>modificar_insumos/index"><i class="fa fa-pencil"></i>INSUMOS</a></li>
                                        <li><a href="<?=base_url()?>modificar_insumos/general"><i class="fa fa-list"></i>INS. CONS. GENERAL</a></li>
                                        <?php }?>
                                    </ul>
                                </li>

                                <li><a><i class="fa fa-edit"></i>REG. PECUARIO<span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a>BOVINOS<span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu">
                                            <li class="sub_menu"><a href="<?=base_url()?>P_nacimientos">NACIMIENTOS</a></li>
                                            <li><a href="P_salidas">SALIDAS</a></li>
                                            <li><a href="P_pajuelas">COMPRA DE PAJUELAS</a></li>
                                            <!-- <li><a href="#level2_2">PRODUCCIÓN GENERAL</a></li>
                                            <li><a href="#level2_2">PRODUCCIÓN INDIVIDUAL</a></li>
                                            <li><a href="#level2_2">CAMBIO DE ESTATUS</a></li>
                                            <li><a href="#level2_2">INVENTARIO</a></li>
                                            <li><a href="#level2_2">OBSERVACIONES DE INSUMOS</a></li>
                                            <li><a href="#level2_2">REPRODUCCIÓN</a></li> -->
                                        </ul>
                                </ul>
                                </li>

                                <li><a><i class="fa fa-plus-square"></i>REGISTROS</a>
                                    <ul class="nav child_menu">
                                        <li><a href="<?=base_url()?>usuarios/registro"><i class="fa fa-user"></i>REG. NUEVOS USUARIOS</a></li>
                                        <li><a href="<?=base_url()?>usuarios/cambio_cord"><i class="fa fa-edit"></i>CAMBIO DE CORDINADOR</a></li>
                                    </ul>
                                </li>
                                <li><a href="<?=base_url()?>usuarios/cambio_contrasenia"><i class="fa fa-key"></i>CAMBIAR CONTRASEÑA</a></li>
                                <?php endif;?>
                            </ul>
                        </div>
                    </div>

                    <?php elseif($this->session->userdata('perfil_id') == 111):?>
                        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                            <div class="menu_section">
                                <h3>Administrador</h3>
                                <ul class="nav side-menu">
                                    <!-- <li><a href="<?=base_url()?>dashboard/report"><i class="fa fa-gear"></i> Panel de Control </a></li> -->
                                    <li><a><i class="fa fa-bar-chart-o"></i>REPORTES / MAPAS</a>
                                        <ul class="nav child_menu">
                                            <li><a href="<?=base_url()?>Dashboard">MAPA</a></li>
                                            <li><a href="<?=base_url()?>mapa">MAPA 2</a></li>
                                            <!--<li><a href="<?=base_url()?>rep_cuadros_upsas">Reporte de Operatividad</a></li>-->
                                        </ul>
                                    </li>
                                    <li><a><i class="fa fa-bar-chart-o"></i>REPORTES MINISTERIO</a>
                                        <ul class="nav child_menu">
                                            <li><a href="<?=base_url()?>rep_cuadros_upsas/sup_total">SUPERFICIE TOTAL POR UPSA</a></li>
                                        </ul>
                                    </li>
                                    <li><a><i class="fa fa-bar-chart-o"></i>REPORTES ESPECIFICOS</a>
                                        <ul class="nav child_menu">
                                            <li><a href="<?=base_url()?>rep_upsas">UPSAS y CORDINADOR REGISTRADOS</a></li>
                                            <!--<li><a href="<?=base_url()?>rep_cuadros_upsas">Reporte de Operatividad</a></li>-->
                                            <li><a href="<?=base_url()?>rep_cuadros_upsas/rep_act_prod">RPT. DE ACT. ECONÓMICA</a></li>
                                            <li><a href="<?=base_url()?>rep_cuadros_upsas/rep_rec_hib">RPT. DE RECURSOS HÍDRICOS</a></li>
                                            <li><a href="<?=base_url()?>rep_cuadros_upsas/rep_voc_tie">RPT. DE VOC. TIERRA</a></li>
                                        </ul>
                                    </li>
                                    <li><a><i class="fa fa-bar-chart-o"></i>REPORTES SEGUMIENTO DE CARGA</a>
                                        <ul class="nav child_menu">
                                            <li><a href="<?=base_url()?>rep_informacion_upsas">RPT. DE USUARIOS REGISTRADOS</a></li>
                                            <li><a href="<?=base_url()?>rep_informacion_upsas/upsas_sin_inf">RPT. DE UPSAS SIN INFORMACIÓN</a></li>
                                            <li><a href="<?=base_url()?>rep_cuadros_upsas/upsas_sin_cargar">INFORMACIÓN DE VENTANAS SIN CARGAR</a></li>
                                        </ul>
                                    </li>
                                    <li><a><i class="fa fa-wrench"></i>HERRAMIENTAS DE APOYO</a>
                                        <ul class="nav child_menu">
                                            <li><a href="<?=base_url()?>calculador/index"><i class="fa fa-calculator"></i>CALCULADOR</a></li>
                                        </ul>
                                    </li>
                                 
                                    <li><a><i class="fa fa-edit"></i>REG. PECUARIO<span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a>BOVINOS<span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu">
                                            <li class="sub_menu"><a href="<?=base_url()?>P_nacimientos">NACIMIENTOS</a></li>
                                            <li><a href="P_salidas">SALIDAS</a></li>
                                            <li><a href="P_pajuelas">COMPRA DE PAJUELAS</a></li>
                                            <!-- <li><a href="#level2_2">PRODUCCIÓN GENERAL</a></li>
                                            <li><a href="#level2_2">PRODUCCIÓN INDIVIDUAL</a></li>
                                            <li><a href="#level2_2">CAMBIO DE ESTATUS</a></li>
                                            <li><a href="#level2_2">INVENTARIO</a></li>
                                            <li><a href="#level2_2">OBSERVACIONES DE INSUMOS</a></li>
                                            <li><a href="#level2_2">REPRODUCCIÓN</a></li> -->
                                        </ul>
                                </ul>
                                </li>
                                    <!--<li><a href="<?=base_url()?>usuarios/registro"><i class="fa fa-plus-square"></i>Registrar Nuevos Usuarios</a></li>-->
                                    <li><a href="<?=base_url()?>usuarios/cambio_contrasenia"><i class="fa fa-key"></i>CAMBIAR CONTRASEÑA</a></li>
                                
                                </ul>
                            </div>
                        </div>
                    <?php endif;?>
                </div>
                <!-- /sidebar menu -->
                <!-- /menu footer buttons -->
                <div class="sidebar-footer hidden-small">
                  <!-- <a data-toggle="tooltip" data-placement="top" title="Configuracion" class="col-sm-1">
                    <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                  </a> -->
                  <a data-toggle="tooltip" data-placement="top" title="Cerrar Sesión" href="<?=base_url()?>usuarios/logout">
                    <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                  </a>
              </div>
            </div>
        </div>

        <div class="top_nav">
            <div class="nav_menu">
                <nav class="" role="navigation">
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>
                    <div class="">
                        <a href="#"><img align="right" src="<?php echo base_url(); ?>assets/images/corpo.png" style="height: 70px" alt=""></a>
                    </div>

                </nav>
            </div>
        </div>
