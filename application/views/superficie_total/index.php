<div class="right_col" role="main">
	<div class="">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>SUPERFICIE TOTAL POR UPSA</h2>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					<table id="datatable-buttons" class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>ESTADO</th>
								<th>NOMBRE UPSA</th>
								<th>RELACIÓN</th>
								<th>SUPERFICIE</th>
							</tr>
						</thead>
						<tbody>
							<?php if($listado_sup != FALSE):?>
							<?php foreach ($listado_sup as $row) : ?>
					       		<tr>
									<td><?php
										if ($row[0]=='' ){
											echo "No ha cargado modulo ubicación";
										}else
											echo $row[0]?></td>
						            <td><?=$row[1]?></td>
						            <td><?=$row[2]?></td>
						            <td><?php
										if ($row[3]=='' ){
											echo "No ha cargado superficie";
										}else
											echo $row[3]?></td>
				        		</tr>
				    		<?php endforeach; ?>
							<?php endif; ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
$(document).ready(function() {
	  $('#datatable-buttons').DataTable({
			"language": {
				"decimal": "",
				"emptyTable": "No hay información",
				"info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
				"infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
				"infoFiltered": "(Filtrado de _MAX_ total entradas)",
				"infoPostFix": "",
				"thousands": ",",
				"lengthMenu": "Mostrar _MENU_ Entradas",
				"loadingRecords": "Cargando...",
				"processing": "Procesando...",
				"search": "Buscar:",
				"zeroRecords": "Sin resultados encontrados",
				"paginate": {
					"first": "Primero",
					"last": "Ultimo",
					"next": "Siguiente",
					"previous": "Anterior"
				}
			},
	  });
	});
</script>
