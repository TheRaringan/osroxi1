<div class="main_container">
    <div class="right_col" role="main">
        <form action="modificar_rubro" method="post" id="demo-form2" autocomplete="off">
            <div class="x_panel">
                <div class="x_title">
                    <div>
                        <h3><i class="fa fa-edit"></i> Modificar rubro - <?=$rubro['nombre_rubro']?> </h3>
                    </div>
                </div>
                <div class="x_content">
                    <div class="form-group">
                        <h4>INFORMACIÓN DEL RUBRO</h4>
                        <div class="row">
                            <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                <label>NOMBRE</label>
                                <input type="text" name="nombre" id="nombre" value="<?=$rubro['nombre_rubro']?>" class="form-control" onKeyUp="this.value=this.value.toUpperCase();" required >
                            </div>

                            <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                <label>RENDIMIENTO</label>
                                <input type="text" name="rendimiento" id="rendimiento" value="<?=$rubro['rendimiento']?>" class="form-control" onKeyPress="return valida(event,this,10,8)"
                                onBlur="valida2(this,10,8)" required >
                            </div>

                            <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                <label>DENSIDAD</label>
                                <input type="text" name="densidad" id="densidad" value="<?=$rubro['densidad']?>" class="form-control" onKeyPress="return valida(event,this,10,8)"
                                onBlur="valida2(this,10,8)" required >
                            </div>

                            <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                <label>CICLO</label>
                                <input type="text" name="ciclo" id="ciclo" value="<?=$rubro['ciclo']?>" onKeyUp="this.value=this.value.toUpperCase();"  class="form-control" required>
                            </div>
                            <input type="hidden" name="id" id="id" value="<?=$rubro['id_rubro']?>" class="form-control">
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <div class="ln_solid"></div>
            <div class="form-group">
                <div class="col-md-offset-5 col-md-6">
                    <button type="button" class="btn btn-primary" onclick="location.href = '../modificar_rubros/index'";>Cancelar</button>
                    <button type="submit" id="" class="btn btn-success">Registrar</button>
                </div>
            </div>
        </form>
    </div>
</div>