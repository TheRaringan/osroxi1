<div class="main_container">
	<div class="right_col" role="main">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
				<div class="col-12">
				<a class="btn btn-dark pull-right my-2 mx-2" href="agregar_rubro"><i class="ft-list"></i> AGREGAR RUBRO</a>
			</div>
					<div class="x_title">
						<h2>CONSULTA GENERAL DE LOS RUBROS</h2>
						<div class="clearfix">

						</div>
					</div>
					<div class="x_content">
						<table id="datatable-buttons" data-page-length='7' class="table table-striped table-bordered" style="width: 100%; text-align: center;">
							<thead>
								<tr>
									<th>NOMBRE</th>
									<th>RENDIMIENTO</th>
									<th>DENSIDAD</th>
									<th>CICLO</th>
									<th>ESTADO</th>
									<th>ACCIÓN</th>
								</tr>
							</thead>
							<tbody>
							<?php foreach($rubros as $rubros): ?>
								<tr>
									<td style="vertical-align: middle"><?=$rubros['nombre_rubro']?></td>
									<td style="vertical-align: middle"><?=$rubros['rendimiento']?></td>
									<td style="vertical-align: middle"><?=$rubros['densidad']?></td>
									<td style="vertical-align: middle"><?=$rubros['ciclo']?></td>
									<td style="vertical-align: middle"><?php if ($rubros['estatus'] == 1)
										echo("Activo");
										else
											echo("Inactivo"); ?>			
									</td>
									<td style="vertical-align: middle">
										<a class="btn btn-dark btn-round" href="<?php echo base_url();?>index.php/modificar_rubros/modificar?id=<?php echo $rubros['id_rubro'];?>" title="Modificar"><i class="fa fa-pencil" ></i></a>
										<?php
											if($rubros['estatus'] == 1){
										?>
											<a class="btn btn-danger btn-round" href="<?php echo base_url();?>index.php/modificar_rubros/estatus?id=<?php echo $rubros['id_rubro'];?>&estatus=0" title="Desactivar"><i class="fa fa-close"></i></a>
										<?php
											}else{
										?>
											<a class="btn btn-success btn-round" href="<?php echo base_url();?>index.php/modificar_rubros/estatus?id=<?php echo $rubros['id_rubro'];?>&estatus=1" title="Activar"><i class="fa fa-check"></i></a>
										<?php
											}
										?>
									</td>
								</tr>
							<?php endforeach; ?> 
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>