<div class="right_col" role="main">
	<div class="">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>Registro de Alianzas</h2>
						<div class="clearfix"></div>
					</div>
					<div class="row">
						<div role="main">
			          		<form action="<?=base_url()?>index.php/registro_alianza/registro_alianza" method="POST" autocomplete="off">
			            		<div class="clearfix"></div>
		            			<div class="row">
					              	<div class="col-md-6 col-xs-12">
					                	<div class="x_panel">
						                  	<div class="x_title">
						                    	<h2>Datos de la Alianza</h2>
						                    	<div class="clearfix"></div>
						                  	</div>
						                  	<div class="x_content">
						                    	<br />
												<div class="form-horizontal form-label-left">
													<div class="form-group">
														<label class="control-label col-md-3 col-sm-3 col-xs-12">Nombre UPSA:</label>
														<div class="col-md-9 col-sm-10 col-xs-12">
										                	<select name="id_upsa" id="id_upsa" class="select2_single form-control" required>
																<option value=''>-- Seleccione --</option>
																<?php foreach($upsas as $nombre_upsa): ?>
																	<option value="<?=$nombre_upsa['id_upsa']?>">
																		<?=$nombre_upsa['nombre']?>
																	</option>
																<?php endforeach; ?>
															</select>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-3 col-sm-3 col-xs-12">Nombre Alianza:</label>
														<div class="col-md-9 col-sm-10 col-xs-12">
															<input type="text" name="nombre_alianza" id="nombre_alianza" class="form-control" onKeyUp="this.value=this.value.toUpperCase();" required>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-3 col-sm-3 col-xs-12">Responsable:</label>
														<div class="col-md-9 col-sm-10 col-xs-12">
															<input type="text" name="responsable" id="responsable" class="form-control" onKeyUp="this.value=this.value.toUpperCase();" required>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-3 col-sm-3 col-xs-12">RIF:</label>
														<div class="col-md-9 col-sm-10 col-xs-12">
															<input type="text" name="rif" id="rif" class="form-control" onKeyUp="this.value=this.value.toUpperCase();" required>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-3 col-sm-3 col-xs-12">Telefóno:</label>
														<div class="col-md-9 col-sm-10 col-xs-12">
															<input type="text" name="telefono" id="telefono" class="form-control" onKeyUp="this.value=this.value.toUpperCase();" required>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-3 col-sm-3 col-xs-12">Dirección:</label>
														<div class="col-md-9 col-sm-10 col-xs-12">
															<input type="text" name="direccion" id="direccion" class="form-control" onKeyUp="this.value=this.value.toUpperCase();" required>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-3 col-sm-3 col-xs-12">Correo Electronico:</label>
														<div class="col-md-9 col-sm-10 col-xs-12">
															<input type="email" name="correo" id="correo" class="form-control" onKeyUp="this.value=this.value.toUpperCase();" required>
														</div>
													</div>
												</div>
						                  	</div>
					                	</div>
									</div>
		              				<div class="col-md-6 col-xs-12">
		                				<div class="x_panel">
		                  					<div class="x_title">
		                    					<h2>Ubicación</h2>
		                    					<div class="clearfix"></div>
		                  					</div>
		                  					<div class="x_content">
							                    <br />
												<div class="form-horizontal form-label-left">
													<div class="form-group">
														<label class="control-label col-md-3 col-sm-3 col-xs-12">Estado:</label>
														<div class="col-md-19 col-sm-9 col-xs-12">
															<select name ="id_estado" id ="id_estado" class="select2_single form-control" required>
																<option value=''>-- Seleccione --</option>
																<?php foreach ($estados as $estado) : ?>
																	<option value='<?=$estado["id_estado"]?>'> <?=$estado["descripcion"]?> </option>
																<?php endforeach; ?>
															</select>
														</div>
													</div>
												 	<div class="form-group">
														<label class="control-label col-md-3 col-sm-3 col-xs-12">Municipio:</label>
														<div class="col-md-19 col-sm-9 col-xs-12">
															<select class="select2_single form-control" id="id_municipio" name="id_municipio" required>
																<option value="">-- Seleccione --</option>
															</select>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-3 col-sm-3 col-xs-12">Parroquia:</label>
														<div class="col-md-9 col-sm-9 col-xs-12">
															<select class="select2_single form-control" id="id_parroquia" name="id_parroquia" required>
																<option value="">-- Seleccione --</option>
															</select>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-3 col-sm-3 col-xs-12">Latitud:</label>
														<div class="col-md-9 col-sm-9 col-xs-12">
															<input type="number" name="latitud" id="latitud" class="form-control" required>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-3 col-sm-3 col-xs-12">Longitud:</label> 
														<div class="col-md-9 col-sm-9 col-xs-12">
															<input type="number" name="longitud" id="longitud" class="form-control" required>
														</div>
													</div>
												</div>
		                  					</div>
		                				</div>
		              				</div>
		            			</div>
								<div class="row">
		              				<div class="col-md-12 col-xs-12">
		                				<div class="x_panel">
		                  					<div class="x_title">
		                    					<h2>Aspectos Legales</h2>
		                    					<div class="clearfix"></div>
		                  					</div>
		                  					<div class="x_content">
							                    <br />
												<div class="form-horizontal form-label-left">
													<div class="col-md-4 col-sm-6 col-xs-12 form-group has-feedback">
														<label>Objeto de la Alianza</label>
							                        	<input type="text" name="objeto" id="objeto" class="form-control" onKeyUp="this.value=this.value.toUpperCase();" required>
							                      	</div>

													<div class="col-md-4 col-sm-6 col-xs-12 form-group has-feedback">
														<label>Aportes de las Partes</label>
							                        	<input type="text" name="aportes" id="aportes" class="form-control" onKeyUp="this.value=this.value.toUpperCase();" required>
							                      	</div>

													<div class="col-md-4 col-sm-6 col-xs-12 form-group has-feedback">
														<label>Fecha de Suscripción</label>
							                        	<input type="date" name="fecha_suscripcion" id="fecha_suscripcion" class="form-control" required >
							                      	</div>

													<div class="col-md-4 col-sm-6 col-xs-12 form-group has-feedback">
														<label>Duración</label>
							                        	<input type="text" name="duracion" id="duracion" class="form-control" onKeyUp="this.value=this.value.toUpperCase();" required >
							                      	</div>

													<div class="col-md-4 col-sm-6 col-xs-12 form-group has-feedback">
														<label>Distribución de Utilidad:</label>
							                        	 <input type="text" name="utilidad" id="utilidad" class="form-control" onKeyUp="this.value=this.value.toUpperCase();" required >
							                      	</div>

													<div class="col-md-4 col-sm-6 col-xs-12 form-group has-feedback">
														<label>Distribución de Producción:</label>
							                        	 <input type="text" name="produccion" id="produccion" class="form-control" onKeyUp="this.value=this.value.toUpperCase();" required >
							                      	</div>

													<div class="col-md-4 col-sm-6 col-xs-12 form-group has-feedback">
														<label>Bienes Asignados:</label><br>
														<select name ="bienes_asignados[]" id ="bienes_asignados" class="select2_single form-control" required multiple>
															<option value=''>-- Seleccione --</option>
															<option value='LOGISTICA'>LOGISTICA</option>
															<option value='MAQUINARIA'>MAQUINARIA</option>
															<option value='UNIDAD DE PRODUCCIÓN'>UNIDAD DE PRODUCCIÓN</option>
														</select>
							                      	</div>
												</div>
		                  					</div>
		                				</div>
		              				</div>
		            			</div>
			            		<div class="clearfix"></div>
		            			<div class="row">
		              				<div class="col-md-6 col-xs-12">
		                				<div class="x_panel">
		                  					<div class="x_title">
		                    					<h2>Aspectos de Producción</h2>
		                    					<div class="clearfix"></div>
		                  					</div>
		                  					<div class="x_content">
							                    <br />
												<div class="form-horizontal form-label-left">
													<div class="form-group">
													   <label class="control-label col-md-3 col-sm-3 col-xs-12">Proyecto Presentado:</label>
													   	<div class="col-md-19 col-sm-9 col-xs-12">
															<textarea name="proyecto" id="proyecto" class="form-control" onKeyUp="this.value=this.value.toUpperCase();" required></textarea>
														</div>
													</div>
												 	<div class="form-group">
														<label class="control-label col-md-3 col-sm-3 col-xs-12">Fecha de Siembra:</label>
														<div class="col-md-19 col-sm-9 col-xs-12">
															<input type="date" name="fecha_siembra" id="fecha_siembra" class="form-control" required>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-3 col-sm-3 col-xs-12">Tiempo Estimado de Cosecha:</label>
														<div class="col-md-9 col-sm-9 col-xs-12">
															<input type="text" name="tiempo_cosecha" id="tiempo_cosecha" class="form-control" onKeyUp="this.value=this.value.toUpperCase();" required >
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-3 col-sm-3 col-xs-12">Hectareas Sembradas: (Has)</label>
														<div class="col-md-9 col-sm-9 col-xs-12">
															<input type="number" name="has_sembradas" id="has_sembradas" class="form-control" required>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-3 col-sm-3 col-xs-12">Hectareas Cosechadas: (Has)</label>
														<div class="col-md-9 col-sm-9 col-xs-12">
															<input type="number" name="has_cosechadas" id="has_cosechadas" class="form-control" required>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-3 col-sm-3 col-xs-12">Fecha de Cosecha:</label>
														<div class="col-md-9 col-sm-9 col-xs-12">
															<input type="date" name="fecha_cosecha" id="fecha_cosecha" class="form-control"  reuired>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-3 col-sm-3 col-xs-12">Peso a Liquidar: (Kg)</label>
														<div class="col-md-9 col-sm-9 col-xs-12">
															<input type="number" name="peso_liquidar" id="peso_liquidar" class="form-control" required>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-3 col-sm-3 col-xs-12">Silo: </label>
														<div class="col-md-9 col-sm-9 col-xs-12">
															<input type="text" name="silo" id="silo" class="form-control" onKeyUp="this.value=this.value.toUpperCase();" required >
														</div>
													</div>
												</div>
		                  					</div>
		                				</div>
		              				</div>

					              	<div class="col-md-6 col-xs-12">
					                	<div class="x_panel">
						                  	<div class="x_title">
						                    	<h2>Ficha de Control y Seguimiento</h2>
						                    	<div class="clearfix"></div>
						                  	</div>
						                  	<div class="x_content">
						                    	<br />
												<div class="form-horizontal form-label-left">
													<div class="form-group">
														<label class="control-label col-md-3 col-sm-3 col-xs-12">Nombre y Apellido:</label>
														<div class="col-md-9 col-sm-10 col-xs-12">
															<input type="text" name="nombre_contacto" id="nombre_contacto" class="form-control" onKeyUp="this.value=this.value.toUpperCase();" required >
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-3 col-sm-3 col-xs-12">Cédula:</label>
														<div class="col-md-9 col-sm-10 col-xs-12">
															<input type="text" name="cedula_contacto" id="cedula_contacto" class="form-control" onKeyUp="this.value=this.value.toUpperCase();" required >
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-3 col-sm-3 col-xs-12">Teléfono:</label>
														<div class="col-md-9 col-sm-10 col-xs-12">
															<input type="text" name="telefono_contacto" id="telefono_contacto" class="form-control" onKeyUp="this.value=this.value.toUpperCase();" required >
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-3 col-sm-3 col-xs-12">Fecha y Hora:</label>
														<div class="col-md-9 col-sm-10 col-xs-12">
															<input type="datetime" name="fecha_contacto" id="fecha_contacto" class="form-control" style="background-color: transparent;" value="<?=date('Y-m-d H:i:s', (strtotime ("-1 Hours")));?>" disabled>
														</div>
													</div>
													<div class="form-group">
													   <label class="control-label col-md-3 col-sm-3 col-xs-12">Información Suministrada:</label>
													   	<div class="col-md-19 col-sm-9 col-xs-12">
															<textarea name="informacion" id="informacion" rows="11" class="form-control" onKeyUp="this.value=this.value.toUpperCase();" required></textarea>
														</div>
													</div>
												</div>
						                  	</div>
					                	</div>
									</div>
		            			</div>
								<div class="row">
									<div class="col-md-5 col-md-offset-5">
										<button type="submit" class="btn btn-primary btn-lg btn-success">Guardar</button>
										<input type="reset" name="reset" class="btn btn-primary btn-lg btn-warning" value="Limpiar">
									</div>
								</div>
			          		</form>
		        		</div>
				    </div>
				</div>
			</div>
		</div>
	</div>
</div>
