<!-- formulario para guardar Datos básico de la UPSA -->
<div class="right_col" role="main">
<?= form_open('basicos/procesardatos', array('name' => 'formulario', 'id' => 'formulario_basico'))?>
	<div class="">
		<!--//boton buscar-->
		<div class="clearfix"></div>
		<div class="row">
			<div class="x_panel">
				<div class="x_title">
					<h2>
						SALIDAS DE BOVINOS
					</h2>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					<div class="col-md-4 col-sm-12 col-xs-12 form-group">
						<label>FECHA DE SUCESO</label>
						 <input type="date" name="f_suceso" id="f_suceso" class="form-control">
					</div>
					<div class="col-md-4 col-sm-12 col-xs-12 form-group">
						<label>NÚMERO DEL ANIMAL</label>
						<input type="text" name="n_animal" id="n_animal" placeholder="NÚMERO DEL ANIMAL" class="form-control" onKeyUp="this.value=this.value.toUpperCase();"
						 onBlur="valida2(this,2,100)">
					</div>

					<div class="col-md-4 col-sm-12 col-xs-12 form-group">
						<label>TIPO DE SALIDA</label>
						<select name="tipo_salida" id="tipo_salida" class="select2_single form-control" tabindex="-1">
							<option value='0'>ROBO</option>
							<option value="1">MUERTE</option>
							<option value="2">VENTA</option>
							<option value="3">TRASLADO</option>
						</select>
					</div>
				</div>
				<div class="x_content">
					<div class="col-md-6 col-sm-12 col-xs-12 form-group">
						<label>UPSA RECEPTORA</label>
						<select name="id_raza" id="id_raza" class="select2_single form-control" tabindex="-1">
							<option value='0'>DOMADOR PINEDA</option>
							<option value="1">USERAS</option>
						</select>
					</div>
					<div class="col-md-6 col-sm-12 col-xs-12 form-group">
						<label>TIPOLOGÍA</label>
						<input type="text" name="tipologia" id="tipologia" placeholder="TIPOLOGÍA" class="form-control"
						 onKeyPress="return valida(event,this,0,100)" onBlur="valida2(this,0,100)">
					</div>
				</div>
				<div class="x_content">
					<div class="col-md-12 col-sm-12 col-xs-12 form-group">
						<label>OBSERVACIONES</label>
						<input type="text" name="observaciones" id="observaciones" placeholder="OBSERVACIONES" class="form-control"
						 onKeyPress="return valida(event,this,0,20)" onBlur="valida2(this,0,20)">
					</div>
				</div>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12">
				<br>
				<div class="text-center">
					<button id="basicoSubmit" class="btn btn-dark" value="Guardar" name="boton" disabled>GUARDAR</button>
				</div>
			</div>
		</div>
		<!-- /formulario para guardar persona -->
	</div>
		<?= form_close()?>
</div>
