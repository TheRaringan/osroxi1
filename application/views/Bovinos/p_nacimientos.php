<!-- formulario para guardar Datos básico de la UPSA -->
<div class="right_col" role="main">
<?= form_open('basicos/procesardatos', array('name' => 'formulario', 'id' => 'formulario_basico'))?>
	<div class="">
		<!--//boton buscar-->
		<div class="clearfix"></div>
		<div class="row">
			<div class="x_panel">
				<div class="x_title">
					<h2>
						DATOS DEL SEMOVIENTE
					</h2>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					<div class="col-md-4 col-sm-12 col-xs-12 form-group">
						<label>NÚMERO DEL ANIMAL</label>
						<input type="text" name="n_animal" id="n_animal" placeholder="NÚMERO DEL ANIMAL" class="form-control" onKeyUp="this.value=this.value.toUpperCase();"
						 onBlur="valida2(this,2,100)">
					</div>
					<div class="col-md-4 col-sm-12 col-xs-12 form-group">
						<label>FECHA DE NACIMIENTO</label>
						 <input type="date" name="f_nacimiento" id="f_nacimiento" class="form-control">
					</div>
					<div class="col-md-4 col-sm-12 col-xs-12 form-group">
						<label>EDAD</label>
						<input type="text" name="edad" id="edad" placeholder="EDAD" class="form-control"
						 onKeyPress="return valida(event,this,10,100)" onBlur="valida2(this,10,100)">
					</div>
				</div>
				<div class="x_content">
					<div class="col-md-4 col-sm-12 col-xs-12 form-group">
						<label>RAZA</label>
						<select name="id_raza" id="id_raza" class="select2_single form-control" tabindex="-1">
							<option value='0'>RAZA</option>
							<option value="1">CARORA</option>
							<option value="2">HOLSTEIN</option>>
						</select>
					</div>
					<div class="col-md-4 col-sm-12 col-xs-12 form-group">
						<label>SEXO</label>
						<select name="id_sexo" id="id_sexo" class="select2_single form-control" tabindex="-1">
							<option value='0'>SEXO</option>
							<option value="1">FEMENINO</option>
							<option value="2">MASCULINO</option>
						</select>
					</div>
					<div class="col-md-4 col-sm-12 col-xs-12 form-group">
						<label>COLOR</label>
						<select name="id_color" id="id_color" class="select2_single form-control" tabindex="-1">
							<option value='0'>COLOR</option>
							<option value="1">BLANCO</option>
							<option value="2">NEGRO</option>
						</select>
					</div>
				</div>
				<div class="x_content">
					<div class="col-md-4 col-sm-12 col-xs-12 form-group">
						<label>MADRE</label>
						<input type="text" name="madre" id="madre" placeholder="MADRE" class="form-control" onKeyUp="this.value=this.value.toUpperCase();"
						 onBlur="valida2(this,0,100)">
					</div>
					<div class="col-md-4 col-sm-12 col-xs-12 form-group">
						<label>PADRE</label>
						<input type="text" name="padre" id="padre" placeholder="PADRE" class="form-control"
						 onKeyPress="return valida(event,this,0,20)" onBlur="valida2(this,0,20)">
					</div>
					<div class="col-md-4 col-sm-12 col-xs-12 form-group">
						<label>TIPOLOGÍA</label>
						<input type="text" name="tipologia" id="tipologia" placeholder="TIPOLOGÍA" class="form-control"
						 onKeyPress="return valida(event,this,0,100)" onBlur="valida2(this,0,100)">
					</div>
				</div>
				<div class="x_content">
					<div class="col-md-4 col-sm-12 col-xs-12 form-group">
						<label>PESO AL NACER</label>
						<input type="text" name="p_nacer" id="p_nacer" placeholder="PESO AL NACER" class="form-control" onKeyUp="this.value=this.value.toUpperCase();"
						 onBlur="valida2(this,2,100)">
					</div>
					<div class="col-md-8 col-sm-12 col-xs-12 form-group">
						<label>OBSERVACIONES</label>
						<input type="text" name="observaciones" id="observaciones" placeholder="OBSERVACIONES" class="form-control"
						 onKeyPress="return valida(event,this,0,20)" onBlur="valida2(this,0,20)">
					</div>
				</div>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12">
				<br>
				<div class="text-center">
					<button id="basicoSubmit" class="btn btn-dark" value="Guardar" name="boton" disabled>GUARDAR</button>
				</div>
			</div>
		</div>
		<!-- /formulario para guardar persona -->
	</div>
		<?= form_close()?>
</div>
