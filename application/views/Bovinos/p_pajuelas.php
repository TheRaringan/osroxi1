<!-- formulario para guardar Datos básico de la UPSA -->
<div class="right_col" role="main">
<?= form_open('basicos/procesardatos', array('name' => 'formulario', 'id' => 'formulario_basico'))?>
	<div class="">
		<!--//boton buscar-->
		<div class="clearfix"></div>
		<div class="row">
			<div class="x_panel">
				<div class="x_title">
					<h2>
						DATOS DE LAS PAJUELAS
					</h2>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					<div class="col-md-4 col-sm-12 col-xs-12 form-group">
						<label>NÚMERO DE PAJUELA (LOTE)L</label>
						<input type="text" name="n_pajuela" id="n_pajuela" placeholder="NÚMERO DE PAJUELA" class="form-control" onKeyUp="this.value=this.value.toUpperCase();"
						 onBlur="valida2(this,2,100)">
					</div>

					<div class="col-md-4 col-sm-12 col-xs-12 form-group">
						<label>NOMBRE DE LA PAJUELA</label>
						<input type="text" name="nombre_pajuela" id="nombre_pajuela" placeholder="NOMBRE DE LA PAJUELA" class="form-control"
						 onKeyPress="return valida(event,this,0,100)" onBlur="valida2(this,0,100)">
					</div>
					<div class="col-md-4 col-sm-12 col-xs-12 form-group">
						<label>ORIGEN</label>
						<input type="text" name="origen" id="origen" placeholder="ORIGEN" class="form-control" onKeyUp="this.value=this.value.toUpperCase();"
						 onBlur="valida2(this,0,100)">
					</div>
				</div>
				<div class="x_content">
					<div class="col-md-4 col-sm-12 col-xs-12 form-group">
						<label>CANTIDAD</label>
						<input type="text" name="cantidad" id="cantidad" placeholder="CANTIDAD" class="form-control" onKeyUp="this.value=this.value.toUpperCase();"
						 onBlur="valida2(this,2,100)">
					</div>
					<div class="col-md-4 col-sm-12 col-xs-12 form-group">
						<label>RAZA</label>
						<select name="id_raza" id="id_raza" class="select2_single form-control" tabindex="-1">
							<option value='0'>RAZA</option>
							<option value="1">CARORA</option>
							<option value="2">HOLSTEIN</option>
						</select>
					</div>
					<div class="col-md-4 col-sm-12 col-xs-12 form-group">
						<label>SEXADA</label>
						<p>
                        	MASCULINO: <input type="radio" class="flat" name="gender" id="genderM" value="M" checked="" required />
							FEMENINO: <input type="radio" class="flat" name="gender" id="genderF" value="F" />
                      	</p>
					</div>
				</div>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12">
				<br>
				<div class="text-center">
					<button id="basicoSubmit" class="btn btn-dark" value="Guardar" name="boton" disabled>GUARDAR</button>
				</div>
			</div>
		</div>
		<!-- /formulario para guardar persona -->
	</div>
		<?= form_close()?>
</div>
