<div class="right_col" role="main">
	<div class="clearfix"></div>
	<div class="row">
		<div class="col-md-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>
						<?=$titulo?>
					</h2>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					<form class="form-horizontal form-label-left">
						<div class="row">
							<div class="col-md-12 col-sm-12 col-xs-12 form-group">
								<label>UPSA</label>
								<input name="id_upsa" type="text" id="id_upsa" class="form-control" value="<?=$this->session->userdata('nombre_upsa')?>"
								  disabled>
							</div>
						</div>
						<!---->
						<div class="col-md-4 col-sm-12 col-xs-12 form-group">
							<label>Tipo</label>
							<select name="id_operatividad" id="id_operatividad" class="select2_single form-control">
								<option value="0">SELECCIONE</option>
								<option value="1">Corrales</option>
								<option value="2">Bebederos</option>
								<option value="3">Mangas</option>
							</select>
						</div>
						<!---->
						<div class="col-md-4 col-sm-12 col-xs-12 form-group">
							<label>Cantidad</label>
							<input name="cantidad" type="text" id="cantidad" placeholder="Cantidad" class="form-control" onKeyPress="return valida(event,this,2,100)"
							  onBlur="valida2(this,2,100)">
						</div>
						<!---->
						<div class="col-md-4 col-sm-12 col-xs-12 form-group">
							<label>Operativo:</label>
							 <p>
							   Si:
							   <input type="radio" class="flat" name="gender" id="genderM" value="1" checked="" required />
							   No:
							   <input type="radio" class="flat" name="gender" id="genderF" value="2" />
							 </p>
                        </div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
