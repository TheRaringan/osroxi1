<div class="right_col" role="main">
	<div class="">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>REPORTE DE UPSAS SIN INFORMACIÓN CARGADA</h2>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					<table id="datatable-buttons" class="table table-striped table-bordered">
						<thead>
							<tr>
								<th class="text-center">CORDINADOR</th>
								<th class="text-center">UPSA</th>
								<th class="text-center">UBICACIÓN</th>
								<th class="text-center">CAP. OPERATIVA</th>
								<th class="text-center">ACT. ECONÓMICA</th>
								<th class="text-center">RECURSOS HÍDRICOS</th>
							</tr>
						</thead>
						<tbody>
							<?php if($listado1 != FALSE)  :?>
							<?php foreach ($listado1 as $row) : ?>
							<tr>
							<td class="text-center"><?=$row[1]?></td>
							<td class="text-center"><?=$row[2]?></td>
							<td class="text-center"><?php
								if ($row[3]=='0' ){
									echo "<b><font color=\"red\">SIN CARGAR</font></b>";;
								}else
									echo "<font color=\"green\">CARGADA</font>"?></td>
							<td class="text-center"><?php
								if ($row[4]=='0' ){
									echo "<b><font color=\"red\">SIN CARGAR</font></b>";;
								}else
									echo "<font color=\"green\">CARGADA</font>"?></td>
							<td class="text-center"><?php
								if ($row[5]=='0' ){
									echo "<b><font color=\"red\">SIN CARGAR</font></b>";;
								}else
									echo "<font color=\"green\">CARGADA</font>"?></td>
							<td class="text-center"><?php
								if ($row[6]=='0' ){
									echo "<b><font color=\"red\">SIN CARGAR</font></b>";;
								}else
									echo "<font color=\"green\">CARGADA</font>"?></td>
							</tr>
						<?php endforeach; ?>
						<?php endif; ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
