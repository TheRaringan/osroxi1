<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Personas_model
 *
 * @author Nelly Moreno & Albert Torres
 */
class Rep_upsas_model extends CI_Model{

	public function listar_upsas(){
		$consulta_sql= "SELECT upsa.nombre as nombre_upsa,
						       cor.cedula,
						       CONCAT(cor.nombre,' ',cor.apellido) as coordinador,
						       cor.tlf_movil,
						       cor.email,
						       est.descripcion as estado
						FROM UPSA
						LEFT JOIN coordinador as cor ON cor.id_upsa = upsa.id_upsa
						LEFT JOIN ubicacion as ubi ON ubi.id_upsa = upsa.id_upsa
						LEFT JOIN estado as est ON est.id_estado = ubi.id_estado";

		$query = $this->db->query($consulta_sql);

		if($query->result() != NULL){
			foreach ($query->result() as $option){
				$data[] = array($option->nombre_upsa,
								$option->cedula,
								$option->coordinador,
								$option->tlf_movil,
								$option->email,
								$option->estado
							);
			 }
			return $data;
		}else{
			$data = FALSE;
			return $data;
		}
	}
} //<!--clase Personas_model-->
