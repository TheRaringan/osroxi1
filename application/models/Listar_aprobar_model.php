<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of listar_aprobar_model
 *
 * @author Nelly Moreno
 */
class listar_aprobar_model extends CI_Model
{
	
       
     public function listar_aprobar()
    {
		//$this->db->select('*');								
		//$this->db->from('solicitudes');
		
		//$query = $this->db->get();  
		$consulta_sql = "select 
								  s.id_solicitud_servicio as cod,
								  t.nombre tipo,
								  s.fecha ,
								  s.cedula_solicitante,
								  s.titulo_evento,
								  s.orden_interna,
								  s.cedula_aprobador,
								  s.celular_solicitante,
								  a.nombre as nombre_agrupacion
						from
							e_sislogin.solicitudes_servicios as s
						inner join
							e_sislogin.tipos_solicitudes as t
						on
							s.id_tipo_solicitud = t.id_tipo_solicitud
						inner join
							e_sislogin.agrupacion as a
						on
							s.agrupacion = a.id_agrupacion
						where
							s.id_estatus_servicio=1";
		$query = $this->db->query($consulta_sql); 
        
        foreach ($query->result() as $option)
        {
            $data[] = array($option->cod, $option->tipo, $option->fecha, $option->cedula_solicitante, $option->titulo_evento, $option->orden_interna, $option->cedula_aprobador, $option->celular_solicitante, $option->nombre_agrupacion);
        }
        return $data;
		
		//echo $this->db->last_query();
		         
        /*foreach ($query->result() as $option)
        {
            $data[] = array( $option->id_aud, $option->seccion, $option->accion, $option->campoclave, $option->id_us, $option->ip, $option->fecha, $option->hora);
        }

         
        return $data;*/
    }
}
