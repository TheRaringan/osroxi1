<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Alianza_model
 *
 * @author Jonelle Hernández
 */
class Alianza_model extends CI_Model{


	public function alianza(){
		$consulta_sql= "
						SELECT 
							alianza.id as id_alianza, 
							edo.descripcion as estado, 
							UPPER(nombre_alianza) as nombre_alianza,
							UPPER(responsable.nombre_apellido) as responsable,
							legales.fecha_suscripcion as fecha_suscripcion,
							UPPER(legales.duracion) as duracion,
							alianza.estatus as estatus

						FROM alianza.alianzas as alianza
						
						LEFT JOIN alianza.ubicacion as ubi ON ubi.id_alianza = alianza.id
						LEFT JOIN alianza.responsables as responsable ON responsable.id_alianza = alianza.id
						LEFT JOIN alianza.aspectos_legales as legales ON legales.id_alianza = alianza.id
						LEFT JOIN public.estado as edo ON edo.id_estado = ubi.id_estado

						ORDER BY estado asc";

		$query = $this->db->query($consulta_sql);

		if($query->result() != NULL){
			foreach ($query->result() as $option){
				$data[] = array(
								$option->id_alianza,			// 0
								$option->nombre_alianza,		// 1
								$option->estado,				// 2
								$option->responsable,			// 3
								$option->fecha_suscripcion,		// 4
								$option->duracion,				// 5
								$option->estatus				// 6
							);
			 }
			return $data;
		}else{
			$data = FALSE;
			return $data;
		}
	}

	public function alianza_2($postData){
		$consulta_sql= "
			SELECT 
				alianza.id as id_alianza,
				UPPER(upsa.nombre) as nombre_upsa,
				UPPER(alianza.nombre_alianza) as nombre_alianza,
				UPPER(responsable.nombre_apellido) as responsable,
				responsable.rif as rif,
				responsable.telefono as telefono,
				UPPER(responsable.direccion) as direccion,
				UPPER(responsable.correo) as correo,
				UPPER(edo.descripcion) as estado,
				UPPER(muni.descripcion) as municipio,
				UPPER(parro.descripcion) as parroquia,
				ubi.latitud as latitud,
				ubi.longitud as longitud,
				UPPER(legales.objeto) as objeto,
				UPPER(legales.aportes) as aportes,
				legales.fecha_suscripcion as fecha_suscripcion,
				UPPER(legales.duracion) as duracion,
				UPPER(legales.distribucion_utilidad) as distribucion_utilidad,
				UPPER(legales.distribucion_produccion) as distribucion_produccion,
				UPPER(legales.bienes_asignados) as bienes_asignados,
				UPPER(produccion.proyecto) as proyecto,
				produccion.fecha_siembra as fecha_siembra,
				UPPER(produccion.tiempo_cosecha) as tiempo_cosecha,
				produccion.has_sembradas as has_sembradas,
				produccion.has_cosechadas as has_cosechadas,
				produccion.fecha_cosecha as fecha_cosecha,
				produccion.peso as peso,
				UPPER(produccion.silo) as silo,
				UPPER(ficha.nombre) as nombre_contacto,
				UPPER(ficha.cedula) as cedula_contacto,
				UPPER(ficha.telefono) as telefono_contacto,
				ficha.fecha as fecha_contacto,
				UPPER(ficha.observaciones) as observaciones
				
			FROM alianza.alianzas as alianza

			LEFT JOIN public.upsa as upsa ON upsa.id_upsa = alianza.id_upsa
			LEFT JOIN alianza.responsables as responsable ON responsable.id_alianza = alianza.id
			LEFT JOIN alianza.ubicacion as ubi ON ubi.id_alianza = alianza.id
			LEFT JOIN public.estado as edo ON edo.id_estado = ubi.id_estado
			LEFT JOIN public.municipio as muni ON muni.id_municipio = ubi.id_municipio
			LEFT JOIN public.parroquia as parro ON parro.id_parroquia = ubi.id_parroquia
			LEFT JOIN alianza.aspectos_legales as legales ON legales.id_alianza = alianza.id
			LEFT JOIN alianza.aspectos_produccion as produccion ON produccion.id_alianza = alianza.id
			LEFT JOIN alianza.ficha_control as ficha ON ficha.id_alianza = alianza.id

			WHERE alianza.id = '".$postData['this_consulta']."'";

			$query = $this->db->query($consulta_sql);
			//echo($query);die;

			if($query->result() != NULL){
				foreach ($query->result() as $option){
					$data[] = array(
									$option->nombre_upsa,				//0
									$option->nombre_alianza,			//1
									$option->responsable,				//2
									$option->rif,						//3
									$option->telefono,					//4
									$option->direccion,					//5
									$option->correo,					//6
									$option->estado,					//7
									$option->municipio,					//8
									$option->parroquia,					//9
									$option->latitud,					//10
									$option->longitud,					//11
									$option->objeto,					//12
									$option->aportes,					//13
									$option->fecha_suscripcion,			//14
									$option->duracion,					//15
									$option->distribucion_utilidad,		//16
									$option->distribucion_produccion,	//17
									$option->bienes_asignados,			//18
									$option->proyecto,					//19
									$option->fecha_siembra,				//20
									$option->tiempo_cosecha,			//21
									$option->has_sembradas,				//22
									$option->has_cosechadas,			//23
									$option->fecha_cosecha,				//24
									$option->peso,						//25
									$option->silo,						//26
									$option->nombre_contacto,			//27
									$option->cedula_contacto,			//28
									$option->telefono_contacto,			//29
									$option->fecha_contacto,			//30
									$option->observaciones				//31
								   );
								   //print_r ($data);die;
				 }
				return $data;
			}else{
				$data = FALSE;
				return $data;
			}

			//$query = $this->db->query($consulta_sql);
	}

	public function get_upsas(){
		$this->db->select('id_upsa, nombre');
		$this->db->order_by('nombre');
		$query = $this->db->get('public.upsa');
		return $query->result_array();
	}

	public function get_estado(){
		$this->db->select('id_estado, descripcion');
		$this->db->order_by('descripcion');
		$query = $this->db->get('public.estado');
		return $query->result_array();
	}

	public function modificar_alianza($id_alianza ,$id_upsa, $nombre_alianza, $responsable, $rif, $telefono, $direccion, $correo, $id_estado, $id_municipio, $id_parroquia, $latitud, $longitud, $objeto, $aportes, $fecha_suscripcion, $duracion, $utilidad, $produccion, $bienes_asignados, $proyecto, $fecha_siembra, $tiempo_cosecha, $has_sembradas, $has_cosechadas, $fecha_cosecha, $peso_liquidar, $silo, $nombre_contacto, $cedula_contacto, $telefono_contacto, $fecha_contacto, $informacion){

//			var_dump($id_alianza);die;

			if(empty($id_upsa))
				$query= $this->db->query("UPDATE alianza.alianzas SET nombre_alianza = '$nombre_alianza' WHERE id = $id_alianza");
			else
				$query= $this->db->query("UPDATE alianza.alianzas SET id_upsa = $id_upsa, nombre_alianza = '$nombre_alianza' WHERE id = $id_alianza");


			if(!empty($id_estado))
				$query= $this->db->query("UPDATE alianza.ubicacion SET id_estado = $id_estado, id_municipio = $id_municipio, id_parroquia = $id_parroquia, latitud = $latitud, longitud = $longitud WHERE id_alianza = $id_alianza");


			$query= $this->db->query("UPDATE alianza.responsables SET nombre_apellido = '$responsable', rif = '$rif', telefono = '$telefono', direccion = '$direccion', correo = '$correo' WHERE id_alianza = $id_alianza");

			if(empty($bienes_asignados))
				$query= $this->db->query("UPDATE alianza.aspectos_legales SET fecha_suscripcion = '$fecha_suscripcion', objeto = '$objeto', aportes = '$aportes', duracion = '$duracion', distribucion_utilidad = '$utilidad', distribucion_produccion = '$produccion' WHERE id_alianza = $id_alianza");
			else
				$query= $this->db->query("UPDATE alianza.aspectos_legales SET fecha_suscripcion = '$fecha_suscripcion', objeto = '$objeto', aportes = '$aportes', duracion = '$duracion', distribucion_utilidad = '$utilidad', distribucion_produccion = '$produccion', bienes_asignados = '$bienes_asignados' WHERE id_alianza = $id_alianza");

			$query= $this->db->query("UPDATE alianza.aspectos_produccion SET fecha_siembra = '$fecha_siembra', has_sembradas = $has_sembradas, has_cosechadas = $has_cosechadas, fecha_cosecha = '$fecha_cosecha', peso = $peso_liquidar, silo = '$silo', proyecto = '$proyecto', tiempo_cosecha = '$tiempo_cosecha' WHERE id_alianza = $id_alianza");

			$query= $this->db->query("UPDATE alianza.ficha_control SET nombre = '$nombre_contacto', cedula = '$cedula_contacto', telefono = '$telefono_contacto', fecha = '$fecha_contacto', observaciones = '$informacion' WHERE id_alianza = $id_alianza");

	        return $query;
	}

    public function modificar_estatus($id, $estatus)
    {
        $query = false;

        $query = $this->db->query("UPDATE alianza.alianzas SET estatus = $estatus WHERE id = $id");

        if ($query != false)
            return $query;
        else
            return $query;
    }
} //<!--clase Alianza_model-->
