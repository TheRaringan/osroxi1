<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Registro_alianza_models
 *
 * @author Jonelle Hernández
 */
class Registro_alianza_model extends CI_Model{


	public function get_upsas(){
		$this->db->select('id_upsa, nombre');
		$this->db->order_by('nombre');
		$query = $this->db->get('public.upsa');
		return $query->result_array();
	}

	public function get_estado(){
		$this->db->select('id_estado, descripcion');
		$this->db->order_by('descripcion');
		$query = $this->db->get('public.estado');
		return $query->result_array();
	}
			
	public	function get_municipio($postData){
		$response = array();
		$this->db->select('id_municipio');
		$this->db->where('id_upsa', $postData['this_id_upsa']);
		$q = $this->db->get('ubicacion');
		$response = $q->result_array();
		return $response;
	}

	public	function get_parroquia($postData){
		$response = array();
		$this->db->select('id_parroquia, UPPER(descripcion) as descripcion');
		$this->db->where('id_municipio', $postData['this_id_municipio']);
		$this->db->order_by('descripcion');
		$q = $this->db->get('parroquia');
		$response = $q->result_array();
		return $response;
	}

	public function get_id_alianza(){
		$this->db->select('MAX(id)');
		$query = $this->db->get('alianza.alianzas');
		return $query->row_array();
	}

	public function registro_alianza($id_upsa, $nombre_alianza, $responsable, $rif, $telefono, $direccion, $correo, $id_estado, $id_municipio, $id_parroquia, $latitud, $longitud, $objeto, $aportes, $fecha_suscripcion, $duracion, $utilidad, $produccion, $bienes_asignados, $proyecto, $fecha_siembra, $tiempo_cosecha, $has_sembradas, $has_cosechadas, $fecha_cosecha, $peso_liquidar, $silo, $nombre_contacto, $cedula_contacto, $telefono_contacto, $informacion){

//		var_dump($id_upsa, $nombre_alianza);die;

		$query= $this->db->query("INSERT INTO alianza.alianzas(id_upsa, nombre_alianza) VALUES ($id_upsa,'$nombre_alianza')");

 		$id_ali = $this->get_id_alianza();
 		$id_alianza = $id_ali['max'];

		$query= $this->db->query("INSERT INTO alianza.ubicacion(id_alianza, id_estado, id_municipio, id_parroquia, latitud, longitud) VALUES ($id_alianza, $id_estado, $id_municipio, $id_parroquia, $latitud, $longitud)");

		$query= $this->db->query("INSERT INTO alianza.responsables(id_alianza, nombre_apellido, rif, telefono, direccion, correo) VALUES ($id_alianza, '$responsable', '$rif', '$telefono', '$direccion', '$correo')");

		$query= $this->db->query("INSERT INTO alianza.aspectos_legales(id_alianza, objeto, aportes, duracion, fecha_suscripcion, distribucion_utilidad, distribucion_produccion, bienes_asignados) VALUES ($id_alianza, '$objeto', '$aportes', '$duracion', '$fecha_suscripcion', '$utilidad', '$produccion', '$bienes_asignados')");

		$query= $this->db->query("INSERT INTO alianza.aspectos_produccion(id_alianza, proyecto, fecha_siembra, tiempo_cosecha, has_sembradas, has_cosechadas, fecha_cosecha, peso, silo) VALUES ($id_alianza, '$proyecto', '$fecha_siembra', '$tiempo_cosecha', '$has_sembradas', '$has_cosechadas', '$fecha_cosecha', '$peso_liquidar', '$silo')");

		$query= $this->db->query("INSERT INTO alianza.ficha_control(id_alianza, nombre, cedula, telefono, observaciones) VALUES ($id_alianza, '$nombre_contacto', '$cedula_contacto', '$telefono_contacto', '$informacion')");

        return $query;
	}

} //<!--clase Personas_model-->
