<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**Principal_models
 *
 * @author Alexander De Abreu
 */
class Modificar_rubros_model extends CI_Model
{
	 public function consultar_rubros()
    {

        $query = $this->db->query("SELECT rub.id_rubro as id_rubro, rub.nombre as nombre_rubro, rub.rendimiento as rendimiento, rub.densidad as densidad, rub.ciclo as ciclo, rub.activo as estatus
        FROM ecopro.rubros rub");
        return $query->result_array();

    }

    public function consultar_rubro($id)
    {
        $query = $this->db->query("SELECT rub.id_rubro as id_rubro, rub.nombre as nombre_rubro, rub.rendimiento as rendimiento, rub.densidad as densidad, rub.ciclo as ciclo, rub.activo as estatus
        FROM ecopro.rubros rub
        WHERE rub.id_rubro = $id");
        return $query->row_array();

    }

    public function modificar_rubro($id, $nombre, $rendimiento, $densidad, $ciclo, $estatus)
    {

        $query = false;

        // Consulta individual para evitar duplicados en los nombres de los rubros
        $consulta_individual = $this->db->query("SELECT * FROM ecopro.rubros WHERE upper(nombre) = upper('$nombre')" );

        // Esto se hace para obtener de manera individual el nombre del rubro en forma de STRING
        $consulta = $this->db->query("SELECT nombre FROM ecopro.rubros WHERE id_rubro = $id" )->result_array();
        $data = array_column($consulta, 'nombre');
        $nombre_c = $data[0];

        // Esto se hace para obtener de manera individual el rendimiento del rubro en forma de STRING
        $consulta_rendimiento = $this->db->query("SELECT rendimiento FROM ecopro.rubros WHERE id_rubro = $id" )->result_array();
        $data = array_column($consulta_rendimiento, 'rendimiento');
        $rendimiento_c = $data[0];

        // Esto se hace para obtener de manera individual la densidad del rubro en forma de STRING
        $consulta_densidad = $this->db->query("SELECT densidad FROM ecopro.rubros WHERE id_rubro = $id" )->result_array();
        $data = array_column($consulta_densidad, 'densidad');
        $densidad_c = $data[0];

        // Esto se hace para obtener de manera individual el ciclo del rubro en forma de STRING
        $consulta_ciclo = $this->db->query("SELECT ciclo FROM ecopro.rubros WHERE id_rubro = $id" )->result_array();
        $data = array_column($consulta_ciclo, 'ciclo');
        $ciclo_c = $data[0];

        // Esto se hace para obtener de manera individual el estatus del rubro en forma de STRING
        $consulta_estatus = $this->db->query("SELECT activo FROM ecopro.rubros WHERE id_rubro = $id" )->result_array();
        $data = array_column($consulta_estatus, 'activo');
        $estatus_c = $data[0];
       
       
        if ($nombre_c != $nombre)
        {
            if ($consulta_individual->num_rows() > 0)
            {
                echo "<script>alert('El nombre de rubro: $nombre se encuentra actualmente en uso.');</script>";
            }
            else
                $query = $this->db->query("UPDATE ecopro.rubros SET nombre = '$nombre' WHERE id_rubro = $id");
        }

        if ($rendimiento_c != $rendimiento)
        {
            $query = $this->db->query("UPDATE ecopro.rubros SET rendimiento = $rendimiento WHERE id_rubro = $id");
        }

        if ($densidad_c != $densidad)
        {
            $query = $this->db->query("UPDATE ecopro.rubros SET densidad = '$densidad' WHERE id_rubro = $id");
        }

        if ($ciclo_c != $ciclo)
        {
            $query = $this->db->query("UPDATE ecopro.rubros SET ciclo = '$ciclo' WHERE id_rubro = $id");
        }

        if ($estatus_c != $estatus && $estatus != '')
        {
            $query = $this->db->query("UPDATE ecopro.rubros SET activo = $estatus WHERE id_rubro = $id");
            if ($estatus == 0)
                $query = $this->db->query("UPDATE ecopro.insumos_x_rubro SET activo = 0 WHERE id_rubro = $id");
            else if ($estatus == 1)
                $query = $this->db->query("UPDATE ecopro.insumos_x_rubro SET activo = 1 WHERE id_rubro = $id");
        }



        if ($query != false)
        {
            return $query;
        }
        else
        {
            $this->session->set_flashdata('pnotify','rubro_sin_modificaciones');
            return $query;
        }



    }


    public function modificar_estatus($id, $estatus)
    {

        $query = false;


        $query = $this->db->query("UPDATE ecopro.rubros SET activo = $estatus WHERE id_rubro = $id");
        if ($estatus == 0)
            $query = $this->db->query("UPDATE ecopro.insumos_x_rubro SET activo = 0 WHERE id_rubro = $id");
        else if ($estatus == 1)
            $query = $this->db->query("UPDATE ecopro.insumos_x_rubro SET activo = 1 WHERE id_rubro = $id");



        if ($query != false)
        {
            $this->session->set_flashdata('pnotify','insert');
            return $query;
        }
        else
        {
            $this->session->set_flashdata('pnotify','rubro_sin_modificaciones');
            return $query;
        }
    }


    public function agregar_rubro_s($nombre, $rendimiento, $densidad, $ciclo, $estatus)
    {
        $query = $this->db->query("INSERT INTO ecopro.rubros (nombre, rendimiento, densidad, ciclo, activo) values ('$nombre', $rendimiento, $densidad, $ciclo, '1')");
        return $query;
    }
     

} //<!--clase Principal_models-->
