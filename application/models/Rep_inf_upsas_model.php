<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Personas_model
 *
 * @author Nelly Moreno & Albert Torres
 */
class Rep_inf_upsas_model extends CI_Model{

	public function listar_inf_upsas(){
		$consulta_sql= "SELECT usuario,
       						   cedula,
							   celular,
							   correo
					    from maestro.usuarios
						where perfil_id <> 777 	AND perfil_id <> 111";

		$query = $this->db->query($consulta_sql);

		if($query->result() != ""){
			foreach ($query->result() as $option){
				$data[] = array($option->usuario,
								$option->cedula,
								$option->celular,
								$option->correo
							);
			 }
			 //var_dump($data);die;

			return $data;
		}else{
			$data = FALSE;
			return $data;
		}
	}


	public function listar_upsas_sinInf(){
		$consulta_sql= "SELECT usu.usuario,
						       usu.cedula,
							   CONCAT(per.nombre,' ',per.apellido) AS nombre_apellido,
						       usu.celular,
						       usu.correo,
       							usu.id_upsa
						FROM maestro.usuarios as usu
						INNER JOIN maestro.persona as per ON per.id_usuario = usu.id_usuario
						WHERE perfil_id <> 777 	AND perfil_id <> 111
						AND usu.id_upsa = 0";

		$query = $this->db->query($consulta_sql);

		if($query->result() != null){
			foreach ($query->result() as $option){
				$data[] = array($option->usuario,
								$option->cedula,
								$option->nombre_apellido,
								$option->celular,
								$option->correo,
								$option->id_upsa
							);
			 }
			 //var_dump($data);die;

			return $data;
		}else{
			$data = FALSE	;
			return $data;
		}
	}
} //<!--clase Personas_model-->
