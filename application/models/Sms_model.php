<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Sms_model
 *
 * @author Nelly Moreno
 */
class Sms_model extends CI_Model
{
	
      public function listar_bandeja_entrada()
    {
        $this->db->select('ReceivingDateTime as fecha,
                            SenderNumber as enviado_por,
                            SMSCNumber as recibido_por,
                            TextDecoded as texto');                             
        $this->db->from('inbox');
        
        $query = $this->db->get();  
        
        //echo $this->db->last_query();
                 
        foreach ($query->result() as $option)
        {
            $data[] = array( $option->fecha, $option->enviado_por,$option->recibido_por, $option->texto);
        }

         
        return $data;
    }

     public function listar_bandeja_salida()
    {
		$this->db->select('id,texto,fecha,estatus,cantidad_registrados,cantidad_enviados,tlf_individual,id_modem');								
		$this->db->from('enviados');
		
		$query = $this->db->get();  
		
		//echo $this->db->last_query();
		         
        foreach ($query->result() as $option)
        {
            $data[] = array( $option->id, $option->texto,$option->fecha, $option->estatus, $option->cantidad_registrados, $option->cantidad_enviados, $option->tlf_individual, $option->id_modem);
        }

         
        return $data;
    }
    public function envio_sms($texto,$tlf_ind)
    {
		 $consulta_sql = "select envio_sms('$texto','modem1','{}',1,0, 2, '$tlf_ind')"; 
		

		$query = $this->db->query($consulta_sql); 
			//echo $this->db->last_query();
		foreach ($query->result() as $option)
        {
            $data[] = array( $option->envio_sms);
        }
         
        return $data;
    }
    
}
