<?php
    class Mapa_model extends CI_Model{
        public function get_totales_pecua(){
            $query = $this->db->get('total_upsa_pecua_por_edo');
            return $query->row_array();
        }
        public function get_totales_agric(){
            $query = $this->db->get('total_upsa_agric_por_edo');
            return $query->row_array();
        }
        public function get_total_upsa(){
            $query = $this->db->get('total_upsa_por_edo');
            return $query->row_array();
        }
        public function get_percent_agric(){
            $query = $this->db->get('porc_superf_opert_upsa_agric_edo');
            return $query->row_array();
        }
        public function get_percent_pecua(){
            $query = $this->db->get('porc_superf_opert_upsa_pecua_edo');
            return $query->row_array();
        }

        public function get_total_agri_nac(){
			$consulta_sql="SELECT sum(co.superficie_operativa_agricola) as total_ope_agricola_nac,
                                  sum(co.superficie_aprovechable_agricola) as total_apro_agri_nac
                           FROM (SELECT co_1.superficie_operativa_agricola,
                                         co_1.superficie_aprovechable_agricola
                                FROM capacidad_operativa co_1
                                    JOIN actividad_econ_upsa aeu ON aeu.id_upsa = co_1.id_upsa
                                WHERE aeu.id_tipo_actividad_econ = 1
                                GROUP BY co_1.id_upsa, co_1.superficie_operativa_agricola, co_1.superficie_aprovechable_agricola
                                ORDER BY co_1.id_upsa) co";

			$query = $this->db->query($consulta_sql);

            //var_dump($query);die;

			if ($query->result() != NULL) {
				foreach ($query->result() as $key) {
					$data[] = array($key->total_ope_agricola_nac,
									$key->total_apro_agri_nac
								);
                    //print_r ($data);die;
				}
				return $data;
			}else {
				$data = FALSE;
				return $data;
			}
		}

        public function get_total_pecu_nac(){
			$consulta_sql="SELECT sum(co.superficie_operativa_pecuaria) as total_ope_pecua_nac,
                                  sum(co.superficie_aprovechable_pecuaria) as total_apro_pecua_nac
                            FROM (SELECT co_1.superficie_operativa_pecuaria,
                            	     co_1.superficie_aprovechable_pecuaria
                            FROM capacidad_operativa co_1
                                JOIN actividad_econ_upsa aeu ON aeu.id_upsa = co_1.id_upsa
                            WHERE aeu.id_tipo_actividad_econ = 2
                            GROUP BY co_1.id_upsa, co_1.superficie_operativa_pecuaria,co_1.superficie_aprovechable_pecuaria
                            ORDER BY co_1.id_upsa) co";

			$query = $this->db->query($consulta_sql);

			if ($query->result() != NULL) {
				foreach ($query->result() as $key) {
					$data[] = array($key->total_ope_pecua_nac,
									$key->total_apro_pecua_nac
								);
                    //print_r ($data);die;
				}
				return $data;
			}else {
				$data = FALSE;
				return $data;
			}
		}

        public function total_otros(){
			$consulta_sql="SELECT sum(superficie_estabulada) as estabulada,
                            	  sum(superficie_reserva_natural) as reservas_natu,
                            	  sum(total_hectareas) as total_hectareas
                            FROM capacidad_operativa";

			$query = $this->db->query($consulta_sql);

			if ($query->result() != NULL) {
				foreach ($query->result() as $key) {
					$data[] = array($key->estabulada,
									$key->reservas_natu,
                                    $key->total_hectareas
								);
				}
				return $data;
			}else {
				$data = FALSE;
				return $data;
			}
		}


    }
?>
