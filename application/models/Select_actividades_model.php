<?php

	class Select_actividades_model extends CI_Model{

		public function get_tipos_actividades(){
			$this->db->select('id_tipo_actividad_econ, nombre');
			$this->db->order_by('nombre');
			$query = $this->db->get('tipo_actividad_econ');
			return $query->result_array();
		}

		public function get_actividades($postData){
			$this->db->select('id_actividad_econ,nombre');
			$this->db->where('id_tipo_actividad_econ',$postData['this_tipo_actividad']);
			$this->db->order_by('nombre');
			$query = $this->db->get('actividad_econ');
			$response = $query->result_array();
			return $response;
		}

		public function get_rubros($postData){
			$this->db->select('id_rubro,nombre');
			$this->db->where('id_actividad_econ',$postData['this_actividad']);
			$this->db->order_by('nombre');
			$query = $this->db->get('rubro');
			$response = $query->result_array();
			return $response;
		}

		public function get_subrubros($postData){
			$this->db->select('id_subrubro,nombre');
			$this->db->where('id_rubro',$postData['this_rubro']);
			$this->db->order_by('nombre');
			$query = $this->db->get('subrubro');
			$response = $query->result_array();
			return $response;
		}

		public function get_unidades_medidas(){
			$this->db->select('id_unidad_medida,nombre');
			$this->db->order_by('nombre');
			$query = $this->db->get('unidad_medida');
			return $query->result_array();
		}

		public function get_estados_operacion(){
			$this->db->select('id_estado_operacion,nombre');
			$this->db->order_by('nombre');
			$query = $this->db->get('estado_operacion');
			return $query->result_array();
		}
	}
