<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Trabajadores_model
 *
 * @author Nelly Moreno
 */
class Calculador_model extends CI_Model
{
	
	 public function listar_trabajadores()
    {
					
		/*$this->db->select('t.cedula, 
                            g.nombre as gerencia,
                            c.nombre as cargo, 
                            p.primer_nombre||" "|| p.primer_apellido as aprobador')
                ->from('e_sislogin.trabajadores as t')
                ->join('e_sislogin.gerencias as g', 't.id_gerencia = g.id_gerencia')
                ->join('e_sislogin.cargos as c', 't.id_cargo = c.id_cargo')
                ->join('e_sislogin.personas as p', 't.cedula = p.cedula');
        $query = $this->db->get();*/
        $consulta_sql = "select 
								t.cedula, 
								g.nombre as gerencia,
								c.nombre as cargo, 
								p.primer_nombre||' '|| p.primer_apellido as aprobador
						from
							e_sislogin.trabajadores as t
						inner join
							e_sislogin.gerencias as g
						on
							t.id_gerencia = g.id_gerencia
						inner join
							e_sislogin.cargos as c
						on	
								t.id_cargo = c.id_cargo
						inner join
								e_sislogin.personas as p
						on
							t.cedula = p.cedula";
		$query = $this->db->query($consulta_sql); 
        
        foreach ($query->result() as $option)
        {
            $data[] = array($option->cedula, $option->gerencia, $option->cargo, $option->aprobador);
        }
        return $data;
	}
	 public function consultar_insumos_method($rubro)
    {
    
        $id_estado = $_SESSION['id_estado'];
		$consulta_sql = "select  
                                tipo,
                                insumo,
                                unidad_medida,
                                cantidad,
                                costo
                         from
                                ecopro.insumos_x_rubro
                         where
                                id_rubro=".$rubro." AND id_estado=".$id_estado; 
        //var_dump($consulta_sql);die('');                        
		$query = $this->db->query($consulta_sql); 
         return $query->result();
        //print_r($this->db->last_query());
	}
	
	
	public function cargar_rubros()
    {
    
        $this->db->select('id_rubro, nombre')
                ->from('ecopro.rubros');
        $query = $this->db->get();
        
        //print_r($this->db->last_query());
        
        foreach ($query->result() as $option)
        {
            $data[] = array($option->id_rubro, $option->nombre);
        }
        return $data;

    }
	
    /*
    *  Rubros Todos
    */
    public function consultar_rubros_todos(){
        
        $this->db->select('rendimiento, densidad, ciclo')
                ->from('ecopro.rubros');
        
        $query = $this->db->get();
                
        foreach ($query->result() as $option){
            $data[] = array($option->rendimiento, $option->densidad, $option->ciclo);
        }

        return $data;
    }
	
    
    
    
   
    
    
     

  

} //<!--clase Personas_model-->
