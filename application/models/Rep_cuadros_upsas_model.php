<?php
	class Rep_cuadros_upsas_model extends CI_Model{

		public function listar_op(){
			$consulta_sql="SELECT es.descripcion as estado,
								  upsa.nombre as nombre_upsa,
								  est_op.nombre as operatividad
							FROM upsa
							LEFT JOIN ubicacion as ub ON ub.id_upsa = upsa.id_upsa
							LEFT JOIN estado as es ON es.id_estado = ub.id_estado
							LEFT JOIN actividad_econ_upsa as eco ON eco.id_upsa = upsa.id_upsa
							INNER JOIN estado_operacion as est_op ON est_op.id_estado_operacion = eco.id_estado_operacion";

							$query =  $this->db->query($consulta_sql);

							if ($query->result() != NULL) {
								foreach ($query->result() as $key) {
									$data[] = array($key->estado,
													$key->nombre_upsa,
													$key->operatividad
												);
								}
								return $data;
							}else {
								$data = FALSE;
								return $data;
							}
		}

		public function listar_act_prod(){
			$consulta_sql="SELECT es.descripcion as estado,
							       upsa.nombre as nombre_upsa,
							       tip_act.nombre as tipo_actividad,
							       act.nombre as actividad,
							       r.nombre as rubro,
							       eco.produccion_mensual,
							       um.descripcion as unidad_medida,
							       eo.nombre as est_operativo
							FROM upsa
							LEFT JOIN ubicacion as ub ON ub.id_upsa = upsa.id_upsa
							LEFT JOIN estado as es ON es.id_estado = ub.id_estado
							LEFT JOIN actividad_econ_upsa as eco ON eco.id_upsa = upsa.id_upsa
							INNER JOIN tipo_actividad_econ as tip_act ON tip_act.id_tipo_actividad_econ = eco.id_tipo_actividad_econ
							INNER JOIN actividad_econ as act ON act.id_actividad_econ = eco.id_actividad_econ
							INNER JOIN rubro as r ON r.id_rubro = eco.id_rubro
							INNER JOIN estado_operacion as eo ON eo.id_estado_operacion = eco.id_estado_operacion
							INNER JOIN unidad_medida as um ON um.id_unidad_medida = eco.id_unidad_medida";

							$query =  $this->db->query($consulta_sql);

							if ($query->result() != NULL) {
								foreach ($query->result() as $key) {
									$data[] = array($key->estado,
													$key->nombre_upsa,
													$key->tipo_actividad,
													$key->actividad,
													$key->rubro,
													$key->produccion_mensual,
													$key->unidad_medida,
													$key->est_operativo,
												);
								}
								return $data;
							}else {
								$data = FALSE;
								return $data;
							}
		}

		public function listar_rec_hib(){
			$consulta_sql="SELECT es.descripcion as estado,
								  upsa.nombre as nombre_upsa,
								  tip_rec_hib.nombre as tipo_recurso,
								  rec.nombre as recurso
							FROM upsa
							LEFT JOIN ubicacion as ub ON ub.id_upsa = upsa.id_upsa
							LEFT JOIN estado as es ON es.id_estado = ub.id_estado
							LEFT JOIN recursos_hidr_upsa as eco ON eco.id_upsa = upsa.id_upsa
							INNER JOIN tipo_recurso_hidr as tip_rec_hib ON tip_rec_hib.id_tipo_recurso_hidr = eco.id_tipo_recurso_hidr
							INNER JOIN recurso_hidr as rec ON  rec.id_recurso_hidr = eco.id_recurso_hidr";

							$query =  $this->db->query($consulta_sql);

							if ($query->result() != NULL) {
								foreach ($query->result() as $key) {
									$data[] = array($key->estado,
													$key->nombre_upsa,
													$key->tipo_recurso,
													$key->recurso
												);
								}
								return $data;
							}else {
								$data = FALSE;
								return $data;
							}
		}

		public function listar_voc_tie(){
			$consulta_sql="SELECT es.descripcion as estado,
								  upsa.nombre as nombre_upsa,
								  uso_voc. nombre as uso,
								  voc.clase,
								  voc.rubro as tipo_recurso
							FROM upsa
							LEFT JOIN ubicacion as ub ON ub.id_upsa = upsa.id_upsa
							LEFT JOIN estado as es ON es.id_estado = ub.id_estado
							LEFT JOIN vocacion_upsa as voc_upsa ON voc_upsa.id_upsa = upsa.id_upsa
							INNER JOIN vocacion as voc ON voc.id_vocacion = voc_upsa.id_vocacion
							INNER JOIN uso_vocacion AS uso_voc ON uso_voc.id_uso_vocacion = voc.id_uso_vocacion";

							$query =  $this->db->query($consulta_sql);

							if ($query->result() != NULL) {
								foreach ($query->result() as $key) {
									$data[] = array($key->estado,
													$key->nombre_upsa,
													$key->uso,
													$key->clase,
													$key->tipo_recurso
												);
								}
								return $data;
							}else {
								$data = FALSE;
								return $data;
							}
		}

		public function sup_total(){
			$consulta_sql="SELECT est.descripcion as estado,
									upsa.nombre as nombre_upsa,
									re.nombre as relacion,
									cap.total_hectareas
								FROM UPSA
								LEFT JOIN coordinador as cor ON cor.id_upsa = upsa.id_upsa
								LEFT JOIN ubicacion as ubi ON ubi.id_upsa = upsa.id_upsa
								LEFT JOIN estado as est ON est.id_estado = ubi.id_estado
								LEFT JOIN relacion_corpo as re ON re.id_relacion_corpo = upsa.id_relacion_corpo
								LEFT JOIN capacidad_operativa as cap ON cap.id_upsa = upsa.id_upsa
								ORDER BY estado";

							$query =  $this->db->query($consulta_sql);

							if ($query->result() != NULL) {
								foreach ($query->result() as $key) {
									$data[] = array($key->estado,
													$key->nombre_upsa,
													$key->relacion,
													$key->total_hectareas
												);
								}
								return $data;
							}else {
								$data = FALSE;
								return $data;
							}
		}

		public function ubicacion_cap(){
			$consulta_sql="SELECT us.id_upsa,
							CONCAT(coor.nombre,' ',coor.apellido) as coordinador,
							up.nombre as upsa,
							COUNT(ub.id_ubicacion) as ubicacion,
							COUNT(co.id_capacidad_operativa) as capacidad,
							COUNT(ae.id_actividad_econ_upsa) as actividad,
							COUNT(rh.id_recursos_hidr_upsa) as recursos
						FROM maestro.usuarios AS us
						LEFT JOIN coordinador AS coor ON coor.id_upsa = us.id_upsa
						LEFT JOIN upsa AS up ON up.id_upsa = us.id_upsa
						LEFT JOIN ubicacion AS ub ON ub.id_upsa = us.id_upsa
						LEFT JOIN capacidad_operativa AS co ON co.id_upsa = us.id_upsa
						LEFT JOIN actividad_econ_upsa AS ae ON ae.id_upsa = us.id_upsa
						LEFT JOIN recursos_hidr_upsa AS rh ON rh.id_upsa = us.id_upsa
						WHERE us.id_upsa != 0
						GROUP BY us.id_upsa,coor.nombre,coor.apellido,up.nombre
						ORDER BY us.id_upsa";

							$query =  $this->db->query($consulta_sql);

							if ($query->result() != NULL) {
								foreach ($query->result() as $key) {
									$data[] = array($key->id_upsa,
													$key->coordinador,
													$key->upsa,
													$key->ubicacion,
													$key->capacidad,
													$key->actividad,
													$key->recursos
												);
								}
								return $data;
							}else {
								$data = FALSE;
								return $data;
							}
		}

	}

?>
