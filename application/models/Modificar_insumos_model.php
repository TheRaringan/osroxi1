<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**Principal_models
 *
 * @author Alexander De Abreu
 */
class Modificar_insumos_model extends CI_Model
{
    public function consultar_rubros()
    {

        $query = $this->db->query("SELECT rub.id_rubro as id_rubro, rub.nombre as nombre_rubro, rub.rendimiento as rendimiento, rub.densidad as densidad, rub.ciclo as ciclo, rub.activo as estatus
        FROM ecopro.rubros rub");
        return $query->result_array();

    }
     

    public function consultar_insumos()
    {
        $id_estado = $_SESSION['id_estado'];
        $query = $this->db->query("SELECT ins.id_ins_rub as id_insumo, rub.nombre as nombre, ins.tipo as tipo, ins.insumo as insumo,
        ins.unidad_medida as medida, ins.cantidad as cantidad, ins.costo as bolivares, ins.activo as estatus
        FROM ecopro.insumos_x_rubro ins
        JOIN ecopro.rubros rub ON rub.id_rubro = ins.id_rubro
        WHERE ins.activo = 1 AND ins.id_estado = $id_estado");
        return $query->result_array();

    }

    public function consultar_insumos_general()
    {
        $id_estado = $_SESSION['id_estado'];
        $query = $this->db->query("SELECT ins.id_ins_rub as id_insumo, rub.nombre as nombre, ins.tipo as tipo, ins.insumo as insumo,
        ins.unidad_medida as medida, ins.cantidad as cantidad, ins.costo as bolivares, ins.activo as estatus, estado.descripcion as estado
        FROM ecopro.insumos_x_rubro ins
        JOIN ecopro.rubros rub ON rub.id_rubro = ins.id_rubro
        JOIN public.estado estado ON estado.id_estado = ins.id_estado
        WHERE ins.activo = 1");
        return $query->result_array();

    }

    public function consultar_insumo($id)
    {
        $query = $this->db->query("SELECT ins.id_ins_rub as id_insumo, rub.nombre as nombre, ins.tipo as tipo, ins.insumo as insumo,
        ins.unidad_medida as medida, ins.cantidad as cantidad, ins.costo as bolivares, ins.activo as estatus
        FROM ecopro.insumos_x_rubro ins
        JOIN ecopro.rubros rub ON rub.id_rubro = ins.id_rubro
        WHERE ins.id_ins_rub = $id");
        return $query->row_array();

    }

    public function agregar_insumo_s($rubro, $tipo, $insumo, $medida, $cantidad, $bolivares)
    {
        $id_estado = $_SESSION['id_estado'];
        $query = $this->db->query("INSERT INTO ecopro.insumos_x_rubro (id_rubro, tipo, insumo, unidad_medida, cantidad, costo, activo, id_estado) values ($rubro, '$tipo', '$insumo', '$medida', $cantidad, $bolivares, 1, $id_estado)");
        return $query;
    }

    public function modificar_insumo($id_insumo, $rubro, $tipo, $insumo, $medida, $cantidad, $bolivares)
    {
        $query = false;

        // Esto se hace para obtener de manera individual el rubro del insumo en forma de STRING
        $consulta_rubro = $this->db->query("SELECT id_rubro FROM ecopro.insumos_x_rubro WHERE id_ins_rub = $id_insumo" )->result_array();
        $data = array_column($consulta_rubro, 'id_rubro');
        $rubro_c = $data[0];

        // Esto se hace para obtener de manera individual el tipo del insumo en forma de STRING
        $consulta_tipo = $this->db->query("SELECT tipo FROM ecopro.insumos_x_rubro WHERE id_ins_rub = $id_insumo" )->result_array();
        $data = array_column($consulta_tipo, 'tipo');
        $tipo_c = $data[0];

        // Esto se hace para obtener de manera individual el insumo en específico en forma de STRING
        $consulta_insumo = $this->db->query("SELECT insumo FROM ecopro.insumos_x_rubro WHERE id_ins_rub = $id_insumo" )->result_array();
        $data = array_column($consulta_insumo, 'insumo');
        $insumo_c = $data[0];

        // Esto se hace para obtener de manera individual la medida en forma de STRING
        $consulta_medida = $this->db->query("SELECT unidad_medida FROM ecopro.insumos_x_rubro WHERE id_ins_rub = $id_insumo" )->result_array();
        $data = array_column($consulta_medida, 'unidad_medida');
        $medida_c = $data[0];

        // Esto se hace para obtener de manera individual la cantidad del insumo en forma de STRING
        $consulta_cantidad = $this->db->query("SELECT cantidad FROM ecopro.insumos_x_rubro WHERE id_ins_rub = $id_insumo" )->result_array();
        $data = array_column($consulta_cantidad, 'cantidad');
        $cantidad_c = $data[0];

        // Esto se hace para obtener de manera individual el valor del insumo en bolivares en forma de STRING
        $consulta_bolivares = $this->db->query("SELECT costo FROM ecopro.insumos_x_rubro WHERE id_ins_rub = $id_insumo" )->result_array();
        $data = array_column($consulta_bolivares, 'costo');
        $bolivares_c = $data[0];

        // Esto se hace para obtener de manera individual el valor del insumo en dolares en forma de STRING
        /* $consulta_dolares = $this->db->query("SELECT dolares FROM ecopro.insumos_x_rubro WHERE id_ins_rub = $id_insumo" )->result_array();
        $data = array_column($consulta_dolares, 'dolares');
        $dolares_c = $data[0]; */

        // Esto se hace para obtener de manera individual el estatus del insumo en forma de STRING
        $consulta_estatus = $this->db->query("SELECT activo FROM ecopro.insumos_x_rubro WHERE id_ins_rub = $id_insumo" )->result_array();
        $data = array_column($consulta_estatus, 'activo');
        $estatus_c = $data[0];

        if ($rubro_c != $rubro && $rubro != '')
        {
            $query = $this->db->query("UPDATE ecopro.insumos_x_rubro SET id_rubro = $rubro WHERE id_ins_rub = $id_insumo");
        }

        if ($tipo_c != $tipo )
        {
            $query = $this->db->query("UPDATE ecopro.insumos_x_rubro SET tipo = '$tipo' WHERE id_ins_rub = $id_insumo");
        }

        if ($insumo_c != $insumo)
        {
            $query = $this->db->query("UPDATE ecopro.insumos_x_rubro SET insumo = '$tipo' WHERE id_ins_rub = $id_insumo");
        }

        if ($medida_c != $medida)
        {
            $query = $this->db->query("UPDATE ecopro.insumos_x_rubro SET unidad_medida = '$medida' WHERE id_ins_rub = $id_insumo");
        }

        if ($cantidad_c != $cantidad)
        {
            $query = $this->db->query("UPDATE ecopro.insumos_x_rubro SET cantidad = $cantidad WHERE id_ins_rub = $id_insumo");
        }

        if ($bolivares_c != $bolivares)
        {
            $query = $this->db->query("UPDATE ecopro.insumos_x_rubro SET costo = $bolivares WHERE id_ins_rub = $id_insumo");
        }

        if ($query != false)
        {
            $this->session->set_flashdata('pnotify','insumo_modificado');
            return $query;
        }
        else
        {
            $this->session->set_flashdata('pnotify','insumo_sin_modificaciones');
            return $query;
        }

    }

} //<!--clase Principal_models-->
