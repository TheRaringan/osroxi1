<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Solicitudes_servicios
 *
 * @author Nelly Moreno
 */
class Solicitudes_servicios_model extends CI_Model
{  

public function cargar_aeropuertos()
    {
    
        $this->db->select('id_aeropuerto, nombre')
                ->from('e_sislogin.aeropuertos');
        $query = $this->db->get();
        
        foreach ($query->result() as $option)
        {
            $data[] = array($option->id_aeropuerto, $option->nombre);
        }
        return $data;

    }
    public function cargar_tipo_vehiculo()
    {
    
        $this->db->select('id_tipo_vehiculo, nombre')
                ->from('e_sislogin.tipo_vehiculo');
        $query = $this->db->get();
        
        foreach ($query->result() as $option)
        {
            $data[] = array($option->id_tipo_vehiculo, $option->nombre);
        }
        return $data;

    }
     
public function cargar_turnos()
    {
    
        $this->db->select('id_turno, nombre')
                ->from('e_sislogin.turnos');
        $query = $this->db->get();
        
        foreach ($query->result() as $option)
        {
            $data[] = array($option->id_turno, $option->nombre);
        }
        return $data;

    } 

    public function cargar_equipaje_especial()
    {
    
        $this->db->select('id_equipaje, nombre')
                ->from('e_sislogin.equipajes');
        $query = $this->db->get();
        
        foreach ($query->result() as $option)
        {
            $data[] = array($option->id_equipaje, $option->nombre);
        }
        return $data;

    }   



public function consultar_correo($aprobador)
    {
    	$consulta_sql = "select
						u.correo,
					from
						e_sislogin.usuarios as u
					where 
						u.cedula= '$aprobador';";
		$query = $this->db->query($consulta_sql); 
    }
public function cargar_id_incluye()
    {
    
        $this->db->select('id_incluye, nombre_incluye')
                ->from('e_sislogin.incluye');
        $query = $this->db->get();
        //echo $this->db->last_query();
        
        foreach ($query->result() as $option)
        {
            $data[] = array($option->id_incluye, $option->nombre_incluye);
        }
        return $data;

    }    

public function cargar_indicadores()
    {
    
        $this->db->select('id_indicador, nombre')
                ->from('e_sislogin.indicadores');
        $query = $this->db->get();
        
        foreach ($query->result() as $option)
        {
            $data[] = array($option->id_indicador, $option->nombre);
        }
        return $data;

    }   

public function cargar_agrupaciones()
    {
    
        $this->db->select('id_agrupacion, nombre')
                ->from('e_sislogin.agrupacion');
        $query = $this->db->get();
        
        foreach ($query->result() as $option)
        {
            $data[] = array($option->id_agrupacion, $option->nombre);
        }
        return $data;

    }   


	 public function listar_solicitudes()
    {
		$consulta_sql = "SELECT 
								s.id_solicitud_servicio as codigo, 
								s.cedula_solicitante,
								p.primer_nombre||' '||p.primer_apellido as nombre_solicitante, 
								s.titulo_evento, 
								o.nombre as indicador, 
									pe.primer_nombre||' '||pe.primer_apellido as aprobador, 
									e.nombre as estatus
							  FROM 
								e_sislogin.solicitudes_servicios as s
							INNER JOIN
								e_sislogin.personas as p
							ON
								s.cedula_solicitante = p.cedula
							INNER JOIN
								e_sislogin.indicadores as o
							on 
								s.orden_interna = o.id_indicador
							INNER JOIN
								e_sislogin.personas as pe
							ON
								s.cedula_aprobador = pe.cedula
							INNER JOIN
								e_sislogin.estatus as e
							ON
								s.id_estatus_servicio = e.id_estatus;"; 
		$query = $this->db->query($consulta_sql); 
		//echo $this->db->last_query();
        if($query ){
	        foreach ($query->result() as $option)
	        {
	            $data[] = array($option->codigo, 
								$option->cedula_solicitante, 
								$option->nombre_solicitante,
								$option->titulo_evento,
								$option->indicador,
								$option->aprobador,
								$option->estatus);
	        }
    	}else {
    		$data[] ="No hay Registros ";
    	}
        return $data;

    }
    
 public function consultar_solicitante()
    {
    	$usuario= $_SESSION['usuario'];
    	//$usuario= 'nmoreno';
    
    	$consulta_sql = "select
						t.cedula as c_solicitante,
						t.cedula_aprobador as c_aprobador,
						p.primer_nombre||' '||p.primer_apellido as nombre_solicitante, 
						pe.primer_nombre||' '||pe.primer_apellido as nombre_aprobador,
						u.celular
					from
						e_sislogin.trabajadores as t
					inner join
						e_sislogin.usuarios as u
					on 
						t.cedula=u.cedula
					left JOIN
						e_sislogin.personas as p
					on 
						t.cedula = p.cedula
					left JOIN
						e_sislogin.personas as pe
					on 
						t.cedula_aprobador = pe.cedula
					where 
						u.usuario= '$usuario';";
		$query = $this->db->query($consulta_sql); 
		//echo $this->db->last_query();
        
        foreach ($query->result() as $option)
        {
            $data[] = array($option->c_solicitante, 
            				$option->c_aprobador, 
							$option->nombre_solicitante, 
							$option->nombre_aprobador,
							$option->celular);
        }
        return $data;
       

    }
   
//transporte

   public function insertar_transporte($tipo_solicitud,
                            $fecha, 
                            $cedula_solicitante,
                            $titulo_evento, 
                            $orden_interna, 
                            $aprobador,  
                            $id_estatus_servicio,
                            
                            $cantidad_persona,
                            $cedula_persona_contacto,
                            $id_tipo_vehiculo,
                            $celular_contacto,
                            $descripcion_servicio,
                            $fecha_salida_t,
                            $hora_salida_t,
                            $lugar_salida_t,
                            $descripcion_salida,
                            $lugar_destino,
                            $descripcion_destino,
                            $fecha_retorno_t,
                            $hora_retorno,
                            $lugar_retorno,
                            $descripcion_retorno, 

                            $celular, 
                            $agrupacion)
    {   
        
       $consulta_sql = "SELECT insertar_transporte(
       													$tipo_solicitud,
														'$fecha', 
														$cedula_solicitante,
            											'$titulo_evento', 
            											$orden_interna, 
            											$aprobador, 
            											$id_estatus_servicio, 
            											
            											$cantidad_persona,
							                            $cedula_persona_contacto,
							                            $id_tipo_vehiculo,
							                            '$celular_contacto',
							                            '$descripcion_servicio',
							                            '$fecha_salida_t',
							                            '$hora_salida_t',
							                            '$lugar_salida_t',
							                            '$descripcion_salida',
							                            '$lugar_destino',
							                            '$descripcion_destino',
							                            '$fecha_retorno_t',
							                            '$hora_retorno',
							                            '$lugar_retorno',
							                            '$descripcion_retorno', 

            											'$celular',
            											$agrupacion

													);
													"; 
													
  

		$result = $this->db->query($consulta_sql);
		
		//echo $celular;
		//echo $this->db->last_query();
		if ($result)
        {
        	$consulta_sql2 = "select
								max(id_solicitud_servicio) as num_solicitud
							from
								e_sislogin.solicitudes_servicios;
													"; 
													
  

			$result2 = $this->db->query($consulta_sql2);
				foreach ($result2->result() as $option)
		        {
		            $solicitud = array($option->num_solicitud);
		        }

			$texto="Se Creó una solicitud de servicio bajo el numero $solicitud[0]";
			//$modem=1;
			//$grupo=0;
			//$opcion_grupo=2;	
			//$tlf='04261073335';
			//$cantidad_modem=1;
			//$modem_vector=array();
			
		 $consulta_sql = "select envio_sms('$texto','modem1','{}',1,0,2,'$celular'); "; 
$query = $this->db->query($consulta_sql); 
			//echo $this->db->last_query();
            return 1001;
        }
        else
        {
            return 1000;
        }
          

        
        

    }                        

//boleteria

   public function insertar_boleteria($tipo_solicitud,
                            $fecha, 
                            $cedula_solicitante,
                            $titulo_evento, 
                            $orden_interna, 
                            $aprobador,  
                            $id_estatus_servicio,
                            
                            $fecha_salida,
                            $id_origen_salida,
                            $id_destino_salida,
                            $fecha_retorno,
                            $id_origen_retorno,
                            $id_destino_retorno,
                            $id_turno,
                            $id_equipaje_especial,

                            $celular, 
                            $agrupacion)
    {   
        
       $consulta_sql = "SELECT insertar_boleteria(
       													$tipo_solicitud,
														'$fecha', 
														$cedula_solicitante,
            											'$titulo_evento', 
            											$orden_interna, 
            											$aprobador, 
            											$id_estatus_servicio,

            											'$fecha_salida',
							                            $id_origen_salida,
							                            $id_destino_salida,
							                            '$fecha_retorno',
							                            $id_origen_retorno,
							                            $id_destino_retorno,
							                            $id_turno,
							                            $id_equipaje_especial,

            											'$celular',
            											$agrupacion

													);
													"; 
													
  

		$result = $this->db->query($consulta_sql);
		
		//echo $celular;
		//echo $this->db->last_query();
		if ($result)
        {
        	$consulta_sql2 = "select
								max(id_solicitud_servicio) as num_solicitud
							from
								e_sislogin.solicitudes_servicios;
													"; 
													
  

			$result2 = $this->db->query($consulta_sql2);
				foreach ($result2->result() as $option)
		        {
		            $solicitud = array($option->num_solicitud);
		        }

			$texto="Se Creó una solicitud de servicio bajo el numero $solicitud[0]";
			//$modem=1;
			//$grupo=0;
			//$opcion_grupo=2;	
			//$tlf='04261073335';
			//$cantidad_modem=1;
			//$modem_vector=array();
			
		 $consulta_sql = "select envio_sms('$texto','modem1','{}',1,0,2,'$celular'); "; 
$query = $this->db->query($consulta_sql); 
			//echo $this->db->last_query();
            return 1001;
        }
        else
        {
            return 1000;
        }
          

        
        

    }
   //hospedaje
 
   
    public function insertar_hospedaje($tipo_solicitud, 
    									$fecha, 
    									$cedula_solicitante,
            							$titulo_evento, 
            							$orden_interna, 
            							$aprobador, 
            							$id_estatus_servicio, 

            							$id_incluye, 
            							$menores_edad, 
            							$ubicacion, 
            							$fecha_entrada_h, 
            							$fecha_salida_h, 

            							$celular, 
            							$agrupacion)
    {   
        
       $consulta_sql = "SELECT insertar_hospedaje(
       													$tipo_solicitud,
														'$fecha', 
														$cedula_solicitante,
            											'$titulo_evento', 
            											$orden_interna, 
            											$aprobador, 
            											$id_estatus_servicio,

            											$id_incluye, 
            											$menores_edad, 
            											'$ubicacion', 
            											'$fecha_entrada_h', 
            											'$fecha_salida_h',

            											'$celular',
            											$agrupacion

													);
													"; 
													
  

		$result = $this->db->query($consulta_sql);
		
		//echo $celular;
		//echo $this->db->last_query();
		if ($result)
        {
        	$consulta_sql2 = "select
								max(id_solicitud_servicio) as num_solicitud
							from
								e_sislogin.solicitudes_servicios;
													"; 
													
  

			$result2 = $this->db->query($consulta_sql2);
				foreach ($result2->result() as $option)
		        {
		            $solicitud = array($option->num_solicitud);
		        }

			$texto="Se Creó una solicitud de servicio bajo el numero $solicitud[0]";
			//$modem=1;
			//$grupo=0;
			//$opcion_grupo=2;	
			//$tlf='04261073335';
			//$cantidad_modem=1;
			//$modem_vector=array();
			
		 $consulta_sql = "select envio_sms('$texto','modem1','{}',1,0,2,'$celular'); "; 
$query = $this->db->query($consulta_sql); 
			//echo $this->db->last_query();
            return 1001;
        }
        else
        {
            return 1000;
        }
          

        
        

    }
//transporte y boleteria

   public function insertar_transporte_boleteria($tipo_solicitud,
                            $fecha, 
                            $cedula_solicitante,
                            $titulo_evento, 
                            $orden_interna, 
                            $aprobador,  
                            $id_estatus_servicio,
                            
                            $cantidad_persona,
                            $cedula_persona_contacto,
                            $id_tipo_vehiculo,
                            $celular_contacto,
                            $descripcion_servicio,
                            $fecha_salida_t,
                            $hora_salida_t,
                            $lugar_salida_t,
                            $descripcion_salida,
                            $lugar_destino,
                            $descripcion_destino,
                            $fecha_retorno_t,
                            $hora_retorno,
                            $lugar_retorno,
                            $descripcion_retorno, 

                            $fecha_salida,
                            $id_origen_salida,
                            $id_destino_salida,
                            $fecha_retorno,
                            $id_origen_retorno,
                            $id_destino_retorno,
                            $id_turno,
                            $id_equipaje_especial,

                            $celular, 
                            $agrupacion)
    {   
        
       $consulta_sql = "SELECT insertar_transporte_boleteria(
       													$tipo_solicitud,
														'$fecha', 
														$cedula_solicitante,
            											'$titulo_evento', 
            											$orden_interna, 
            											$aprobador, 
            											$id_estatus_servicio, 
            											
            											$cantidad_persona,
							                            $cedula_persona_contacto,
							                            $id_tipo_vehiculo,
							                            '$celular_contacto',
							                            '$descripcion_servicio',
							                            '$fecha_salida_t',
							                            '$hora_salida_t',
							                            '$lugar_salida_t',
							                            '$descripcion_salida',
							                            '$lugar_destino',
							                            '$descripcion_destino',
							                            '$fecha_retorno_t',
							                            '$hora_retorno',
							                            '$lugar_retorno',
							                            '$descripcion_retorno',

							                            '$fecha_salida',
							                            $id_origen_salida,
							                            $id_destino_salida,
							                            '$fecha_retorno',
							                            $id_origen_retorno,
							                            $id_destino_retorno,
							                            $id_turno,
							                            $id_equipaje_especial, 

            											'$celular',
            											$agrupacion

													);
													"; 
													
  

		$result = $this->db->query($consulta_sql);
		
		//echo $celular;
		//echo $this->db->last_query();
		if ($result)
        {
        	$consulta_sql2 = "select
								max(id_solicitud_servicio) as num_solicitud
							from
								e_sislogin.solicitudes_servicios;
													"; 
													
  

			$result2 = $this->db->query($consulta_sql2);
				foreach ($result2->result() as $option)
		        {
		            $solicitud = array($option->num_solicitud);
		        }

			$texto="Se Creó una solicitud de servicio bajo el numero $solicitud[0]";
			//$modem=1;
			//$grupo=0;
			//$opcion_grupo=2;	
			//$tlf='04261073335';
			//$cantidad_modem=1;
			//$modem_vector=array();
			
		 $consulta_sql = "select envio_sms('$texto','modem1','{}',1,0,2,'$celular'); "; 
$query = $this->db->query($consulta_sql); 
			//echo $this->db->last_query();
            return 1001;
        }
        else
        {
            return 1000;
        }
          

        
        

    }        

//hospedaje y transporte
    public function insertar_hospedaje_transporte($tipo_solicitud,
                            $fecha, 
                            $cedula_solicitante,
                            $titulo_evento, 
                            $orden_interna, 
                            $aprobador,  
                            $id_estatus_servicio,
                            
                            $cantidad_persona,
                            $cedula_persona_contacto,
                            $id_tipo_vehiculo,
                            $celular_contacto,
                            $descripcion_servicio,
                            $fecha_salida_t,
                            $hora_salida_t,
                            $lugar_salida_t,
                            $descripcion_salida,
                            $lugar_destino,
                            $descripcion_destino,
                            $fecha_retorno_t,
                            $hora_retorno,
                            $lugar_retorno,
                            $descripcion_retorno, 

							$id_incluye, 
            				$menores_edad, 
            				$ubicacion, 
            				$fecha_entrada_h, 
            				$fecha_salida_h,                             

                            $celular, 
                            $agrupacion)
    {   
        
       $consulta_sql = "SELECT insertar_hospedaje_transporte(
       													$tipo_solicitud,
														'$fecha', 
														$cedula_solicitante,
            											'$titulo_evento', 
            											$orden_interna, 
            											$aprobador, 
            											$id_estatus_servicio, 
            											
            											$cantidad_persona,
							                            $cedula_persona_contacto,
							                            $id_tipo_vehiculo,
							                            '$celular_contacto',
							                            '$descripcion_servicio',
							                            '$fecha_salida_t',
							                            '$hora_salida_t',
							                            '$lugar_salida_t',
							                            '$descripcion_salida',
							                            '$lugar_destino',
							                            '$descripcion_destino',
							                            '$fecha_retorno_t',
							                            '$hora_retorno',
							                            '$lugar_retorno',
							                            '$descripcion_retorno', 

							                            $id_incluye, 
            											$menores_edad, 
            											'$ubicacion', 
            											'$fecha_entrada_h', 
            											'$fecha_salida_h',

            											'$celular',
            											$agrupacion

													);
													"; 
													
  

		$result = $this->db->query($consulta_sql);
		
		//echo $celular;
		//echo $this->db->last_query();
		if ($result)
        {
        	$consulta_sql2 = "select
								max(id_solicitud_servicio) as num_solicitud
							from
								e_sislogin.solicitudes_servicios;
													"; 
													
  

			$result2 = $this->db->query($consulta_sql2);
				foreach ($result2->result() as $option)
		        {
		            $solicitud = array($option->num_solicitud);
		        }

			$texto="Se Creó una solicitud de servicio bajo el numero $solicitud[0]";
			//$modem=1;
			//$grupo=0;
			//$opcion_grupo=2;	
			//$tlf='04261073335';
			//$cantidad_modem=1;
			//$modem_vector=array();
			
		 $consulta_sql = "select envio_sms('$texto','modem1','{}',1,0,2,'$celular'); "; 


$query = $this->db->query($consulta_sql); 
			//echo $this->db->last_query();
            return 1001;
        }
        else
        {
            return 1000;
        }
          

        
        

    }                        

    
 //hospedaje y boleteria                          
public function insertar_hospedaje_boleteria(
							$tipo_solicitud, 
							$fecha, 
							$cedula_solicitante,
            				$titulo_evento, 
            				$orden_interna, 
            				$aprobador, 
            				$id_estatus_servicio, 

            				$fecha_salida,
                            $id_origen_salida,
                            $id_destino_salida,
                            $fecha_retorno,
                            $id_origen_retorno,
                            $id_destino_retorno,
                            $id_turno,
                            $id_equipaje_especial,


            				$id_incluye, 
            				$menores_edad, 
            				$ubicacion, 
            				$fecha_entrada_h, 
            				$fecha_salida_h, 

            				$celular, 
            				$agrupacion)
    {   
        
       $consulta_sql = "SELECT insertar_hospedaje_boleteria(
       													$tipo_solicitud,
														'$fecha', 
														$cedula_solicitante,
            											'$titulo_evento', 
            											$orden_interna, 
            											$aprobador, 
            											$id_estatus_servicio, 

            											'$fecha_salida',
							                            $id_origen_salida,
							                            $id_destino_salida,
							                            '$fecha_retorno',
							                            $id_origen_retorno,
							                            $id_destino_retorno,
							                            $id_turno,
							                            $id_equipaje_especial,

            											$id_incluye, 
            											$menores_edad, 
            											'$ubicacion', 
            											'$fecha_entrada_h', 
            											'$fecha_salida_h',

            											'$celular',
            											$agrupacion

													);
													"; 
													
  

		$result = $this->db->query($consulta_sql);
		
		//echo $celular;
		//echo $this->db->last_query();
		if ($result)
        {
        	$consulta_sql2 = "select
								max(id_solicitud_servicio) as num_solicitud
							from
								e_sislogin.solicitudes_servicios;
													"; 
													
  

			$result2 = $this->db->query($consulta_sql2);
				foreach ($result2->result() as $option)
		        {
		            $solicitud = array($option->num_solicitud);
		        }

			$texto="Se Creó una solicitud de servicio bajo el numero $solicitud[0]";
			//$modem=1;
			//$grupo=0;
			//$opcion_grupo=2;	
			//$tlf='04261073335';
			//$cantidad_modem=1;
			//$modem_vector=array();
			
		 $consulta_sql = "select envio_sms('$texto','modem1','{}',1,0,2,'$celular'); "; 
$query = $this->db->query($consulta_sql); 
			//echo $this->db->last_query();
            return 1001;
        }
        else
        {
            return 1000;
        }
          

        
        

    }

    //insertar_transporte_boleteria_hospedaje
    public function insertar_transporte_boleteria_hospedaje($tipo_solicitud,
                            $fecha, 
                            $cedula_solicitante,
                            $titulo_evento, 
                            $orden_interna, 
                            $aprobador,  
                            $id_estatus_servicio,
                            
                            $cantidad_persona,
                            $cedula_persona_contacto,
                            $id_tipo_vehiculo,
                            $celular_contacto,
                            $descripcion_servicio,
                            $fecha_salida_t,
                            $hora_salida_t,
                            $lugar_salida_t,
                            $descripcion_salida,
                            $lugar_destino,
                            $descripcion_destino,
                            $fecha_retorno_t,
                            $hora_retorno,
                            $lugar_retorno,
                            $descripcion_retorno, 

                            $fecha_salida,
                            $id_origen_salida,
                            $id_destino_salida,
                            $fecha_retorno,
                            $id_origen_retorno,
                            $id_destino_retorno,
                            $id_turno,
                            $id_equipaje_especial,

                            $id_incluye, 
            				$menores_edad, 
            				$ubicacion, 
            				$fecha_entrada_h, 
            				$fecha_salida_h,

                            $celular, 
                            $agrupacion)
    {   
        
       $consulta_sql = "SELECT insertar_transporte_boleteria_hospedaje(
       													$tipo_solicitud,
														'$fecha', 
														$cedula_solicitante,
            											'$titulo_evento', 
            											$orden_interna, 
            											$aprobador, 
            											$id_estatus_servicio, 
            											
            											$cantidad_persona,
							                            $cedula_persona_contacto,
							                            $id_tipo_vehiculo,
							                            '$celular_contacto',
							                            '$descripcion_servicio',
							                            '$fecha_salida_t',
							                            '$hora_salida_t',
							                            '$lugar_salida_t',
							                            '$descripcion_salida',
							                            '$lugar_destino',
							                            '$descripcion_destino',
							                            '$fecha_retorno_t',
							                            '$hora_retorno',
							                            '$lugar_retorno',
							                            '$descripcion_retorno',

							                            '$fecha_salida',
							                            $id_origen_salida,
							                            $id_destino_salida,
							                            '$fecha_retorno',
							                            $id_origen_retorno,
							                            $id_destino_retorno,
							                            $id_turno,
							                            $id_equipaje_especial, 

							                            $id_incluye, 
            											$menores_edad, 
            											'$ubicacion', 
            											'$fecha_entrada_h', 
            											'$fecha_salida_h',

            											'$celular',
            											$agrupacion

													);
													"; 
													
  

		$result = $this->db->query($consulta_sql);
		
		//echo $celular;
		//echo $this->db->last_query();
		if ($result)
        {
        	$consulta_sql2 = "select
								max(id_solicitud_servicio) as num_solicitud
							from
								e_sislogin.solicitudes_servicios;
													"; 
													
  

			$result2 = $this->db->query($consulta_sql2);
				foreach ($result2->result() as $option)
		        {
		            $solicitud = array($option->num_solicitud);
		        }

			$texto="Se Creó una solicitud de servicio bajo el numero $solicitud[0]";
			//$modem=1;
			//$grupo=0;
			//$opcion_grupo=2;	
			//$tlf='04261073335';
			//$cantidad_modem=1;
			//$modem_vector=array();
			
		 $consulta_sql = "select envio_sms('$texto','modem1','{}',1,0,2,'$celular'); "; 
$query = $this->db->query($consulta_sql); 
			//echo $this->db->last_query();
            return 1001;
        }
        else
        {
            return 1000;
        }
          

        
        

    }        
}

