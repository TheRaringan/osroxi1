<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Personas_model
 *
 * @author Nelly Moreno & Albert Torres
 */
class Ficha_upsa_model extends CI_Model{


	public function ficha_upsa(){
		$consulta_sql= "SELECT upsa.id_upsa,
								upsa.nombre as nombre_upsa,
						       est.descripcion as estado
						FROM UPSA
						LEFT JOIN coordinador as cor ON cor.id_upsa = upsa.id_upsa
						LEFT JOIN ubicacion as ubi ON ubi.id_upsa = upsa.id_upsa
						LEFT JOIN estado as est ON est.id_estado = ubi.id_estado
						where upsa.id_upsa != '0'";

		$query = $this->db->query($consulta_sql);

		if($query->result() != NULL){
			foreach ($query->result() as $option){
				$data[] = array($option->id_upsa,
								$option->nombre_upsa,
								$option->estado
							);
			 }
			return $data;
		}else{
			$data = FALSE;
			return $data;
		}
	}

	public function ficha_upsa_2($postData){
		/*$this->db->select('upsa.id_upsa as id_upsa,
                            upsa.nombre as nombre_upsa,
                            estado.descripcion as estado,
							municipio.descripcion as municipio,
							parroquia.descripcion as parroquia,
							relacion_corpo.nombre as relacion,
							coordinador.nombre as responsable,
							coordinador.apellido as apellido,
							coordinador.cedula as cedula,
							coordinador.tlf_movil as telefono,
							capacidad_operativa.total_hectareas as superficie_total,
							capacidad_operativa.superficie_aprovechable_total as superficie_utilizable,
							capacidad_operativa.superficie_operativa_agricola as operativa_agri,
							capacidad_operativa.superficie_operativa_pecuaria as operativa_pec,
							array_to_string(array_agg(actividad_econ_upsa.id_tipo_actividad_econ), ',') as act
							');
            $this->db->join('ubicacion','ubicacion.id_upsa = upsa.id_upsa','left');
            $this->db->join('estado','estado.id_estado = ubicacion.id_estado','left');
			$this->db->join('municipio','municipio.id_estado = estado.id_estado','left');
			$this->db->join('parroquia','parroquia.id_municipio = municipio.id_municipio','left');
			$this->db->join('relacion_corpo','relacion_corpo.id_relacion_corpo = upsa.id_relacion_corpo','left');
			$this->db->join('coordinador','coordinador.id_upsa = upsa.id_upsa','left');
			$this->db->join('capacidad_operativa','capacidad_operativa.id_upsa = upsa.id_upsa','left');
			$this->db->join('actividad_econ_upsa','actividad_econ_upsa.id_upsa = upsa.id_upsa','left');
			$this->db->where('upsa.id_upsa',$postData['this_consulta']);
            $query = $this->db->get('upsa');
			//var_dump($query->result_array());die;
            return $query->result_array();*/

			$consulta_sql= "SELECT 	upsa.id_upsa,
									upsa.nombre as nombre_upsa,
									r.descripcion as region,
									e.descripcion as estado,
									m.descripcion as municipio,
									p.descripcion as parroquia,
									rc.descripcion as relacion,
									c.cedula,
									CONCAT (c.nombre,' ',c.apellido) AS responsable,
									c.tlf_movil,
									co.total_hectareas,
									co.superficie_aprovechable_total,
									co.superficie_operativa_agricola,
									co.superficie_operativa_pecuaria,
									array_to_string(array_agg(DISTINCT tact.descripcion), ', ') as tipo_act,
									array_to_string(array_agg(DISTINCT trh.descripcion), ', ') as tipo_recur_hid,
									array_to_string(array_agg(DISTINCT rhd.descripcion), ', ') as tipo_recurso,
									act.id_laboral as laboral
								FROM UPSA
									LEFT JOIN ubicacion as u ON u.id_upsa = upsa.id_upsa
									LEFT JOIN region as r ON r.id_region = u.id_region
									LEFT JOIN estado as e ON e.id_estado = u.id_estado
									LEFT JOIN municipio as m ON m.id_municipio = u.id_municipio
									LEFT JOIN parroquia as p ON p.id_parroquia = u.id_parroquia
									LEFT JOIN relacion_corpo as rc ON rc.id_relacion_corpo = upsa.id_relacion_corpo
									LEFT JOIN coordinador as c ON c.id_upsa = upsa.id_upsa
									LEFT JOIN capacidad_operativa as co ON co.id_upsa = upsa.id_upsa
									LEFT JOIN actividad_econ_upsa AS act ON act.id_upsa = upsa.id_upsa
									LEFT JOIN tipo_actividad_econ AS tact ON tact.id_tipo_actividad_econ = act.id_tipo_actividad_econ
									LEFT JOIN recursos_hidr_upsa AS rh ON rh.id_upsa = upsa.id_upsa
									LEFT JOIN tipo_recurso_hidr AS trh ON trh.id_tipo_recurso_hidr = rh.id_tipo_recurso_hidr
									LEFT JOIN recurso_hidr AS rhd ON rhd.id_recurso_hidr = rh.id_recurso_hidr
							where upsa.id_upsa = '".$postData['this_consulta']."'
							GROUP BY upsa.id_upsa, r.descripcion,
									 e.descripcion, m.descripcion,
									 p.descripcion, rc.descripcion,
									 c.cedula,c.nombre,
									 c.apellido,c.tlf_movil,
									 co.total_hectareas, co.superficie_aprovechable_total,
									co.superficie_operativa_agricola,
									co.superficie_operativa_pecuaria, act.id_laboral";

			$query = $this->db->query($consulta_sql);
			//echo($query);die;

			if($query->result() != NULL){
				foreach ($query->result() as $option){
					$data[] = array($option->nombre_upsa,//0
									$option->region,//1
									$option->estado,//2
									$option->municipio,//3
									$option->parroquia,//4
									$option->relacion,//5
									$option->cedula,//6
									$option->responsable,//7
									$option->tlf_movil,//8
									$option->total_hectareas,//9
									$option->superficie_aprovechable_total,//10
									$option->superficie_operativa_agricola,//11
									$option->superficie_operativa_pecuaria,//12
									$option->tipo_act,//13
									$option->tipo_recur_hid,//14
									$option->tipo_recurso,//15
									$option->laboral//15
								   );
								   //print_r ($data);die;
				 }
				return $data;
			}else{
				$data = FALSE;
				return $data;
			}

			//$query = $this->db->query($consulta_sql);
	}


} //<!--clase Personas_model-->
