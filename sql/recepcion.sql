﻿DROP TABLE nota_recepcion_x_insumos

DROP TABLE public.nota_recepcion;

CREATE TABLE public.nota_recepcion
(
  id_nota_recepcion SERIAL,
  nota_recepcion character varying(25),
  id_ca integer,
  fecha date DEFAULT now(),
  id_transportista integer,
  CONSTRAINT nota_recepcion_pkey PRIMARY KEY (id_nota_recepcion),
  CONSTRAINT nota_recepcion_id_ca_fkey FOREIGN KEY (id_ca)
      REFERENCES public.centros_acopio (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT nota_recepcion_id_transportista_fkey FOREIGN KEY (id_transportista)
      REFERENCES public.transportistas (id_transportista) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)

CREATE TABLE public.nota_recepcion_x_insumos
(
  id_nr_x_ins SERIAL,
  id_nota_recepcion integer,
  id_catalogo integer,
  CONSTRAINT nota_recepcion_x_insumos_pkey PRIMARY KEY (id_nr_x_ins),
  CONSTRAINT nota_recepcion_x_insumos_id_catago_fkey FOREIGN KEY (id_catalogo)
      REFERENCES public.catalogo_insumos (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT nota_recepcion_x_insumos_id_nota_recepcion_fkey FOREIGN KEY (id_nota_recepcion)
      REFERENCES public.nota_recepcion (id_nota_recepcion) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)

