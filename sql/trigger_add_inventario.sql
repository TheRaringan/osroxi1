﻿------------------------------------------------------------------------------------------
DROP TABLE public.carga_insumos CASCADE;

CREATE TABLE public.carga_insumos
(
  id SERIAL,
  nro_factura integer,
  id_catalogo integer,
  fecha_registro timestamp(0) without time zone DEFAULT now(),
  cantidad integer,
  peso_cantidad numeric(8,2),
  CONSTRAINT carga_insumos_pkey PRIMARY KEY (id),
  CONSTRAINT carga_insumos_id_catalogo_fkey FOREIGN KEY (id_catalogo)
      REFERENCES public.catalogo_insumos (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

-------------------------------------------------------------------------------------------
DROP TABLE public.insumos CASCADE;

CREATE TABLE public.insumos
(
  id SERIAL,
  id_tipo_insumo integer,
  id_subtipo_insumo integer,
  id_presentacion integer,
  name character varying(255),
  CONSTRAINT insumos_pkey PRIMARY KEY (id),
  CONSTRAINT insumos_id_subtipo_insumo_fkey FOREIGN KEY (id_subtipo_insumo)
      REFERENCES public.subtipos_insumo (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT insumos_id_tipo_insumo_fkey FOREIGN KEY (id_tipo_insumo)
      REFERENCES public.tipos_insumo (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
);
-------------------------------------------------------------------------------------------
DROP TABLE public.catalogo_insumos CASCADE;
CREATE TABLE public.catalogo_insumos
(
  id SERIAL,
  id_tipo_insumo integer,
  id_subtipo_insumo integer,
  id_insumo integer,
  presentacion character varying(255),
  id_tipo_presentacion integer,
  peso_unidad numeric(8,2),
  CONSTRAINT catalogo_insumos_pkey PRIMARY KEY (id),
  CONSTRAINT catalogo_insumos_id_insumo_fkey FOREIGN KEY (id_insumo)
      REFERENCES public.insumos (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT catalogo_insumos_id_subtipo_insumo_fkey FOREIGN KEY (id_subtipo_insumo)
      REFERENCES public.subtipos_insumo (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT catalogo_insumos_id_tipo_insumo_fkey FOREIGN KEY (id_tipo_insumo)
      REFERENCES public.tipos_insumo (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT catalogo_insumos_id_tipo_presentacion_fkey FOREIGN KEY (id_tipo_presentacion)
      REFERENCES public.tipo_presentaciones (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);
---------------------------------------------------------------------------------------------
DROP TABLE public.inventario;

CREATE TABLE public.inventario
(
  id SERIAL,
  id_catalogo integer,
  cantidad integer,
  peso numeric(8,2),
  CONSTRAINT inventario_pkey PRIMARY KEY (id),
  CONSTRAINT inventario_id_catalogo_fkey FOREIGN KEY (id_catalogo)
      REFERENCES public.catalogo_insumos (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);
---------------------------------------------------------------------------------------------
DROP TABLE public.insumos_factura CASCADE;

CREATE TABLE public.insumos_factura
(
  id SERIAL,
  id_catalogo integer,
  id_factura integer,
  cantidad integer,
  peso_cantidad numeric(8,2),
  CONSTRAINT insumos_factura_pkey PRIMARY KEY (id),
  CONSTRAINT insumos_factura_id_factura_fkey FOREIGN KEY (id_factura)
      REFERENCES public.facturas (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

DROP TABLE public.facturas CASCADE;

CREATE TABLE public.facturas
(
  id SERIAL,
  nro_factura integer,
  fecha_registro timestamp(0) without time zone DEFAULT now(),
  id_proveedor INT,
  CONSTRAINT facturas_pkey PRIMARY KEY (id),
  FOREIGN KEY(id_proveedor) REFERENCES proveedores(id)
);
---------------------------------------------------------------------------------------------

DROP TABLE asignaciones_ca CASCADE;
CREATE TABLE asignaciones_ca(
	id SERIAL,
	nro_asignacion INT,
	id_ca INT,
	fecha_asignacion TIMESTAMP(0) DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY(id),
	FOREIGN KEY(id_ca) REFERENCES centros_acopio(id)
);

DROP TABLE asignaciones_insumos;
CREATE TABLE asignaciones_insumos(
	id SERIAL,
	id_asignacion INT,
	id_catalogo INT,
	cantidad INT,
	peso_cantidad NUMERIC(8,2),
	PRIMARY KEY(id),
	FOREIGN KEY(id_asignacion) REFERENCES asignaciones_ca(id),
	FOREIGN KEY(id_catalogo) REFERENCES catalogo_insumos(id)
);

---------------------------------------------------------------------------------------------
DROP FUNCTION public.sumar_al_inventario() CASCADE;
CREATE OR REPLACE FUNCTION sumar_al_inventario()
RETURNS TRIGGER AS
$$
BEGIN
	UPDATE inventario
	SET	cantidad = cantidad + NEW.cantidad,
		peso = peso + NEW.peso_cantidad

	WHERE id_catalogo = NEW.id_catalogo;

	RETURN NEW;
END;
$$
LANGUAGE 'plpgsql';

CREATE TRIGGER after_factura
	AFTER INSERT
	ON insumos_factura
	FOR EACH ROW
	EXECUTE PROCEDURE sumar_al_inventario();
---------------------------------------------------------------------------------------------
DROP FUNCTION public.restar_al_inventario() CASCADE;
CREATE OR REPLACE FUNCTION restar_al_inventario()
RETURNS TRIGGER AS
$$
BEGIN
	UPDATE inventario
	SET cantidad = cantidad - NEW.cantidad,
	    peso = peso - NEW.peso_cantidad
	WHERE id_catalogo = NEW.id_catalogo;

	RETURN NEW;
END;
$$
LANGUAGE 'plpgsql';

CREATE TRIGGER after_asignacion
	AFTER INSERT
	ON asignaciones_insumos
	FOR EACH ROW
	EXECUTE PROCEDURE restar_al_inventario();

-----------------------------------------------------
DROP FUNCTION public.insertar_al_inventario() CASCADE;
CREATE OR REPLACE FUNCTION insertar_al_inventario()
RETURNS TRIGGER AS
$$
BEGIN
	INSERT INTO inventario(id_catalogo,cantidad,peso)
	VALUES	(NEW.id,0,0);

	RETURN NEW;
END;
$$
LANGUAGE 'plpgsql';

CREATE TRIGGER after_catalogo_insumo
	AFTER INSERT
	ON catalogo_insumos
	FOR EACH ROW
	EXECUTE PROCEDURE insertar_al_inventario();
------------------------------------------------------


CREATE OR REPLACE FUNCTION insert_inventario_ca()
RETURNS TRIGGER AS
$$
BEGIN
	INSERT INTO inventario_ca(id_catalogo, id_ca, cantidad, peso)
	VALUES	(NEW.id_catalogo,select,0,0);

	RETURN NEW;
END;
$$
LANGUAGE 'plpgsql';

CREATE TRIGGER after_nota_recepcion
	AFTER INSERT
	ON catalogo_insumos and centros_acopio
	FOR EACH ROW
	EXECUTE PROCEDURE insert_inventario_ca();

    -- ----------------------------------------------------
    -- DROP FUNCTION public.restar_al_inventario() CASCADE;
CREATE OR REPLACE FUNCTION update_asignacion()
RETURNS TRIGGER AS
$$
BEGIN
	UPDATE asignaciones_ca
	SET id_status = 4
	WHERE id = NEW.id;

	RETURN NEW;
END;
$$
LANGUAGE 'plpgsql';

CREATE TRIGGER after_asignacion
	AFTER INSERT
	ON nota_recepcion
	FOR EACH ROW
	EXECUTE PROCEDURE update_asignacion();
