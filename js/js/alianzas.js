$(document).ready(function(){
	/*$(".boton").click(function(){

		var valores="";

		// Obtenemos todos los valores contenidos en los <td> de la fila
		// seleccionada
		$(this).parents("tr").find("td").each(function(){
			valores+=$(this).html()+"\n";
		});

		alert(valores);
	});*/

	$('.alianza').click(function(){
        var consulta = $(this).data('value');
        console.log('consulta = '+consulta);
        // AJAX request
         $.ajax({
            url:base_url+'alianza/listar',
            method: 'post',
            data: {this_consulta: consulta},
            dataType: 'json',
            success: function(response){
                console.log(response);
                //$('#asignacion-label').text('INSUMOS DE ASIGNACION #'+asignacion);
				$('#estado1').html('');
                $.each(response,function(index,data){
					$('#nombre_upsa').val(data['0']);
					$('#nombre_alianza').val(data['1']);
					$('#responsable').val(data['2']);
					$('#rif').val(data['3']);
					$('#telefono').val(data['4']);
					$('#direccion').val(data['5']);
					$('#correo').val(data['6']);

					$('#estado').val(data['7']);
					$('#municipio').val(data['8']);
					$('#parroquia').val(data['9']);
					$('#latitud').val(data['10']);
					$('#longitud').val(data['11']);

					$('#objeto').val(data['12']);
					$('#aportes').val(data['13']);
					$('#fecha_suscripcion').val(data['14']);
					$('#duracion').val(data['15']);
					$('#distribucion_utilidad').val(data['16']);
					$('#distribucion_produccion').val(data['17']);
					$('#bienes_asignados').val(data['18']);

					$('#proyecto').val(data['19']);
					$('#fecha_siembra').val(data['20']);
					$('#tiempo_cosecha').val(data['21']);
					$('#has_sembradas').val(data['22']);
					$('#has_cosechadas').val(data['23']);
					$('#fecha_cosecha').val(data['24']);
					$('#peso').val(data['25']);
					$('#silo').val(data['26']);

					$('#nombre_contacto').val(data['27']);
					$('#cedula_contacto').val(data['28']);
					$('#telefono_contacto').val(data['29']);
					$('#fecha_contacto').val(data['30']);
					$('#observaciones').val(data['31']);
                });
            }
        });
	});
	$('.alianza_modificar').click(function(){
        var consulta = $(this).data('value');
        console.log('consulta = '+consulta);
        // AJAX request
         $.ajax({
            url:base_url+'alianza/listar',
            method: 'post',
            data: {this_consulta: consulta},
            dataType: 'json',
            success: function(response){
                console.log(response);
                //$('#asignacion-label').text('INSUMOS DE ASIGNACION #'+asignacion);
				$('#estado1').html('');
                $.each(response,function(index,data){
					$('#nombre_upsa_mod').val(data['0']);
					$('#nombre_alianza_mod').val(data['1']);
					$('#responsable_mod').val(data['2']);
					$('#rif_mod').val(data['3']);
					$('#telefono_mod').val(data['4']);
					$('#direccion_mod').val(data['5']);
					$('#correo_mod').val(data['6']);

					$('#estado_mod').val(data['7']);
					$('#municipio_mod').val(data['8']);
					$('#parroquia_mod').val(data['9']);
					$('#latitud_mod').val(data['10']);
					$('#longitud_mod').val(data['11']);

					$('#objeto_mod').val(data['12']);
					$('#aportes_mod').val(data['13']);
					$('#fecha_suscripcion_mod').val(data['14']);
					$('#duracion_mod').val(data['15']);
					$('#distribucion_utilidad_mod').val(data['16']);
					$('#distribucion_produccion_mod').val(data['17']);
					$('#bienes_asignados_mod').val(data['18']);

					$('#proyecto_mod').val(data['19']);
					$('#fecha_siembra_mod').val(data['20']);
					$('#tiempo_cosecha_mod').val(data['21']);
					$('#has_sembradas_mod').val(data['22']);
					$('#has_cosechadas_mod').val(data['23']);
					$('#fecha_cosecha_mod').val(data['24']);
					$('#peso_mod').val(data['25']);
					$('#silo_mod').val(data['26']);

					$('#nombre_contacto_mod').val(data['27']);
					$('#cedula_contacto_mod').val(data['28']);
					$('#telefono_contacto_mod').val(data['29']);
//					$('#fecha_contacto_mod').val(data['30']);
					$('#observaciones_mod').val(data['31']);
					$('#id_alianza').val(consulta);
                });
            }
        });
	});
});
