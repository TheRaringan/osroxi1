$(document).ready(function() {
  console.log($('#myPnotify'));
  if ($("#myPnotify")) {
    pnoti = $('#myPnotify').val();
    console.log(pnoti);
    switch (pnoti) {
      case "logout":
        console.log('fuaaaaaa');
        pnotify = new PNotify({
          title: "Has Cerrado Sesión!",
          type: "error",
          styling: "bootstrap3"
        });
        setTimeout(() => {
          pnotify.remove();
          $('#myPnotify').remove();
        }, 3500);
        break;

      case "insert":
        pnotify = new PNotify({
          title: "Registro Exitoso!",
          type: "success",
          styling: "bootstrap3"
        });
        setTimeout(() => {
          pnotify.remove();
          $('#myPnotify').remove();
        }, 3500);
        break;

      case "alianza_mod":
        pnotify = new PNotify({
          title: "Alianza Modificada",
          type: "success",
          styling: "bootstrap3"
        });
        setTimeout(() => {
          pnotify.remove();
          $('#myPnotify').remove();
        }, 3500);
        break;

      case "activo":
        pnotify = new PNotify({
          title: "Estatus Modificado: ACTIVO",
          type: "success",
          styling: "bootstrap3"
        });
        setTimeout(() => {
          pnotify.remove();
          $('#myPnotify').remove();
        }, 3500);
        break;

      case "inactivo":
        pnotify = new PNotify({
          title: "Estatus Modificado: INACTIVO",
          type: "success",
          styling: "bootstrap3"
        });
        setTimeout(() => {
          pnotify.remove();
          $('#myPnotify').remove();
        }, 3500);
        break;

      case "login":
        pnotify = new PNotify({
          title: "Has iniciado Sesión!",
          type: "success",
          styling: "bootstrap3"
        });
        setTimeout(() => {
          pnotify.remove();
          $('#myPnotify').remove();
        }, 3500);
        break;

        case "rubro_modificado":
        pnotify = new PNotify({
          title: "Rubro modificado con éxito.",
          type: "success",
          styling: "bootstrap3"
        });
        setTimeout(() => {
          pnotify.remove();
          $('#myPnotify').remove();
        }, 3500);
        break;

        case "rubro_agregado":
        pnotify = new PNotify({
          title: "Rubro agregado con éxito.",
          type: "success",
          styling: "bootstrap3"
        });
        setTimeout(() => {
          pnotify.remove();
          $('#myPnotify').remove();
        }, 3500);
        break;

        case "rubro_sin_modificaciones":
        pnotify = new PNotify({
          title: "No se detectaron cambios en el rubro.",
          type: "warning",
          styling: "bootstrap3"
        });
        setTimeout(() => {
          pnotify.remove();
          $('#myPnotify').remove();
        }, 3500);
        break;

        case "insumo_sin_modificaciones":
        pnotify = new PNotify({
          title: "No se detectaron cambios en el insumo.",
          type: "warning",
          styling: "bootstrap3"
        });
        setTimeout(() => {
          pnotify.remove();
          $('#myPnotify').remove();
        }, 3500);
        break;

        case "insumo_modificado":
        pnotify = new PNotify({
          title: "Insumo modificado con éxito.",
          type: "success",
          styling: "bootstrap3"
        });
        setTimeout(() => {
          pnotify.remove();
          $('#myPnotify').remove();
        }, 3500);
        break;


 	case "incorrecto":
          pnotify = new PNotify({
            title: "Usuario o Contraseña Incorrecta!",
            type: "warning",
            styling: "bootstrap3"
          });
          setTimeout(() => {
            pnotify.remove();
            $('#myPnotify').remove();
          }, 3500);
          break;

    case "repetido1":
        pnotify = new PNotify({
          title: "El Recurso Hídrico ya se encuentra Registrado!",
          type: "warning",
          styling: "bootstrap3"
        });
        setTimeout(() => {
          pnotify.remove();
          $('#myPnotify').remove();
        }, 3500);
    break;

    case "repetido2":
        pnotify = new PNotify({
          title: "El usuario ya se encuentra Registrado!",
          type: "warning",
          styling: "bootstrap3"
        });
        setTimeout(() => {
          pnotify.remove();
          $('#myPnotify').remove();
        }, 3500);
    break;

    case "repetido2":
        pnotify = new PNotify({
          title: "La actividad Economica ya se encuentra Registrado!",
          type: "warning",
          styling: "bootstrap3"
        });
        setTimeout(() => {
          pnotify.remove();
          $('#myPnotify').remove();
        }, 3500);
    break;

      case "captcha":
        pnotify = new PNotify({
          title: "Por favor ingrese correctamente los datos del Captcha",
          type: "warning",
          styling: "bootstrap3"
        });
        setTimeout(() => {
          pnotify.remove();
          $('#myPnotify').remove();
        }, 3500);
        break;
        case "exists":
          pnotify = new PNotify({
            title: "Registro ya Existe",
            type: "warning",
            styling: "bootstrap3"
          });
          setTimeout(() => {
            pnotify.remove();
            $('#myPnotify').remove();
          }, 3500);
          break;
        
        case "fail":
          pnotify = new PNotify({
            title: "Falla al Registrar",
            type: "warning",
            styling: "bootstrap3"
          });
          setTimeout(() => {
            pnotify.remove();
            $('#myPnotify').remove();
          }, 3500);
        break;
        
        case "alert":
          pnotify = new PNotify({
            title: "Debe Ingresar una region",
            type: "warning",
            styling: "bootstrap3"
          });
          setTimeout(() => {
            pnotify.remove();
            $('#myPnotify').remove();
          }, 3500);
          break;
    }
  }else{
    console.log('pnoti not found');
  }
});
