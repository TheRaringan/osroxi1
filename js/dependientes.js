
$(document).ready(function(){
	//################################## INFO DE RECURSOS HIDRICOS ############################################
	//Cambio en Tipo
	$('#s1').change(function(){
			var tipo_recurso = $(this).val();
			var tipo_seleccionado = $('#s1 option:selected').text().trim();
			// AJAX request
			 $.ajax({
					url:base_url+'consultas_ajax/get_recursos_hidricos',
					method: 'post',
					data: {this_tipo_recurso: tipo_recurso},
					dataType: 'json',
					success: function(response){
							// Remove options
							$('#s2').find('option').not(':first').remove();
							// Add options
							$.each(response,function(index,data){
								$('#s2').append('<option id="recurso-'+data['id_recurso_hidr']+'" value="'+data['id_recurso_hidr']+'">'+data['nombre']+'</option>');
								$('#myTable > tr').each(function(){
									var recurso = $(this).find('td').eq(1).text();
									var tipo = $(this).find('td').eq(0).text();
									if(recurso == data['nombre'] && tipo == tipo_seleccionado){
										$('#s2 option[value="'+data['id_recurso_hidr']+'"]').remove();
									}
								});
							});
					}
			});
	});
	//############################################ INFO DE ACTIVIDADES ECONOMICAS #######################################
	//Cambio en Tipo
	$('#a1').change(function(){
		var tipo_actividad = $(this).val();

		// AJAX request
		 $.ajax({
				url:base_url+'consultas_ajax/get_actividades',
				method: 'post',
				data: {this_tipo_actividad: tipo_actividad},
				dataType: 'json',
				success: function(response){

						// Remove options
						$('#a4').find('option').not(':first').remove();
						$('#a3').find('option').not(':first').remove();
						$('#a2').find('option').not(':first').remove();
						// Add options
						$.each(response,function(index,data){
								$('#a2').append('<option value="'+data['id_actividad_econ']+'">'+data['nombre']+'</option>');
						});
				}
		});
});

// Cambio en Actividad

	$('#a2').change(function(){
			var actividad = $(this).val();
			var actividad_seleccionada = $('#a2 option:selected').text().trim();

			// AJAX request
			 $.ajax({
					url:base_url+'consultas_ajax/get_rubros',
					method: 'post',
					data: {this_actividad: actividad},
					dataType: 'json',
					success: function(response){
						// Remove options
						$('#a4').find('option').not(':first').remove();
						$('#a3').find('option').not(':first').remove();
						// Add options
						$.each(response,function(index,data){
								$('#a3').append('<option value="'+data['id_rubro']+'">'+data['nombre']+'</option>');
								$('#actTable > tr').each(function(){
									var actividad_tabla = $(this).find('td').eq(1).text();
									var rubro = $(this).find('td').eq(2).text();
									if(rubro == data['nombre'] && actividad_tabla == actividad_seleccionada){
										$('#a3 option[value="'+data['id_rubro']+'"]').remove();
									}
								});
						});
					}
			});
	});

	// Cambio en Rubro
	// $('#a3').change(function(){
	// 		var rubro = $(this).val();
	// 		var rubro_seleccionado = $('#a3 option:selected').text().trim();
	// 		// AJAX request
	// 		 $.ajax({
	// 				url:base_url+'consultas_ajax/get_subrubros',
	// 				method: 'post',
	// 				data: {this_rubro: rubro},
	// 				dataType: 'json',
	// 				success: function(response){
	// 						// Remove options
	// 						$('#a4').find('option').not(':first').remove();
	// 						// Add options
	// 						$.each(response,function(index,data){
	// 								$('#a4').append('<option value="'+data['id_subrubro']+'">'+data['nombre']+'</option>');
	// 								$('#actTable > tr').each(function(){
	// 									var rubro_tabla = $(this).find('td').eq(2).text();
	// 									var subrubro = $(this).find('td').eq(3).text();
	// 									if(subrubro == data['nombre'] && rubro_tabla == rubro_seleccionado){
	// 										$('#a4 option[value="'+data['id_subrubro']+'"]').remove();
	// 									}
	// 								});
	// 						});
	// 				}
	// 		});
	// });
});
