const UiBotn = document.querySelector('#actElement');
const UiTabla = document.querySelector('#actTable');
const UisTipos = document.querySelector('#a1');
const UisActividad = document.querySelector('#a2');
const UisRubro = document.querySelector('#a3');
const UisSubrubro = document.querySelector('#a4');
const UisProduccion = document.querySelector('#produccion_mensual');
const UisUnidad = document.querySelector('#unidad_medida');
const UisOperacion = document.querySelector('#estado_operacion');
const UiBotnSubmit = document.querySelector('#actSubmit');
var increment = 0;

document.addEventListener('DOMContentLoaded', () => {
  if(UiBotnSubmit)
  UiBotnSubmit.disabled = true;
});

function addEventHandler(){
  if( UisTipos.value == 0 ||
      UisActividad.value == 0 ||
      UisRubro.value == 0 ||
      //UisSubrubro.value == 0 ||
      UisProduccion == '' ||
      UisUnidad == 0 ||
      UisOperacion == 0
  ){
    console.log('No paso');
  }else{

    if(UiBotnSubmit){
      UiBotnSubmit.disabled = false;
    }
    /* UisActividad.options[0].selected; */
    const tr = document.createElement('tr');
    increment = increment +1;
    tr.className='myTr';
    tr.innerHTML = `
		<td>${UisTipos.selectedOptions[0].text}<input type="text" name="id_tipo_actividad_econ[]" id="ins-type-${increment}" hidden value="${UisTipos.value}"></td>
		<td>${UisActividad.selectedOptions[0].text}<input type="text" name="id_actividad_econ[]" id="ins-subtype-${increment}" hidden value="${UisActividad.value}"></td>
		<td>${UisRubro.selectedOptions[0].text}<input type="text" hidden name="id_rubro[]" id="ins-desc-${increment}" value="${UisRubro.value}"></td>
		<td>${UisUnidad.selectedOptions[0].text}<input type="text" hidden name="id_unidad_medida[]" id="ins-pres-${increment}" value="${UisUnidad.value}"></td>
		<td>${UisProduccion.value}<input type="text" hidden name="produccion_mensual[]" id="ins-pres-${increment}" value="${UisProduccion.value}"></td>
		<td>${UisOperacion.selectedOptions[0].text}<input type="text" hidden name="id_estado_operacion[]" id="ins-pres-${increment}" value="${UisOperacion.value}"></td>
		<td>
			<div class="row clearfix text-center">
				<a href="#" class="deleteBtn"><i class="fa fa-lg fa-trash"></i></a>
			</div>
		</td>
    `;

    //HTML DE SUBRUBRO
    /* <td>${UisSubrubro.selectedOptions[0].text}<input type="text" hidden name="id_subrubro[]" id="ins-pres-${increment}" value="${UisSubrubro.value}"></td> */

    UiTabla.appendChild(tr);
    UisTipos.value = 0;
    UisTipos.dispatchEvent(new Event('change'));
    UisUnidad.value = 0;
    UisUnidad.dispatchEvent(new Event('change'));
    UisOperacion.value = 0;
    UisOperacion.dispatchEvent(new Event('change'));
    UisProduccion.value = '';
  }
}
//}

function deleteEventHandler(e){
	// var checkingtable = document.getElementById('myTable');
  if(e.target.parentElement.classList.contains('deleteBtn')){
    e.target.parentElement.parentElement.parentElement.parentElement.remove();
    UisTipos.value = 0;
    UisTipos.dispatchEvent(new Event('change'));
    UisUnidad.value = 0;
    UisUnidad.dispatchEvent(new Event('change'));
    UisOperacion.value = 0;
    UisOperacion.dispatchEvent(new Event('change'));
    UisProduccion.value = '';
    if(UiTabla.childElementCount === 0){
      if(UiBotnSubmit){
        UiBotnSubmit.disabled = true;
      }

    }
	}
  e.preventDefault();
}

if(UiTabla){
  UiBotn.addEventListener('click', addEventHandler);
  UiTabla.addEventListener('click', deleteEventHandler);
}
