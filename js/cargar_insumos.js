//alert("Hello! I am an alert box!!");
function buscar_insumo(){
	var id_rubro = $("#id_rubro").val();
	var data = {
		'id_rubro':id_rubro
		};

		$.ajax({
            /* url:"http://ecopro.corpodelagro.gob.ve/index.php/calculador/cargar_insumos", */
            url:"http://localhost/ecopro_upsa/index.php/calculador/cargar_insumos",
			type:"POST",
			data: data,
			cache: false,
			error: function(resp){alert(resp);},
			success: function(resp){
							var recordset=$.parseJSON(resp);
							$("#tbody_tablas").html("");
							crear_tabla(recordset);
						}
			});
}

function buscar_rubros_todos(){
	var id_rubro = $("#id_rubro").val();
	var data = {
		'id_rubro':id_rubro
		};

		$.ajax({
			/* url:"http://ecopro.corpodelagro.gob.ve/index.php/calculador/cargar_rubros_todos", */
            url:"http://localhost/ecopro_upsa/index.php/calculador/cargar_rubros_todos",
			type:"POST",
			data: data,
			cache: false,
			error: function(resp){alert(resp);},
			success: function(resp){
					var recordset=$.parseJSON(resp);
					$("#rendimiento").text(recordset[0][0]);
					$("#densidad").text(recordset[0][1]);
					$("#ciclo").text(recordset[0][2]);
			}
		});
}

function crear_tabla(recordset){
	var tabla = "";
	var cont = 1;
	var acum = 0;
	var acum_dolar = 0;
	$.each( recordset, function( clave, valor ){
	  	//alert( "Index #" + clave + ": " + valor["tipo"] );

	  	extension = $("#extension").val();
	  	dolar = $("#dolar").val();
	  	extension_por_cantidad = extension * valor["cantidad"];
		precio_dolar = valor["costo"].toLocaleString('i')*dolar;
	    total_dolar = extension_por_cantidad * precio_dolar;
 	    cantidad_por_costo = extension_por_cantidad * valor["costo"];
   	  	acum = acum + cantidad_por_costo;
   	  	acum_dolar = acum_dolar + total_dolar;
		costo = new Intl.NumberFormat(["ban", "id"]).format(valor["costo"]);
		precio_dolar = new Intl.NumberFormat(["ban", "id"]).format(precio_dolar);
	  	
	  	tabla="<tr>\
	  					<th>"+cont+"</th>\
	  					<th>"+valor["tipo"]+"</th>\
	  					<th>"+valor["insumo"]+"</th>\
	  					<th>"+valor["unidad_medida"]+"</th>\
	  					<th>"+extension_por_cantidad.toLocaleString('de-DE')+"</th>\
	  					<th>"+costo+"</th>\
						<th>"+precio_dolar+"</th>\
	  					<th>"+cantidad_por_costo.toLocaleString('de-DE')+"</th>\
	  					<th>"+total_dolar.toLocaleString('de-DE')+"</th>\
	  	</tr>";
	  	$("#tbody_tablas").append(tabla);
	  	tabla = "";
	  	cont++;
	});
	//Total
	tabla2="<tr>\
	  					<th></th>\
	  					<th></th>\
	  					<th></th>\
	  					<th></th>\
	  					<th></th>\
	  					<th></th>\
	  					<th>Total </th>\
	  					<th>"+acum.toLocaleString('de-DE')+"</th>\
	  					<th>"+acum_dolar.toLocaleString('de-DE')+"</th>\
	  	</tr>";

	$("#tbody_tablas").append(tabla2);

	buscar_rubros_todos();
}
